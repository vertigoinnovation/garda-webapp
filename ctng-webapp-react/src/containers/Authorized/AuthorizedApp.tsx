import { Component } from 'react';

import MainLayout from '../../components/Layout/MainLayout';

export class AuthorizedApp extends Component {
    state = {
        collapsed: false,
    };

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };

    render() {
        return (
            <MainLayout />
        );
    }

    // render() {
    //     return (
    // <Layout>
    //     <Header className="header">
    //         <div className="logo" />
    //         <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['2']}>
    //             <Menu.Item key="1">nav 1</Menu.Item>
    //             <Menu.Item key="2">nav 2</Menu.Item>
    //             <Menu.Item key="3">nav 3</Menu.Item>
    //         </Menu>
    //     </Header>
    //     <SideMenu />
    //     <Layout className="site-layout">
    //         <Header className="site-layout-background" style={{ padding: 0 }}>
    //             {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
    //                 className: 'trigger',
    //                 onClick: this.toggle,
    //             })}
    //         </Header>
    //         <Content
    //             className="site-layout-background"
    //             style={{
    //                 margin: '24px 16px',
    //                 padding: 24,
    //                 minHeight: 280,
    //             }}
    //         >
    //             Content
    //         </Content>
    //         <Footer>Footer</Footer>
    //     </Layout>
    // </Layout>
    //     );
    // }
}

export default AuthorizedApp;

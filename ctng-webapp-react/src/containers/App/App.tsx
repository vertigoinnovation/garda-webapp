import "../../styles/App.less";

import PublicRouter from '../../components/App/PublicRouter';

// import 'antd/dist/antd.css';

const App = () => {
  return (
    <PublicRouter />
  );
}

export default App;

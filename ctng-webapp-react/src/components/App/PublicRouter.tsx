import { lazy, Suspense } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { BASE_URL } from '../../constants/routes';

const LoadableAuthorizedApp = lazy(() => import('../../containers/Authorized/AuthorizedApp'));

function PublicRouter() {
    return (
        <Router>
            <Suspense fallback={<div className="Loading"></div>}>
                <Switch>
                    <Route path={BASE_URL}>
                        <Switch>
                            {/* <Route path={`${ROUTE_URLS.LOGIN}`} component={LoadableLoginPage} /> */}
                            <Route component={LoadableAuthorizedApp} />
                        </Switch>
                    </Route>
                </Switch>
            </Suspense>
        </Router>
    );
}

export default PublicRouter;
import { useState } from 'react';

import { Layout, Tabs, Button, Form, DatePicker, Select, Row, Col } from 'antd';
import { Option } from "antd/lib/mentions";
import {
    LeftCircleOutlined,
    RightCircleOutlined,
    CheckSquareOutlined,
} from '@ant-design/icons';

import StopStatusCompleteService from '../../../assets/STOP_STATUS_COMPLETE_SERVICE.png';
import { FooterComponent } from '../Footer/Footer';
import { HeaderComponent } from '../Header/Header';
import { SiderMenu } from '../Menu/SiderMenu';
import { Sorter } from '../../../utils/sorter';
import Table from '../Table/Table';
import LocationDisplay from '../../../services/api/LocationDisplay';
import { CashTrack } from '../../../services/models/Models';
import { format, parseISO } from 'date-fns';


const { Sider, Content } = Layout;

const columns = [
  {
      title: '',
      dataIndex: 'stopStatusCompleteService',
      key: 'stopStatusCompleteService',
      render: () => (
      <div className="logo"> 
        <img src={StopStatusCompleteService} alt=''></img>        
      </div>
      ) 
  },
  {
    title: (
      <div style={{ width: "75px"}}>
        Route #
      </div>
    ),
    dataIndex: 'RouteNumber',
    key: 'RouteNumber',
    sorter: {
      compare: Sorter.DEFAULT,     
    },
  },
  {
    title: (
      <div style={{ width: "75px" }}>
        Route description
      </div>
    ),
    dataIndex: 'MasterRouteName',
    key: 'MasterRouteName',
    sorter: {
      compare: Sorter.DEFAULT,     
    },
  },
  {
    title: (
      <div style={{ width: "75px" }}>
        Site barcode
      </div>
    ),
    dataIndex: 'Barcode',
    key: 'Barcode',
    sorter: {
      compare: Sorter.DEFAULT,    
    },
  },
  {
    title: (
      <div style={{ width: "75px" }}>
        Site name
      </div>
    ),
    dataIndex: 'UnitName',
    key: 'UnitName',
    width:'2500px',
    sorter: {
      compare: Sorter.DEFAULT,
    },
  },
  {
    title: (
      <div style={{ width: "150px" }}>
        Address
      </div>
    ),
    dataIndex: 'Address1',
    key: 'Address1',
    width:'2500px',
    sorter: {
      compare: Sorter.DEFAULT,
    },
  },
  {
    title: (
      <div style={{ width: "75px" }}>
        City
      </div>
    ),
    dataIndex: 'CityName',
    key: 'CityName',
    sorter: {
      compare: Sorter.DEFAULT,
    },
  },
  {
    title: (
      <div style={{ width: "75px" }}>
        State
      </div>
    ),
    dataIndex: 'SubdivisionCountryCode',
    key: 'SubdivisionCountryCode',
    sorter: {
      compare: Sorter.DEFAULT,
    },
  },
  {
    title: (
      <div style={{ width: "75px" }}>
        Date
      </div>
    ),
    dataIndex: 'PlannedDate',
    key: 'PlannedDate',
    sorter: {
      compare: Sorter.DATE,
    },
  },
  {
    title: (
      <div style={{ width: "75px" }}>
        Day
      </div>
    ),
    dataIndex: 'DayNum',
    key: 'DayNum',
    sorter: {
      compare: Sorter.DAY,
    },
  },
  {
    title: (
      <div style={{ width: "75px" }}>
        Start
      </div>
    ),
    dataIndex: 'StartedOn',
    key: 'StartedOn',
    width:'250px',
    sorter: {
      compare: Sorter.HOUR,
    },
  },
  {
    title: (
      <div style={{ width: "75px" }}>
        Customer Start
      </div>
    ),
    dataIndex: 'CustomerStartedOn',
    key: 'CustomerStartedOn',
    sorter: {
      compare: Sorter.HOUR,
    },
  },
  {
    title: (
      <div style={{ width: "75px" }}>
        Customer End
      </div>
    ),
    dataIndex: 'CustomerEndedOn',
    key: 'CustomerEndedOn',
    sorter: {
      compare: Sorter.HOUR,
    },
  },
  {
    title: (
      <div style={{ width: "75px" }}>
        End
      </div>
    ),
    dataIndex: 'EndedOn',
    key: 'EndedOn',
    width:'250px',
    sorter: {
      compare: Sorter.HOUR,
    },
  },
  {
    title: (
      <div style={{ width: "75px" }}>
        Min in stop
      </div>
    ),
    dataIndex: 'MinInStop',
    key: 'MinInStop',
    sorter: {
      compare: Sorter.DEFAULT,
    },
  },
  {
    title: (
      <div style={{ width: "75px" }}>
        Pickups
      </div>
    ),
    dataIndex: 'NumberOfPickups',
    key: 'NumberOfPickups',
    sorter: {
      compare: Sorter.DEFAULT,
    },
  },
  {
    title: '',
    dataIndex: 'printPickups',
    key: 'printPickups'
  },
  {
    title: (
      <div style={{ width: "75px" }}>
        Deliveries
      </div>
    ),
    dataIndex: 'NumberOfDeliveries',
    key: 'NumberOfDeliveries',
    sorter: {
      compare: Sorter.DEFAULT,
    },
  },
  {
    title: '',
    dataIndex: 'printDeliveries',
    key: 'printDeliveries'
  },
  {
    title: (
      <div style={{ width: "75px" }}>
        Missed Reason
      </div>
    ),
    dataIndex: 'MissedStopReason',
    key: 'MissedStopReason',
    sorter: {
      compare: Sorter.DEFAULT,
    },
  },
  {
    title: (
      <div style={{ width: "75px" }}>
        Comment
      </div>
    ),
    dataIndex: 'Comments',
    key: 'Comments',
    width: '600px',
    sorter: {
      compare: Sorter.DEFAULT,
    },
  },
];

const nestedColumns1 = [
  {
    title: 'Barcode',
    dataIndex: 'ItemBarcode',
    key: 'ItemBarcode',
  },
  {
    title: 'Status',
    dataIndex: 'ItemStatusCode',
    key: 'ItemStatusCode',
  },
  {
    title: 'Date',
    dataIndex: 'Date', //{{s.Date | date:'shortDate'}}
    key: 'Date',
  },
  {
      title: 'Time',
      dataIndex: 'time',//{{s.Date | date:'shortDate'}}
      key: 'time',
    },
    {
      title: 'Route #',
      dataIndex: 'MasterRouteNumber',
      key: 'MasterRouteNumber',
    },
    {
      title: 'Route Name',
      dataIndex: 'MasterRouteName',
      key: 'MasterRouteName',
    },
    {
      title: 'From',
      dataIndex: 'FromDescription',
      key: 'FromDescription',
    },
    {
      title: 'To',
      dataIndex: 'ToDescription',
      key: 'ToDescription',
    },
    {
      title: 'Commodity',
      dataIndex: 'Commodity',
      key: 'Commodity',
    },
    {
      title: 'Type',
      dataIndex: 'TransactionCode',
      key: 'TransactionCode',
    },
    {
      title: 'Quantity',
      dataIndex: 'Quantity',
      key: 'Quantity',
    },
    {
      title: 'Amount  ',
      dataIndex: 'Value',
      key: 'Value',
    },
    {
      title: 'Operator',
      dataIndex: 'EmployeeFullName',
      key: 'EmployeeFullName',
    }
];

const nestedColumns2 = [
    {
      title: 'Time',
      dataIndex: 'Date', //{{ t.Date | date:'shortDate' }} {{ t.Date | date:"shortTime" }}
      key: 'Date',
    },
    {
      title: 'Type',
      dataIndex: 'TransactionCode',
      key: 'TransactionCode',
    },
    {
      title: 'From',
      dataIndex: 'FromDescription',
      key: 'FromDescription',
    },
    {
        title: 'To',
        dataIndex: 'ToDescription',
        key: 'ToDescription',
      },
      {
        title: 'Comment',
        dataIndex: 'Comments',
        key: 'Comments',
      },
      {
        title: 'Commodity',
        dataIndex: 'Commodity',
        key: 'Commodity',
      },
      {
        title: 'Quantity',
        dataIndex: 'quanQuantitytity',
        key: 'Quantity',
      },
      {
        title: 'Amount',
        dataIndex: 'Value', //t.Value | currency
        key: 'Value',
      } ,
      {
        title: 'Employee',
        dataIndex: 'EmployeeBarcode',
        key: 'EmployeeBarcode',
      } ,
      {
        title: 'Bagout',
        dataIndex: 'BagoutNumber',
        key: 'BagoutNumber',
      } 
]
interface TableData {
  stopReports: Array<CashTrack.ClientPortal.StopsReportResult>;
  packagesList: Array<Array<Object>>;
  transactionItemsList: Array<Array<Object>>;
}

const MainLayout = () => {

  const [tableData, setTableData] = useState({} as TableData);
  const [collapsed, setCollapsed] = useState(true);
  const [loading, setLoading] = useState(false);

  const handleCollapsedToggle = () => {
    setCollapsed(!collapsed);
  };
  
  function delay(ms:number) {
    return new Promise((resolve) => {
       setTimeout(resolve, ms);
    })
  }

  const loadTable = async () => {
    setLoading(true);
    await delay(1000);

    let results = (await LocationDisplay.findStopsByFilterAndDate()).Results;

    let tableData = {stopReports: [],packagesList:[],transactionItemsList:[]} as TableData;

    results.map((result,i) => 
      result.PackagesList.map((pkg,j) => 
        pkg.TransactionItemsList.map((transactionItem,k) =>
           {
            const plannedDate = format(parseISO(result.PlannedDate.toString()),"MM/dd/yy"); 
            const dayNum = format(parseISO(result.PlannedDate.toString()),"EEEE"); 
            const startedOn = format(parseISO(result.StartedOn.toString()),"h:mm a"); 
            const endedOn = format(parseISO(result.EndedOn.toString()),"h:mm a"); 
            const customerStartedOn = format(parseISO(result.CustomerStartedOn.toString()),"h:mm a"); 
            const customerEndedOn = format(parseISO(result.CustomerEndedOn.toString()),"h:mm a"); 

            tableData.stopReports.push({...result,PlannedDate:plannedDate,DayNum:dayNum,CustomerStartedOn:customerStartedOn,CustomerEndedOn:customerEndedOn,StartedOn:startedOn,EndedOn:endedOn,key: 'stopReports'+i+'-'+j+'-'+k} as any) 
            tableData.packagesList.push({...pkg,key: 'packageList'+i+'-'+j+'-'+k} as any);
            tableData.transactionItemsList.push({...transactionItem,key: 'transactionItemsList'+i+'-'+j+'-'+k} as any); 
          }
        )
      )
    )

    setTableData(tableData);
    setLoading(false);
  }

  const expandedRowRender2 = () => {
    return (
        <Table
          className="components-table-nested"
          columns={nestedColumns2}
          dataSource={tableData.transactionItemsList}
          pagination={false}
        />
      );
  }
  
  const expandedRowRender = () => {
      return (
          <Table showSorterTooltip
            className="components-table-nested"
            columns={nestedColumns1}
            expandable={{expandedRowRender: expandedRowRender2}}
            dataSource={tableData.packagesList}
            pagination={false}
          />
        );
  }  

  return (
      <>
        <Layout style={{minHeight: '100vh'}}>
          <HeaderComponent handleCollapsedToggle={handleCollapsedToggle}/>   
          <Layout>
          <Sider trigger={null} collapsible collapsed={collapsed} width='225px'>
                  <SiderMenu collapsed={collapsed}/>     
                  <div className='SiderTrigger'>
                      <Button className='SiderTriggerButton' type='primary' icon={collapsed ? <RightCircleOutlined className='SiderTriggerIcon' /> : <LeftCircleOutlined className='SiderTriggerIcon'/>} onClick={handleCollapsedToggle}>
                      </Button>          
                  </div>
          </Sider>
          <Layout>
                  <Content style={{backgroundColor:'white', maxWidth:'100%',position:'inherit'}}>
                      <Layout>                       
                          <div className='FormBox'>
                              <div className="BoxHeader" 
                                  > <CheckSquareOutlined /> Location Display  
                              </div >
                              <div className='Form'>
                                  <Row gutter={[24,12]}>
                                    <Col className="gutter-row" span={2}>
                                        <div style={{float: 'right', marginTop:'5px'}}>Criteria:</div>
                                    </Col>
                                    <Col className="gutter-row" span={10}>
                                      <Form.Item hasFeedback style={{width:'136px'}}>
                                        <Select allowClear>
                                          <Option value='Option1'></Option>
                                        </Select>
                                      </Form.Item>
                                    </Col>
                                    <Col className="gutter-row" span={2}>
                                    </Col>
                                    <Col className="gutter-row" span={10}>
                                      <Form.Item labelAlign="left" hasFeedback style={{ width:'136px'}}>
                                        <Select allowClear>
                                          <Option value='Option1'></Option>
                                        </Select>
                                      </Form.Item>
                                    </Col>
                                    <Col className="gutter-row" span={2}>
                                      <div style={{float: 'right', marginTop:'5px'}}>From:</div>
                                    </Col>
                                    <Col className="gutter-row" span={10}>
                                      <Form.Item name="date-picker" style={{width:'150px'}} >
                                        <DatePicker />
                                      </Form.Item>
                                    </Col>
                                    <Col className="gutter-row" span={2}>
                                      <div style={{float: 'right', marginTop:'5px'}}>To:</div>
                                    </Col>
                                    <Col className="gutter-row" span={10}>
                                      <Form.Item name="date-picker" style={{width:'150px'}}>
                                        <DatePicker />
                                      </Form.Item> 
                                    </Col>
                                  </Row>
                              </div>
                          </div>
                          <div className="SearchButtonSection">
                              <Button className="SearchButton" type='primary' disabled={loading} onClick={loadTable}> Search
                              </Button>  
                          </div>                        
                          <div className="TableSection">
                              <Tabs defaultActiveKey='1'>
                                  <Tabs.TabPane key='1' tab='Route Items'>
                                      <Table scroll={{x: true}} loading={loading} className="components-table-demo-nested" 
                                          columns={columns as any} 
                                          expandable={{expandedRowRender}}
                                          dataSource={tableData.stopReports}
                                          />
                                  </Tabs.TabPane>
                                  <Tabs.TabPane key='2' tab='Routing'> </Tabs.TabPane>
                                  <Tabs.TabPane key='3' tab='Destinations'> </Tabs.TabPane>
                                  <Tabs.TabPane key='4' tab='Shipments'> </Tabs.TabPane>   
                              </Tabs>
                          </div>
                      </Layout>
                  </Content>
                  <FooterComponent/>
           </Layout>
         </Layout>
        </Layout>
      </>
  );
};

export default MainLayout;

import { Header } from "antd/lib/layout/layout";
import GardaLogo from '../../../assets/GardaLogo.png';
import CashtrakLogo from '../../../assets/cashtrak.png';
import { Button, Select, Space } from "antd";
import { LogoutOutlined, MenuOutlined } from "@ant-design/icons";
import { useState } from "react";

const defaultLocale = localStorage['locale'] ? localStorage['locale'] : '🇺🇸 English'; // English is default locale if none is set
const localeList = [
    { name: '🇺🇸 English', code: 'en', lang: 'English' },
    { name: '🇫🇷 Francais', code: 'fr', lang: 'French' }
];

export const HeaderComponent = (props: any) => { 

    const [currentLocale, setCurrentLocale] = useState(defaultLocale);
    const onChangeLanguage = (e) => {
      const selectedLocale = e;
      setCurrentLocale(selectedLocale);
      localStorage.setItem('locale',selectedLocale)
  }

    const  toggleSider = () => {
        props.handleCollapsedToggle();
    };

    return <Header className="MainHeader" style={{flexDirection:'row'}}>               
                <div style={{display:'flex'}}> 
                    <Space>             
                        <div className="logo">
                            <img src={GardaLogo} alt=''></img>
                        </div>     
                        <div className="logo"> 
                            <img src={CashtrakLogo} alt=''></img>        
                        </div> 
                    </Space>
                    <Space style={{marginLeft:'auto'}}>
                        <Select onChange={onChangeLanguage} defaultValue={currentLocale}> 

                            {
                                localeList.map((locale,index)=>(
                                <option key={index} value={locale.code}>{locale.name}</option>
                                ))
                            }
                        </Select>                       
                        <Button
                            type="primary"
                            icon={<LogoutOutlined />}
                        />
                        <Button
                            type="primary"
                            icon={<MenuOutlined />}
                            onClick={toggleSider}
                        />
                    </Space>
                </div>
           </Header >;
}
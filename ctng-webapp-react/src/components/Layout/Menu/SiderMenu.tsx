import { ClusterOutlined, ContainerOutlined, DashboardOutlined, DollarOutlined, HomeOutlined, LockOutlined, MenuFoldOutlined, MenuOutlined, MenuUnfoldOutlined, PrinterOutlined, TeamOutlined } from "@ant-design/icons";
import { faMoneyBillAlt } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Menu } from "antd";
import SubMenu from "antd/lib/menu/SubMenu";
import { ADD_ON_DEMAND, ADMINISTRATION, ASSIGN_ON_DEMAND, ASSIGN_TANKERS_TO_ROUTE, AUTHORIZE_BILLING, BALANCE_ECASH_INVENTORY, BILLING, BILLING_DASHBOARD, CHECKIN_ITEMS_LIST, COIN_CHECKIN, CONSOLIDATED_MANIFESTED_ITEMS, CREATE_BAGOUT, CREATE_TANKER_FOR_OTHER_AREA, CREATE_TANKER_FOR_OTHER_DESTINATION, DISPATCH_LOG, DISPLAY_VAULT_INVENTORY, EDIT_ITEM, EDIT_MISSING_ITEMS, EXPORT_TO_MM_FOR_IMMEDIATE_CREDIT, HOLDOVER, HOME, INBOUND_ITEMS, ITEMS_MANIFEST, ITEM_CHECKIN, ITEM_CHECKOUT, ITEM_DISPLAY, JDE_ERROR_MESSAGE_REPORT, LOAD_TRUCK_DISPLAY, LOCATION_DISPLAY, MANAGE_AREAS, MANAGE_CUSTOMER_INFORMATION, MANAGE_CUSTOMER_KEYS, MANAGE_EMPLOYEES, MANAGE_HOLIDAY_ROUTES, MANAGE_MASTER_ROUTES, MANAGE_ON_DEMAND, MANAGE_SOURCE, MANAGE_TANKERS, MANAGE_TRUCKS, MASTER_ROUTE_PER_SITE, MISMATCH_REPORT, MISSED_AND_MANUAL_STOPS_RECAP, MONEY_MANAGER_ERROR_REPORT, MOVE_AND_BALANCE, MOVE_STOPS_AND_ITEMS_TO_ANOTHER_ROUTE, ON_DEMAND, OPERATIONS_FOLLOW_UP, OUTBOUND_ITEMS, PREPARE_MANIFEST, PRINT_COIN_PULL_SHEETS, PRINT_DELIVERY_RECEIPTS, PRINT_ECASH_ITEMS, PRINT_KEY_REPORT, PRINT_LABELS, PRINT_ROUTE_SHEETS, PRINT_SHIPPING_SHEETS, PRINT_TANKER_MANIFEST, REPORTS, REQUEST_HHT_FILES_FOR_BEGIN_DAY, ROUTES, ROUTE_ANALYSIS, ROUTE_CHECKIN, ROUTE_CHECKOUT, SITE_CHANGE_DASHBOARD, ST_SERVICE, SUPERVISOR_PIN, THROW_TO_TANKER, TRANSFER_FROM_CVS_CARRIER, TRANSFER_FROM_OTHER_AREA_ROUTE, TRANSFER_TO_CVS, TRANSFER_TO_EXTERNAL_AREA, TRANSFER_TO_OTHER_AREA, UNROUTED, VAULT_ROOM, VERIFY_ROUTE_KEYS } from "../../../constants/siderMenuConstants";

const data = [
    {
        "title":HOME,
        "icon":<HomeOutlined/>,//"antd_<HomeOutlined />",
        "items":[
            
        ]    
    },
    {
        "title":ST_SERVICE,
        "icon":<MenuOutlined/>,
        "items":[
            {"title":SITE_CHANGE_DASHBOARD},
            {"title":MANAGE_CUSTOMER_INFORMATION},
            {"title":JDE_ERROR_MESSAGE_REPORT}, 
        ]                   
    },
    {
        "title":ON_DEMAND,
        "icon":<MenuOutlined/>,
        "items":[
            {"title":ADD_ON_DEMAND},
            {"title":MANAGE_ON_DEMAND},
            {"title":ASSIGN_ON_DEMAND}, 
        ]                   
    },
    {
        "title":ROUTES,
        "icon":<ClusterOutlined/>,
        "items":[
            {"title":MANAGE_MASTER_ROUTES},
            {"title":MASTER_ROUTE_PER_SITE},
            {"title":MISMATCH_REPORT}, 
            {"title":MANAGE_HOLIDAY_ROUTES},
        ]                   
    },
    {
        "title":ITEMS_MANIFEST,
        "icon":<ContainerOutlined/>,
        "items":[
            {"title":PREPARE_MANIFEST},
            {"title":CONSOLIDATED_MANIFESTED_ITEMS},
            {"title":MONEY_MANAGER_ERROR_REPORT}, 
        ]                   
    },
    {
        "title":OUTBOUND_ITEMS,
        "icon":<MenuFoldOutlined/>,
        "items":[
            {
                "title":ITEM_CHECKOUT,
                "items":[
                    {"title":ROUTE_CHECKOUT},
                    {"title":TRANSFER_TO_OTHER_AREA},
                    {"title":TRANSFER_TO_CVS},
                    {"title":TRANSFER_TO_EXTERNAL_AREA},
            ]},
            {"title":REQUEST_HHT_FILES_FOR_BEGIN_DAY},
            {"title":LOAD_TRUCK_DISPLAY},
        ],
    },
    {
        "title":INBOUND_ITEMS,
        "icon":<MenuFoldOutlined/>,
        "items":[
            {
                "title":ITEM_CHECKIN,
                "items":[
                    {"title":ROUTE_CHECKIN},
                    {"title":TRANSFER_FROM_OTHER_AREA_ROUTE},
                    {"title":TRANSFER_FROM_CVS_CARRIER},
                    
            ]},
            {"title":COIN_CHECKIN},
            {"title":CHECKIN_ITEMS_LIST},
            {"title":EXPORT_TO_MM_FOR_IMMEDIATE_CREDIT},
        ],
    },
    {
        "title":VAULT_ROOM,
        "icon":<LockOutlined/>,
        "items":[
            {"title":THROW_TO_TANKER},
            {"title":HOLDOVER},
            {"title":UNROUTED},
            {"title":MOVE_AND_BALANCE},
            {"title":BALANCE_ECASH_INVENTORY},
            {"title":CREATE_BAGOUT}, 
            {"title":CREATE_TANKER_FOR_OTHER_AREA},
            {"title":CREATE_TANKER_FOR_OTHER_DESTINATION},
            {"title":MOVE_STOPS_AND_ITEMS_TO_ANOTHER_ROUTE}, 
            {"title":EDIT_ITEM},
            {"title":VERIFY_ROUTE_KEYS},
        ]                   
    },
    {
        "title":REPORTS,
        "icon":<PrinterOutlined/>,
        "items":[
            {"title":PRINT_ROUTE_SHEETS},
            {"title":PRINT_SHIPPING_SHEETS},
            {"title":PRINT_DELIVERY_RECEIPTS},
            {"title":PRINT_KEY_REPORT},
            {"title":PRINT_COIN_PULL_SHEETS},
            {"title":PRINT_TANKER_MANIFEST}, 
            {"title":PRINT_ECASH_ITEMS},
        ]                   
    },
    {
        "title":OPERATIONS_FOLLOW_UP,
        "icon":<DashboardOutlined/>,
        "items":[
            {"title":LOCATION_DISPLAY},
            {"title":ITEM_DISPLAY},
            {"title":MISSED_AND_MANUAL_STOPS_RECAP},
            {"title":ROUTE_ANALYSIS},
            {"title":DISPATCH_LOG},
            {"title":EDIT_MISSING_ITEMS}, 
            {"title":DISPLAY_VAULT_INVENTORY},
        ]                   
    },
    {
        "title":ADMINISTRATION,
        "icon":<TeamOutlined/>,
        "items":[
            {"title":PRINT_LABELS},
            {"title":MANAGE_EMPLOYEES},
            {"title":MANAGE_AREAS},
            {"title":MANAGE_TRUCKS},
            {"title":MANAGE_CUSTOMER_KEYS},
            {"title":MANAGE_TANKERS}, 
            {"title":ASSIGN_TANKERS_TO_ROUTE},
            {"title":MANAGE_SOURCE}, 
            {"title":SUPERVISOR_PIN},
        ]                   
    },
    {
        "title":BILLING,
        "icon":<DollarOutlined/>, //{<FontAwesomeIcon style={{marginRight:'10px'}} icon={faMoneyBillAlt}/>}
        "items":[
            {"title":BILLING_DASHBOARD},
            {"title":AUTHORIZE_BILLING},
        ]                   
    },
];

const renderMenuItem = (title:string,icon:any,key:string) =>{
    return <Menu.Item icon={icon} key={key}>{title}</Menu.Item>;                                        
}

const renderSubMenu = (title:string,menuItems:Array<any>,icon:any,key:string) =>{
    return <SubMenu key={key} title={title} icon={icon}> 
                {menuItems.map((menuItem,i) =>{ 
                    if(menuItem.items?.length >0){
                        return renderSubMenu(menuItem.title,menuItem.items,null,'subMenu-'+key+i);
                    }                 
                    else{
                       return renderMenuItem(menuItem.title,null,'menuItem-'+key+i);
                    }  
                })}       
           </SubMenu>      
    ;                                        
}

const renderMenu = (data:Array<any>) =>{

    return (
        <Menu mode="inline">
            {data.map((item, i) =>{
                if(item.items?.length >0){
                    console.log(item.icon);
                    return renderSubMenu(item.title,item.items,item.icon,'subMenu-'+i);
                }                 
                else{
                   return renderMenuItem(item.title,item.icon,'menuItem-'+i);
                }                 
            })}
        </Menu>
    )
}

export const SiderMenu = (props:any) => {

    return <div style={{}}>
        <Menu mode="inline">
            {renderMenu(data)}
        </Menu>
    </div>
}
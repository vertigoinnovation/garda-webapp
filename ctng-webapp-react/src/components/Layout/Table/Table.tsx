import React from 'react';
import { Table as AntTable } from 'antd';

const Table = (props: any) => {
  const {
    columns,
    ...otherTableProps
  } = props;

  const sortableColumns = columns.map((column: any) => {
    const { sorter, dataIndex, ...otherColumnProps } = column;

    if (sorter) {
      const { compare, ...otherSorterProps } = sorter;

      return {
        ...otherColumnProps,
        dataIndex,
        sorter: {
          compare: (rowA: any, rowB: any) => compare(rowA[dataIndex], rowB[dataIndex]),
          ...otherSorterProps,
        }
      };
    }

    return column;
  });

  return (
    <AntTable
      columns={sortableColumns}
      {...otherTableProps}
    />
  );
};

export default Table;
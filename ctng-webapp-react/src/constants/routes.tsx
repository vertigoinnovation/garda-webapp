export const BASE_URL = `/`;

export const ROUTE_URLS = {
  LOGIN: `${BASE_URL}/login`,
  HOME: `${BASE_URL}/home`,
};

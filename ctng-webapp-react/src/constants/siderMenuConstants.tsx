/*
    This file contains all the sider menu sections titles 
*/
export const HOME = 'Home';

export const ST_SERVICE = 'ST Service';
export const SITE_CHANGE_DASHBOARD = 'Site Change Dashboard';
export const MANAGE_CUSTOMER_INFORMATION = 'Manage Customer Information';
export const JDE_ERROR_MESSAGE_REPORT = 'JDE Error Message Report';    

export const ON_DEMAND = 'On demand';   
export const ADD_ON_DEMAND = 'Add On Demand';
export const MANAGE_ON_DEMAND = 'Manage On Demand';  
export const ASSIGN_ON_DEMAND = 'Assign On Demand';

export const ROUTES = 'Routes';     
export const MANAGE_MASTER_ROUTES = 'Manage Master Routes';
export const MASTER_ROUTE_PER_SITE = 'Master Route per Site';  
export const MISMATCH_REPORT = 'Mismatch Report';                   
export const MANAGE_HOLIDAY_ROUTES = 'Manage Holiday Routes';   

export const ITEMS_MANIFEST = 'Items Manifest';      
export const PREPARE_MANIFEST = 'Prepare Manifest';
export const CONSOLIDATED_MANIFESTED_ITEMS = 'Consolidated Manifested Items';  
export const MONEY_MANAGER_ERROR_REPORT = 'Money Manager Error Report'; 
      
export const OUTBOUND_ITEMS = 'Outbound Items';
export const ITEM_CHECKOUT = 'Item Checkout';                          
export const ROUTE_CHECKOUT = 'Route Checkout';
export const TRANSFER_TO_OTHER_AREA = 'Transfer to Other Area';
export const TRANSFER_TO_CVS = 'Transfer to CVS';
export const TRANSFER_TO_EXTERNAL_AREA = 'Transfer to External Area';
export const REQUEST_HHT_FILES_FOR_BEGIN_DAY = 'Request HHT Files For Begin Day'; 
export const LOAD_TRUCK_DISPLAY = 'Load Truck Display'; 

export const INBOUND_ITEMS = 'Inbound Items';
export const ITEM_CHECKIN = 'Item Checkin';                           
export const ROUTE_CHECKIN = 'Route Checkin';
export const TRANSFER_FROM_OTHER_AREA_ROUTE = 'Transfer From Other Area Route';
export const TRANSFER_FROM_CVS_CARRIER = 'Transfer From CVS/Carrier';
export const COIN_CHECKIN = 'Coin Checkin';  
export const CHECKIN_ITEMS_LIST = 'Checkin Items List';                
export const EXPORT_TO_MM_FOR_IMMEDIATE_CREDIT = 'Export to MM for immediate credit'; 

export const VAULT_ROOM = 'Vault Room';       
export const THROW_TO_TANKER = 'Throw to tanker';
export const HOLDOVER = 'Holdover';  
export const UNROUTED = 'Unrouted';                     
export const MOVE_AND_BALANCE = 'Move and balance';
export const BALANCE_ECASH_INVENTORY = 'Balance eCash Inventory';
export const CREATE_BAGOUT = 'Create bagout';  
export const CREATE_TANKER_FOR_OTHER_AREA = 'Create tanker for other are';      
export const CREATE_TANKER_FOR_OTHER_DESTINATION = 'Create tanker for other destination';
export const MOVE_STOPS_AND_ITEMS_TO_ANOTHER_ROUTE = 'Move Stops and Items to another route';
export const EDIT_ITEM = 'Edit Item';  
export const VERIFY_ROUTE_KEYS = 'Verify Route Keys'; 

export const REPORTS = 'Reports';      
export const PRINT_ROUTE_SHEETS = 'Print Route Sheets';
export const PRINT_SHIPPING_SHEETS = 'Print Shipping Sheets';  
export const PRINT_DELIVERY_RECEIPTS = 'Print Delivery Receipts';           
export const PRINT_KEY_REPORT = 'Print Key Report';
export const PRINT_COIN_PULL_SHEETS = 'Print Coin Pull Sheets';            
export const PRINT_TANKER_MANIFEST = 'Print Tanker Manifest';             
export const PRINT_ECASH_ITEMS = 'Print eCash Items';  

export const OPERATIONS_FOLLOW_UP = 'Operations Follow up';
export const LOCATION_DISPLAY = 'Location Display';
export const ITEM_DISPLAY = 'Item Display';  
export const MISSED_AND_MANUAL_STOPS_RECAP = 'Missed & Manual Stops Recap';      
export const ROUTE_ANALYSIS = 'Route Analysis';
export const DISPATCH_LOG = 'Dispatch Log';                     
export const EDIT_MISSING_ITEMS = 'Edit Missing Items';               
export const DISPLAY_VAULT_INVENTORY = 'Display Vault Inventory'; 

export const ADMINISTRATION = 'Administration';     
export const PRINT_LABELS = 'Print Labels';
export const MANAGE_EMPLOYEES = 'Manage Employees';  
export const MANAGE_AREAS = 'Manage Areas';                     
export const MANAGE_TRUCKS = 'Manage Trucks';
export const MANAGE_CUSTOMER_KEYS = 'Manage Customer Keys';             
export const MANAGE_TANKERS = 'Manage Tankers';                   
export const ASSIGN_TANKERS_TO_ROUTE = 'Assign Tankers to Route'; 
export const MANAGE_SOURCE = 'Manage Source';                    
export const SUPERVISOR_PIN = 'Supervisor PIN';           
        
export const BILLING = 'Billing';       
export const BILLING_DASHBOARD = 'Billing Dashboard';
export const AUTHORIZE_BILLING = 'Authorize Billing';                
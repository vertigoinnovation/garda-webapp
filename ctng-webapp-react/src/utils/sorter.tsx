import { parse } from 'date-fns'
import {enCA} from 'date-fns/locale'

// SYMBOL	          | MEANING	                    | PRESENTATION  |	  EXAMPLES
// yy, yyyy	        | The year.	                  | year	        |  20, 2020
// M, MM, MMM, MMMM	| The month of the year.  	  | number/text	  |  8, 08, Aug, August
// d, dd	          | The day of the month.	      | number	      |  3, 03
// E, EEEE, EEEEE	  | The day of the week.	      | text	        |  Thu, Thursday, T
// D	              | The day of year.The number  | number        |  216
//                  |   of days since January     |               |
// G, GGGG, GGGGG	  | The period or era.	        | text	        |  AD, Anno Domini, A

// ------PATTERN EXAMPLE-------
// yyyy-MM-dd	                    |    2018-07-14
// dd-MMM-yyyy	                  |    14-Jul-2018
// dd/MM/yyyy	                    |    14/07/2018
// E, MMM dd yyyy	                |    Sat, Jul 14 2018
// h:mm a	                        |    12:08 PM
// EEEE, MMM dd, yyyy HH:mm:ss a	|    Saturday, Jul 14, 2018 14:31:06 PM
// yyyy-MM-dd'T'HH:mm:ssZ	        |    2018-07-14T14:31:30+0530
// hh 'o''clock' a, zzzz	        |    12 o’clock PM, Pacific Daylight Time
// K:mm a, z	                    |    0:08 PM, PDT

/**
 * @param {string} dateA - a date, represented in string format
 * @param {string} dateB - a date, represented in string format
 */
 const dateSort = (dateA:string, dateB:string) => {return (parse(dateA, 'MM/dd/yy', new Date()) > parse(dateB, 'MM/dd/yy', new Date()))? 1:-1};

 /**
 * @param {string} dayA - a day, represented in string format
 * @param {string} dayB - a day, represented in string format
 */
  const daySort = (dayA:string, dayB:string) =>  {return (parse(dayA, 'EEEE', new Date()) > parse(dayB, 'EEEE', new Date()))? 1:-1};

 /**
 * @param {string} hourA - an hour, represented in string format
 * @param {string} hourB - an hour, represented in string format
 */
const hourSort = (hourA:string, hourB:string) => {return (parse(hourA, 'hh:mm a', new Date()) > parse(hourB, 'hh:mm a', new Date()))? 1:-1};

 /**
 *
 * @param {number|string} a
 * @param {number|string} b
 */
const defaultSort = (a: number | string, b: number | string) => {
    if (a < b) return -1;
    if (b < a) return 1;
    return 0;
};

export const Sorter = {
  DEFAULT: defaultSort,
  DATE: dateSort,
  DAY: daySort,
  HOUR: hourSort,
};


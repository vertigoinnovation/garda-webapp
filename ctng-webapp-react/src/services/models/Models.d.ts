﻿

export declare module CashTrack {
	interface BusinessRuleValidationResult {
		Code: CashTrack.Core.BusinessRuleValidationCodeEnum;
		IsValid: boolean;
		NeedsConfirmation: boolean;
		IsFinal: boolean;
		CanRetry: boolean;
	}
}
declare module CashTrack.Common {
	interface SearchTableResult<TEntity> {
		TotalCount: number;
		FilteredTotalCount: number;
		Results: TEntity[];
	}
	interface SearchTableQuery extends CashTrack.Common.Request {
		CurrentPage: number;
		ItemsToShow: number;
		EnablePaging: boolean;
		OrderBy: string;
		SortOrder: System.Data.SqlClient.SortOrder;
	}
	interface Request {
		RequestDateTime?: Date;
	}
	interface ChangeMissedStopCommand extends CashTrack.Common.Request {
		StopID: number;
		MissedStopReasonCode: string;
		Comments: string;
	}
	interface RouteEmployeeResult {
		RouteID: number;
		EmployeeID: number;
		EmployeeBarcode: string;
		EmployeeFullName: string;
		EmployeeRoleCode: string;
		FirstName: string;
		MiddleInitial: string;
		LastName: string;
		RouteStatusCode: string;
		DatasetNumber: number;
	}
	interface RouteVehicleResult {
		ID: number;
		RouteID: number;
		VehicleID: number;
		VehicleBarcode: string;
		EndMileage: number;
		StartMileage: number;
		PlannedDate: Date;
		RouteStatusCode: string;
		IsOnActiveRoute: boolean;
	}
	interface AreaResult {
		ID: number;
		Number: number;
		Name: string;
		MaxRouteMileage: number;
		CutOffAreaTime: System.TimeSpan;
		EndDate: Date;
		AreaUnitID: number;
		IsPieceCountToGardaCVSAllowed: boolean;
		IsManagingOverride: boolean;
		MMSTSiteNumber?: string;
	}
	interface CommodityResult {
		ID: number;
		Description: string;
		Code: string;
		CommodityTypeCode: string;
		UnitAmount: number;
		IsCheckInAllowed: boolean;
		IsEmergency: boolean;
		BarcodeSuffix: string;
	}
	interface UnitResult extends CashTrack.Common.UnitBriefResult {
		Address: string;
		FullAddress: CashTrack.Common.AddressResult;
		UnitTypeCode: string;
		UnitType: CashTrack.Common.UnitTypeResult;
		Description: string;
		JDEServiceLocationNumber: number;
		CustomerUnitNumber: string;
		IsActive: boolean;
		SiteLocationID: number;
		SiteLocation: CashTrack.Common.SiteLocationResult;
		SuspendedBeganOn: Date;
		SuspendedEndedOn: Date;
		Latitude: number;
		Longitude: number;
		MissedStopRequiredActionID: number;
		MissedStopRequiredAction: CashTrack.Common.MissedStopRequiredActionResult;
		TimeZoneCode: string;
		TimeWindowStartedOn: Date;
		TimeWindowEndedOn: Date;
		ExceptTimeWindowStartedOn: Date;
		ExceptTimeWindowEndedOn: Date;
		OutsourcedReferenceNumber: string;
		MilesFromArea: number;
		IsMixedCommodityAllowed: boolean;
		CassetteQuantity: number;
		ContactName: string;
		ContactEmail: string;
		ContactPhoneNumber: string;
		IsBillable: boolean;
		UsedAsAlternateDestination: boolean;
	}
	interface UnitBriefResult {
		ID: number;
		Barcode: string;
		Name: string;
	}
	interface AddressResult {
		Street: string;
		City: string;
		State: string;
		Zipcode: string;
	}
	interface UnitTypeResult {
		Code: string;
		Description: string;
		IsNonBillableUnit: boolean;
		IsCustomerUnit: boolean;
	}
	interface SiteLocationResult {
		ID: number;
		CityID: number;
		Number: number;
		Address1: string;
		Address2: string;
		Address3: string;
		SearchAddress: string;
		ZipPostalCode: string;
		IsConjunctiveService: boolean;
		City: CashTrack.Common.CityResult;
	}
	interface CityResult {
		ID: number;
		Name: string;
		SubdivisionCountryCode: string;
	}
	interface MissedStopRequiredActionResult {
		ID: number;
		Code: string;
		Description: string;
	}
	interface ActiveServiceChangesOnDateOrPeriodQuery {
		UnitID: number;
		Date: Date;
		PeriodStartDate: Date;
		PeriodEndDate: Date;
	}
	interface HolidayResult {
		ID: number;
		Code: string;
		Description: string;
		ShortDescriptionBillingType: string;
		AreaHolidayDate: Date;
		IsHolidayAM: boolean;
		IsHolidayPM: boolean;
		IsRemovable: boolean;
		IsEditable: boolean;
	}
	interface MasterRouteResult {
		ID: number;
		RouteNumber: string;
		RouteName: string;
		RouteTypeCode: string;
		IsSpecialRoute: boolean;
		AreaID: number;
		IsRouteAM: boolean;
		TypeCode: string;
		IsOverDaysRoute: boolean;
		ODRDaysQty: number;
		LastGeneratedDailyRouteDateOn: Date;
	}
	interface StopTypeResult {
		Code: string;
		Description: string;
	}
	interface MissedStopToRescheduleResult {
		StopID: number;
		Route: string;
		Date: Date;
		MissedStopReasonCode: string;
		IsGardaResponsibility: boolean;
		Comments: string;
		ShortAttributeCode: string;
	}
	interface RouteTypeResult {
		Code: string;
		Description: string;
		IsServiceByTruck: boolean;
		IsDisplayedInMasterRouteDashboard: boolean;
		IsDailyGenerated: boolean;
		IsInternalRoute: boolean;
		IsSimulated: boolean;
	}
	interface StopServiceResult {
		ID: number;
		Stop: CashTrack.Common.StopResult;
		Unit: CashTrack.Common.UnitResult;
		FixeServiceDescription: string;
		StopServiceBillingTypeID: number;
		IsCancelled: boolean;
		CreatedBy: number;
		CreatedOn: Date;
		LastUpdatedBy: number;
		LastUpdatedOn: Date;
		AreaID: number;
		UnitID: number;
		CITServiceID: number;
		MasterStop: CashTrack.Common.MasterStopResult;
	}
	interface StopResult {
		ID: number;
		IsManual: boolean;
		IsCancelled: boolean;
		IsBillable: boolean;
		SequenceNumber: number;
		InternalSequenceNumber: number;
		MasterStopID: number;
		StartedOn: Date;
		EndedOn: Date;
		CustomerStartedOn: Date;
		CustomerEndedOn: Date;
		Route_ID: number;
		Route: CashTrack.Common.RouteResult;
		Unit: CashTrack.Common.UnitResult;
	}
	interface RouteResult {
		ID: number;
		RouteNumber: string;
		DepartedOn: Date;
		ArrivedOn: Date;
		RouteStatusCode: string;
		PlannedDate: Date;
		EndDate: Date;
		CutOffDate: Date;
		TankerID: number;
		UnloadStartedOn: Date;
		UnloadEndedOn: Date;
		RouteName: string;
		AreaName: string;
		AreaID: number;
		AreaUnitID: number;
		AreaHolidayDateID: number;
		MasterRouteID: number;
		MasterRouteDetailID: number;
		IsServiceByTruck: boolean;
		IsParcelCheckInCompleted: boolean;
		IsCoinCheckInCompleted: boolean;
		IsCheckOutCompleted: boolean;
		IsTransferCheckInCompleted: boolean;
		RouteTanker: string;
		LoadStartedOn: Date;
		LoadEndedOn: Date;
		RouteTypeCode: string;
		ActualUnloadEndDateTime: Date;
		ActualBalanceStartDateTime: Date;
		TankerBarcode: string;
		AMorPMCode: string;
	}
	interface MasterStopResult {
		ID: number;
		MasterRouteDetailID: number;
		UnitID: number;
		PreviousMasterStopID: number;
		NextMasterStopID: number;
		StopTypeCode: string;
		InternalSequenceNumber: number;
		IsDepositStop: boolean;
		IsShipmentStop: boolean;
		IsDeliveryStop: boolean;
		ServiceDayCode: string;
		ServiceDateBegunOn: Date;
		ServiceDateEndedOn: Date;
		EndedTemporaryBegunOn: Date;
		EndedTemporaryEndedOn: Date;
		IsKeyRequired: boolean;
		IsDeliveredOnSameRoute: boolean;
		IsBilled: boolean;
		CreatedBy: number;
		CreatedOn: Date;
		LastUpdatedBy: number;
		LastUpdatedOn: Date;
		OperationComment: string;
		StopTypeDescription: string;
		AreaHolidayDateID: number;
	}
	interface RelatedMasterStopResult {
		ID: number;
		MasterRouteDetailID: number;
		UnitID: number;
		StopTypeCode: string;
		InternalSequenceNumber: number;
		IsDepositStop: boolean;
		IsShipmentStop: boolean;
		ServiceDayCode: string;
		RouteNumber: string;
		UnitBarcode: string;
		UnitName: string;
		AreaName: string;
		ServiceDateBegunOn: Date;
	}
	interface VerifyStopSequenceforRouteQuery {
		RouteIDs: number[];
	}
	interface CommodityTypeResult {
		Code: string;
		Description: string;
	}
	interface SuggestedRouteResult extends CashTrack.Common.RouteResult {
		AMorPMCode: string;
		DeliveryRouteID: number;
		DestinationAreaID: number;
		Exclusive: boolean;
	}
	interface TankerResult {
		ID: number;
		Barcode: string;
		Name: string;
		TankerTypeID: number;
		TankerTypeCode: string;
		AreaID: number;
		IsActive: boolean;
	}
	interface UnitDestinationResult extends CashTrack.Common.UnitBriefResult {
		Address: string;
		Description: string;
		IsActive: boolean;
		SuspendedBeganOn: Date;
		SuspendedEndedOn: Date;
		IsBillable: boolean;
		UnitTypeCode?: string;
	}
	interface ShipperDestinationResult {
		ShipperID: number;
		Destination: CashTrack.Common.UnitDestinationResult;
		IsSameDay: boolean;
		LastUpdatedOn: Date;
		IsAreaCommodityDefaultDestination: boolean;
		DestinationID?: number;
	}
	interface ItemDetailsManifestNoBagoutResult {
		ID: number;
		Barcode: string;
		OriginalBarcode: string;
		ItemStatusCode: string;
		Amount: number;
		Quantity: number;
		IsMissing: boolean;
		IsReturnedToOrigin: boolean;
		IsFlaggedAsReturningToOrigin: boolean;
		BagOutBarcode: string;
		Commodity: CashTrack.Common.CommodityResult;
		Shipper: CashTrack.Common.UnitDestinationResult;
		Destination: CashTrack.Common.UnitDestinationResult;
		CurrentTankerID?: number;
		CustomerShipmentID?: number;
		IsABagOut?: boolean;
		ShipperID?: number;
	}
	interface ItemDetailsResult {
		ID: number;
		Barcode: string;
		OriginalBarcode: string;
		ItemStatusCode: string;
		Amount: number;
		Quantity: number;
		IsMissing: boolean;
		IsReturnedToOrigin: boolean;
		IsFlaggedAsReturningToOrigin: boolean;
		IsCancelled: boolean;
		BagOutBarcode: string;
		IsInBagout?: boolean;
		ParentBagout?: CashTrack.ManageItems.BagOutDetailsResult;
		CommodityID?: number;
		Commodity: CashTrack.Common.CommodityResult;
		Shipper: CashTrack.Common.UnitDestinationResult;
		Destination: CashTrack.Common.UnitDestinationResult;
		CurrentTankerID?: number;
		CustomerShipmentID?: number;
		CoinShipmentID?: number;
		IsABagOut?: boolean;
		ShipperID?: number;
		DestinationID?: number;
		SuggestedRoutes?: CashTrack.Common.SuggestedRouteResult[];
		SelectedRoute?: CashTrack.Common.SuggestedRouteResult;
		LastItemTransactionID?: number;
		LastItemTransaction?: CashTrack.Common.ItemTransactionResult;
		IsBagoutTanker?: boolean;
		IsECashItem?: boolean;
		ECashDestinationAreaID?: number;
		ECashExpirationDate?: Date;
	}
	interface ValidationErrorResult {
		Field: string;
		ErrorNumber: number;
		ErrorDescription: string;
	}
	interface ItemTransactionResult {
		ID: number;
		CreatedOn: Date;
		ItemTransactionTypeCode: string;
		Area: string;
		Door: string;
		LongDescription: string;
		Route: string;
		Truck: string;
		RouteID: number;
		To: number;
		From: number;
		ItemTransactionType?: CashTrack.Common.ItemTransactionTypeResult;
	}
	interface ItemTransactionTypeResult {
		Code: string;
		Description?: string;
		LongDescription?: string;
		IsCoin?: boolean;
		IsManual?: boolean;
		IsLoadTruck?: boolean;
		IsPickup?: boolean;
		IsDelivery?: boolean;
		IsReturnToSender?: boolean;
		IsRedelivery?: boolean;
	}
	interface RouteListResult {
		ID: number;
		RouteNumber: string;
		RouteStatusCode: string;
		PlannedDate: Date;
		EndDate: Date;
		RouteName: string;
		RouteTypeCode: string;
		AreaName: string;
		AreaID: number;
		MasterRouteID: number;
	}
	interface EmployeeResult {
		ID: number;
		Barcode: string;
		FirstName: string;
		MiddleInitial: string;
		LastName: string;
		IsActive: boolean;
		FullName: string;
		UserLevelCode: string;
		AreaID: number;
		UserArea: CashTrack.Common.AreaResult;
	}
	interface ShipperResult extends CashTrack.Common.UnitDestinationResult {
	}
	interface ItemDetailsTankerResult extends CashTrack.Common.ItemDetailsResult {
		NotFoundInTanker: boolean;
		CurrentRoute: CashTrack.Common.SuggestedRouteResult;
		CurrentTanker: CashTrack.Common.TankerResult;
	}
	interface MissingItemTankerResult {
		ID: number;
		OpenedOn: Date;
		OpenedByEmployeeFullName: string;
		MissingItemTypeDescription: string;
	}
	interface ItemDetailsTransferResult extends CashTrack.Common.ItemDetailsResult {
		CustomerShipmentID?: number;
		DatedBankSourceTypeID?: number;
		MissingItems?: CashTrack.Common.MissingItemResult[];
		RouteIds?: number[];
		ItemShipments?: CashTrack.ManageItems.ItemShipmentInRouteResult[];
		IsTankerFromOtherArea?: boolean;
	}
	interface MissingItemResult extends Garda.CashTrackNG.Core.Contracts.Models.Common.Results.MissingItemBaseResult {
		LastTransaction: CashTrack.Common.ItemTransactionResult;
		IsMissingItemReasonCodeReadOnly: boolean;
		Employee: CashTrack.Common.EmployeeResult;
	}
	interface CreateTransactionCommandBase {
		ItemID: number;
		CreatedBy: number;
		ItemTransactionTypeCode: string;
		RouteID: number;
		NextRouteID: number;
		FromID: number;
		Comments: string;
		CreatedOn: Date;
		DestinationAreaID: number;
		CommodityTypeCode: string;
		ItemStatus?: CashTrack.Consts.ItemStatusEnum;
	}
	interface DeclareCheckOutMissingItemCommand extends CashTrack.Common.Request {
		ItemID: number;
		AreaID: number;
		EmployeeID: number;
		MissingItemReasonCode: string;
		MissingItemTypeCode?: string;
		Comment: string;
	}
	interface CreateItemCommand {
		Barcode: string;
		CommodityID: number;
		DestinationID: number;
		ShipperID: number;
		Amount: number;
		Quantity: number;
		ItemStatusCode: string;
	}
	interface MergeEmployeeToRouteCommand extends CashTrack.Common.Request {
		RouteID: number;
		EmployeeBarcode: string;
		EmployeeRoleCode: string;
	}
	interface RemoveResult {
		Success: boolean;
		CanRetry: boolean;
		ValidationResults: CashTrack.BusinessRuleValidationResult[];
	}
	interface CoinDenominationResult {
		ID: number;
		Code: string;
		Description: string;
		CoinPackageSizeDescription: string;
		UnitAmount: number;
		SequenceNumber: number;
		CommodityID: number;
	}
	interface CoinInputSequenceResult {
		ID: number;
		CoinDenominationID: number;
		BankSourceTypeID: number;
		SequenceNumber: number;
	}
	interface CoinPackageSizeResult {
		ID: number;
		Code: string;
		Description: string;
		DenominationGroupCode: string;
	}
	interface CvsAreaResult {
		ID: number;
		Number: string;
		Description: string;
		OddCoinIncludedInCash: boolean;
	}
	interface DatedBankSourceTypeResult {
		ID: number;
		PreparedOn: Date;
		DefaultDeliveryOn: Date;
		ShipmentSourceCode: string;
		BankSourceTypeID: number;
	}
	interface DestinationShipmentCoinDenominationResult {
		CoinDenominationID: number;
		Code: string;
		Description: string;
		UnitAmount: number;
		SequenceNumber: number;
		CommodityID: number;
	}
	interface StopsRelatedToRoutesResult {
		RouteID: number;
		StopID: number;
		StopType: string;
		PlannedDate: Date;
	}
	interface CommonErrorResult {
		Code: string;
		Description: string;
		Notes: string;
		ErrorLevel: CashTrack.Consts.ErrorLevelEnum;
	}
	interface TransactionExtResult extends Garda.CashTrackNG.Core.Contracts.Models.Common.Results.TransactionResult {
		CreatedOn: Date;
	}
	interface ItemResult {
		ID: number;
	}
	interface CvsBankResult {
		ID: number;
		CVSBankNumber: string;
		CVSBankName: string;
	}
	interface BankSourceTypeDeliveryAreaResult {
		ID: number;
		BankSourceTypeID: number;
		AreaID: number;
	}
	interface BankSourceTypeExtResult extends CashTrack.Common.BankSourceTypeResult {
		ExistingCVSBankSources: CashTrack.ItemManifest.ExistingCVSBankSourceResult[];
	}
	interface BankSourceTypeResult {
		BankSourceTypeID: number;
		AreaID: number;
		Name: string;
		LongDescription: string;
		SourceTypeID: number;
		SourceTypeCode: string;
		ShipperUnitID: number;
		ShortDescription: string;
		ReportDescription: string;
		SourceMasterRouteID: number;
		EffectiveDate: Date;
		EndDate: Date;
		DefaultCommodityID: number;
		IsCodPayment: boolean;
		IsBankSourcePrintedOnPod: boolean;
		CoinPackageSizeID: number;
		IsCoinInputByAmount: boolean;
		UseCustomList: boolean;
		IsECashSource: boolean;
		OddCoinID: number;
		ECashExpirationDays: number;
		CoinInputSequenceResults: CashTrack.Common.CoinInputSequenceResult[];
		Errors: CashTrack.Common.ValidationErrorResult[];
	}
	interface DailyRouteGenerationStatusForDateResult {
		AreaID: number;
		Date: Date;
		IsRouteGenerated: boolean;
	}
	interface MasterRouteServiceDaysResult extends CashTrack.Common.MasterRouteResult {
		ServiceDayCodes: CashTrack.Common.DayCodeRouteCategoryResult[];
		RouteCategories: Garda.CashTrackNG.Core.Contracts.Models.Common.Results.RouteCategoryResult[];
	}
	interface DayCodeRouteCategoryResult extends CashTrack.Common.DayCodeResult {
		RouteCategoryCode: string;
	}
	interface DayCodeResult {
		Code: string;
		Description: string;
		DayNumber: number;
	}
	interface SiteDeliveryAreaValidationResult {
		DeliveryAreaNotFound: boolean;
		DeliverySiteNotFound: boolean;
		SiteDeliveryAreaID: number;
	}
	interface SiteDeliveryServiceDayResult {
		ID: number;
		SiteDeliveryAreaID: number;
		ServiceDayCode: string;
	}
	interface SiteDeliveryAreaResult {
		ID: number;
		SourceAreaID: number;
		DeliverySite: CashTrack.Common.UnitResult;
		DeliveryArea: CashTrack.Common.AreaResult;
		EffectiveDate: Date;
		EndDate: Date;
		SiteDeliveryServiceDay: Garda.CashTrackNG.Core.Contracts.Models.Common.Results.WeekDayResult[];
	}
	interface ManageAreaSiteDeliveryAreaResult {
		CommodityID: number;
		CommodityTypeCode: string;
		Description: string;
		Code: string;
		DefaultDestinationID: number;
		UnitBarcode: string;
		UnitName: string;
		IsRouted: boolean;
		IsActive: boolean;
	}
	interface RelatedStopResult {
		ID: number;
		RouteNumber: string;
		UnitBarcode: string;
		UnitName: string;
		AreaName: string;
	}
	interface RelatedMasterRouteResult {
		ID: number;
		AreaID: number;
		RouteCategoryCode: string;
		Duration: number;
		DayNumber: number;
		DefaultTankerID: number;
		MasterRouteVersionID: number;
		StopTypeCode: string;
	}
	interface RouteFromToResult {
		FromRouteID: number;
		NewRouteID: number;
	}
	interface MasterRouteVersionWithRouteIDResult {
		ID: number;
		MasterRouteID: number;
		EffectiveDateOn: Date;
		EndDateOn: Date;
		IsSimulationRouteVersion: boolean;
		FutureVersionDate: Date;
		IsSimulationVersionExists: boolean;
		SimulationVersionDate: Date;
		IsUpdateSuccess: boolean;
		RouteID: number;
	}
	interface OverrideUserResult {
		UserID: string;
		PIN: string;
		EffectiveDate: Date;
	}
	interface ContractLineCustomerFormResult {
		ID: number;
		customerNumber: number;
		customerName: string;
		unitNumber: number;
		unitName: string;
		StoreNumber: string;
		SiteAdress: string;
		ZipPostalCode: string;
		City: string;
		CountryCode: string;
		ServiceChanges: Garda.CashTrackNG.Core.Contracts.Models.Common.Results.ContractLineServiceChangeLightResult[];
	}
	interface CommodityBaseResult {
		ID: number;
	}
	interface CommodityBarcodeSuffixResult extends CashTrack.Common.CommodityBaseResult {
		BarcodeSuffix: string;
	}
	interface CommodityUnitAmountResult extends CashTrack.Common.CommodityBaseResult {
		UnitAmount: number;
	}
	interface DatedBankSourceTypeIDResult {
		ID: number;
	}
	interface EmployeeIdentificationResult {
		ID: number;
		Barcode: string;
		FirstName: string;
		LastName: string;
		FullName: string;
	}
	interface LongResult {
		Value: number;
	}
	interface CancelStopOnMasterRouteResult {
		Success: boolean;
		CanRetry: boolean;
		ValidationResults: CashTrack.BusinessRuleValidationResult[];
		RelatedStopToRemove: string;
	}
	interface MasterRouteExtResult {
		ID: number;
		RouteNumber: string;
		RouteName: string;
		RouteTypeCode: string;
		IsSpecialRoute: boolean;
		AreaID: number;
		IsRouteAM: boolean;
		TypeCode: string;
		RouteID: number;
	}
	interface MasterRouteListFromResult {
		Routes: CashTrack.Common.MasterRouteResult[];
		Zips: CashTrack.Common.ZipCodeResult[];
		Holidays: CashTrack.Common.HolidayResult[];
	}
	interface ZipCodeResult {
		ZipCode: string;
	}
	interface MasterRouteDayDataResult {
		MasterRouteVersion: CashTrack.Common.MasterRouteVersionResult;
		CountOfWeeks: number;
		AreaHolidays: CashTrack.Common.HolidayResult[];
	}
	interface MasterRouteVersionResult {
		ID: number;
		MasterRouteID: number;
		EffectiveDateOn: Date;
		EndDateOn: Date;
		IsSimulationRouteVersion: boolean;
		FutureVersionDate: Date;
		IsSimulationVersionExists: boolean;
		SimulationVersionDate: Date;
		IsUpdateSuccess: boolean;
		MasterRouteDetails: CashTrack.Common.MasterRouteDetailResult[];
		AreaHolidays: CashTrack.Common.HolidayResult[];
	}
	interface MasterRouteDetailResult {
		ID: number;
		MasterRouteVersionID: number;
		ServiceDayCode: string;
		ServiceDayDescription: string;
		CutoffRouteTimeOn: System.TimeSpan;
		BeginRouteTimeOn: System.TimeSpan;
		EndRouteTimeOn: System.TimeSpan;
		DefaultTankerID: number;
		AreaHolidayDateID: number;
		RouteCategoryCode: string;
		IsActive: boolean;
		FirstStopRecordID: number;
		LastStopRecordID: number;
		CreatedBy: number;
		CreatedOn: Date;
		LastUpdatedBy: number;
		LastUpdatedOn: Date;
		SuspendedBegunOn: Date;
		SuspendedEndedOn: Date;
		Duration: number;
		MasterRouteID: number;
		CutoffRouteTimeOnAsDate: Date;
		BeginRouteTimeOnAsDate: Date;
		EndRouteTimeOnAsDate: Date;
		Date: Date;
		OldDefaultTankerID: number;
		DefaultTankerBarcode: string;
		RouteAMorPM: string;
		DayNumber: number;
		IsInActive: boolean;
		IsSuspended: boolean;
		IsCanSuspend: boolean;
		CanSelectTanker: boolean;
		CanBeDeactivated: number;
		MasterStopID: number;
		HasStops: boolean;
	}
	interface CreateOrUpdateResult {
		Success: boolean;
		CanRetry: boolean;
		ValidationResults: CashTrack.BusinessRuleValidationResult[];
	}
	interface CommodityCodeResult {
		ID: number;
		Code: string;
		Description: string;
		CommodityTypeCode: string;
	}
	interface AreaForCheckinResult extends CashTrack.Common.AreaResult {
		IsManagingBranchTanker: boolean;
	}
	interface AreaForCVSResult extends CashTrack.Common.AreaResult {
		IsPieceCountToGardaCVSAllowed: boolean;
	}
	interface HolidayCodeResult {
		ID: number;
		Code: string;
		Description: string;
	}
	interface ManageAreaCommodityResult {
		CommodityID: number;
		CommodityTypeCode: string;
		Description: string;
		Code: string;
		DefaultDestinationID: number;
		UnitBarcode: string;
		UnitName: string;
		IsRouted: boolean;
		IsActive: boolean;
	}
	interface ManageAreaResult extends CashTrack.Common.AreaResult {
		Number: number;
		TimeZoneCode: string;
		EndDate: Date;
		IsManagingBranchTanker: boolean;
		IsPieceCountToGardaCVSAllowed: boolean;
	}
	interface ManageAreaSourceRouteResult {
		MasterRouteID: number;
		RouteTypeCode: string;
		RouteNumber: string;
		Name: string;
		AMorPMCode: string;
		NumberOfCrew: number;
		IsHHTPrinterRequired: boolean;
		LastGeneratedDailyRouteDateOn: Date;
		IsSpecialRoute: boolean;
		MasterRouteVersionID: number;
		EffectiveDateOn: Date;
		EndDateOn: Date;
		IsSimulationRouteVersion: boolean;
	}
	interface ManageAreaVaultDoorResult {
		ID: number;
		Barcode: string;
		Description: string;
		IsActive: boolean;
	}
	interface RouteWithDestinationAreaResult extends CashTrack.Common.RouteResult {
		DestinationAreaIDs: number[];
	}
	interface SearchTextResult {
		Found: boolean;
		SearchedText: string;
	}
	interface ShipperDestinationCitResult extends CashTrack.Common.ShipperDestinationResult {
		Commodity: CashTrack.Common.CommodityCodeResult;
		EffectiveDate: Date;
		EndDate: Date;
	}
	interface SourceTypeResult {
		ID: number;
		Code: string;
		Description: string;
	}
	interface DatedBankSourceTypesResult {
		DatedBankSourceTypes: CashTrack.Common.DatedBankSourceTypeResult[];
		HasNoBST: boolean;
		HasBSTWithSameShipper: boolean;
		HasBSTWithSameDestination: boolean;
		HasConsistentPackageCoinSize: boolean;
		HasSameInputCoinType: boolean;
	}
	interface EquipmentRouteResult {
		ID: number;
		Barcode: string;
		EquipmentTypeCode: string;
		IsActive: boolean;
		SiteLocationID: number;
		UnitID: number;
		UnitName: string;
		UnitBarcode: string;
		Address: CashTrack.Common.AddressResult;
		RouteNumber: string;
		RouteName: string;
	}
	interface ItemTransactionWithEmployeeInfoResult extends CashTrack.Common.ItemTransactionResult {
		OpenedBy: CashTrack.Common.EmployeeResult;
	}
	interface MissingItemWithEmployeeInfoResult extends Garda.CashTrackNG.Core.Contracts.Models.Common.Results.MissingItemBaseResult {
		OpenedBy: CashTrack.Common.EmployeeResult;
		ClosedBy: CashTrack.Common.EmployeeResult;
		MissingItemTypeCode: string;
		MissingItemTypeDescription: string;
		ItemID: number;
		LastTransaction: CashTrack.Common.ItemTransactionWithEmployeeInfoResult;
	}
	interface UnitForStopValidationResult extends CashTrack.Common.UnitBriefResult {
		SuspendedBeganOn: Date;
		SuspendedEndedOn: Date;
		Description: string;
		FullAddress: CashTrack.Common.AddressResult;
		Contracts: string;
		ContractLines: string;
		JDEServiceLocationNumber: number;
	}
	interface UpdatableTankerResult {
		ID: number;
		Barcode: string;
		Name: string;
		TankerTypeID: number;
		TankerTypeCode: string;
		AreaID: number;
		IsActive: boolean;
		IsTankerUpdatable: boolean;
		IsActiveUpdatable: boolean;
		IsTankerTypeUpdatable: boolean;
	}
	interface VehicleEquipmentResult {
		ID: number;
		Barcode: string;
		EquipmentTypeCode: string;
		IsActive: boolean;
		VehicleID: number;
	}
	interface VerifyRouteKeyResult {
		ScannedEquipmentResults: CashTrack.Common.EquipmentResult[];
		NotScannedEquipmentResults: CashTrack.Common.EquipmentResult[];
		ErrorResults: CashTrack.Common.CommonErrorResult[];
	}
	interface EquipmentResult {
		ID: number;
		Barcode: string;
		EquipmentTypeCode: string;
		IsActive: boolean;
		SiteLocationID: number;
		UnitID: number;
		UnitName: string;
		UnitBarcode: string;
		Address: CashTrack.Common.AddressResult;
		IsDuplicate?: boolean;
	}
	interface UnitAddressResult {
		UnitID: number;
		Barcode: string;
		Name: string;
		Street: string;
		City: string;
		State: string;
		Zipcode: string;
		IsActive: boolean;
	}
	interface CustomerShipmentResult {
		ID: number;
	}
	interface CoinShipmentResult {
		ID: number;
	}
	interface ContractLineForCITUnitResult {
		ID: number;
		ProductBillingCode: string;
		TriPartyCode: string;
		ContractLineNumber: number;
		Description: string;
		LiabilityTreshold: number;
		PremiseTimeThreshold: number;
		ItemThreshold: number;
		IsActive: boolean;
		IsCheckLiabilityBilled: boolean;
		ProductCode: Garda.CashTrackNG.Core.Contracts.Models.Common.Results.ProductCodeResult;
		Contract: CashTrack.Common.ContractResult;
		Attributes: CashTrack.Common.ContractLineAttributeForCITUnitResult[];
	}
	interface ContractResult {
		ID: number;
		CustomerID: number;
		Number: number;
		Description: string;
		Version: number;
		Type: string;
		IsActive: boolean;
		CreatedBy: number;
		CreatedOn: Date;
		LastUpdatedBy: number;
		LastUpdatedOn: Date;
		ConjunctiveServiceCode: string;
		IsAuthorizationNumberRequired: boolean;
		Customer: CashTrack.Administration.CustomerResult;
		ConjunctiveService: CashTrack.Common.ConjunctiveServiceCodeResult;
	}
	interface ConjunctiveServiceCodeResult {
		Code: string;
		Description: string;
		IsConjunctiveService: boolean;
	}
	interface ContractLineAttributeForCITUnitResult {
		ID: number;
		ContractLineID: number;
		UnitID: number;
		AttributeBillingCode: string;
		ShortAttributeCodeID: number;
		IsActive: boolean;
		LastUpdatedOn: Date;
		ShortAttributeCode: CashTrack.Common.ShortAttributeCodeResult;
	}
	interface ShortAttributeCodeResult {
		ID: number;
		Code: string;
		Description: string;
		UnitOfMeasureCode: string;
		VariableBillingCode: string;
		IsSeasonalService: boolean;
		IsAppliedToConjunctiveService: boolean;
		CommodityIdToBill: number;
		BillingLineComment: string;
		ShortDescriptionBillingType: string;
		IsActive: boolean;
		ServiceOrExtra: string;
		CITAttributeDescription: string;
		IsFixedDayService: boolean;
	}
	interface ItemDetailsManifestResult extends CashTrack.Common.ItemDetailsManifestNoBagoutResult {
		Penny: number;
		Nickel: number;
		Dime: number;
		Quarter: number;
		Half: number;
		Dollar: number;
	}
	interface StopForEnterShipmentResult {
		ID: number;
		RouteID: number;
		StopTypeCode: string;
		IsShipmentStop: boolean;
		CutOffDate: Date;
	}
	interface TankerIDResult {
		ID: number;
	}
	interface AreaIdResult {
		AreaID: number;
	}
	interface AutomaticOnDemandResult {
		AreaID: number;
		AttributeID: number;
		StopID: number;
		MasterStopID: number;
		UnitID: number;
		CitAttributeServiceID: number;
		CitAttributeServiceTypeCode: string;
		ServiceCode: string;
		PlannedDate: Date;
	}
	interface AMorPMRouteResult {
		Code: string;
		Description: string;
	}
	interface AreaDetailResult extends CashTrack.Common.AreaResult {
		CurrentSimulationEffectiveDateOn: Date;
		TimeZoneCode: string;
		IsManagingBranchTanker: boolean;
		CutOffAreaTimeAsDate: Date;
	}
	interface MasterRouteListResult {
		ID: number;
		IsSelected: boolean;
		Category: string;
		RouteNumber: string;
		RouteName: string;
		RouteTypeDescription: string;
		DayTimeCode: string;
		DaySunday: CashTrack.Common.MasterRouteDayTimeResult;
		DayMonday: CashTrack.Common.MasterRouteDayTimeResult;
		DayTuesday: CashTrack.Common.MasterRouteDayTimeResult;
		DayWednesday: CashTrack.Common.MasterRouteDayTimeResult;
		DayThursday: CashTrack.Common.MasterRouteDayTimeResult;
		DayFriday: CashTrack.Common.MasterRouteDayTimeResult;
		DaySaturday: CashTrack.Common.MasterRouteDayTimeResult;
	}
	interface MasterRouteDayTimeResult {
		ID: number;
		DayNumber: number;
		Time: Date;
		IsActive: boolean;
		IsSuspended: boolean;
	}
	interface AssignTankerResult {
		ID: number;
		Barcode: string;
		Name: string;
		TankerTypeCode: string;
		IsManifested: boolean;
	}
	interface MasterStopUpdateResult extends CashTrack.Common.MasterStopEditRetrieveResult {
		Errors: CashTrack.Common.ValidationErrorResult[];
	}
	interface MasterStopEditRetrieveResult extends CashTrack.Common.MasterStopResult {
		AreaName: string;
		AreaID: number;
		Unit: string;
		RouteNumber: string;
		TimeZoneCode: string;
		RouteCategory: string;
		MinimumServiceDateBegunOn: Date;
		IsUnitBillable: boolean;
	}
	interface TankerTypeResult {
		Code: string;
		Description: string;
		ID: number;
		IsUpdatedByTankerFunction: boolean;
	}
	interface TankerWithTypeResult extends CashTrack.Common.TankerResult {
		TankerType: CashTrack.Common.TankerTypeResult;
	}
	interface PrintKeyReportResult {
		Address1: string;
		AreaID: number;
		AreaName: string;
		AreaNumber: number;
		CityName: string;
		EndDate: Date;
		EquipmentBarcode: string;
		EquipmentID: number;
		LastRouteID: number;
		LastRouteName: string;
		LastRouteNumber: string;
		LastRoutePlannedDate: Date;
		MasterRouteName: string;
		MasterRouteNumber: string;
		PlannedDate: Date;
		RouteID: number;
		SequenceNumber: number;
		StopID: number;
		UnitBarcode: string;
		UnitID: number;
		UnitName: string;
		DatasetNumber: number;
	}
	interface AssignRouteResult {
		ID: number;
		MasterRouteID: number;
		MasterRouteDetailID: number;
		TankerID: number;
		PlannedDate: Date;
		Category: string;
		RouteNumber: string;
		RouteName: string;
		RouteTypeDescription: string;
		DayTimeCode: string;
		ServiceDayCode: string;
		RouteTypeCode: string;
		IsServiceByTruck: boolean;
		IsInternalRoute: boolean;
	}
	interface StopAltResult {
		ID: number;
		IsManual: boolean;
		IsCancelled: boolean;
		IsBillable: boolean;
		SequenceNumber: number;
		StartedOn: Date;
		EndedOn: Date;
		CustomerStartedOn: Date;
		CustomerEndedOn: Date;
		Route: CashTrack.Common.RouteResult;
		Unit: Garda.CashTrackNG.Core.Contracts.Models.Common.Results.UnitWithShipmentsResult;
		StopServices: CashTrack.Common.StopServiceAltResult[];
		MasterStopID: number;
		MasterRouteVersions: CashTrack.Common.MasterRouteVersionResult[];
	}
	interface StopServiceAltResult {
		ID: number;
		FixeServiceDescription: string;
		StopServiceBillingTypeID: number;
		CreatedBy: number;
		CreatedOn: Date;
		LastUpdatedBy: number;
		LastUpdatedOn: Date;
	}
	interface UnitInformationResult {
		Unit: CashTrack.Common.UnitResult;
		Equipments: CashTrack.Common.EquipmentResult[];
		SiteLocation: CashTrack.Common.SiteLocationResult;
		ServiceDays: string[];
		KeyRequired: boolean;
	}
	interface UserLevelResult {
		Code: string;
		Description: string;
	}
	interface RouteTypeAndStopTypeResult {
		RouteTypeCode: string;
		StopTypeCode: string;
	}
	interface MasterRouteUpdateResult {
		MasterRoute: CashTrack.Common.MasterRouteResult;
		Errors: CashTrack.Common.ValidationErrorResult[];
	}
	interface ContractLineServiceChangeResult {
		ID: number;
		ContractLineID: number;
		TemporaryChangeStartDate: Date;
		TemporaryChangeOriginalStartDate: Date;
		TemporaryChangeEndDate: Date;
		ServiceFrequency: Garda.CashTrackNG.Core.Contracts.Models.Common.Results.ServiceFrequencyCodeResult;
		ServiceChangeType: Garda.CashTrackNG.Core.Contracts.Models.Common.Results.ServiceChangeTypeResult;
		ServiceDays: Garda.CashTrackNG.Core.Contracts.Models.Common.Results.WeekDayResult[];
		Area: CashTrack.Common.AreaResult;
		ContractLine: CashTrack.Common.ContractLineForCITUnitResult;
		ServiceStartDate: Date;
		Notes: string;
	}
	interface DateTimeResult {
		Result: Date;
	}
	interface MasterRouteElementResult {
		ID: number;
		AreaID: number;
		RouteTypeCode: string;
		AMorPMCode: string;
		IsRouteAM: boolean;
		RouteNumber: string;
		Name: string;
		IsHHTRoute: boolean;
		NumberOfCrew: number;
		IsHHTPrinterRequired: boolean;
		IsSpecialRoute: boolean;
		LastGeneratedDailyRouteDateOn: Date;
		IsInternalRoute: boolean;
		IsSimulated: boolean;
		IsShuttleWithActiveAreaStops: boolean;
		IsOverDaysRoute: boolean;
		ODRDaysQty: number;
		Area: CashTrack.Common.AreaDetailResult;
		RouteType: CashTrack.Common.RouteTypeResult;
		MasterRouteVersions: CashTrack.Common.MasterRouteVersionResult[];
		Routes: CashTrack.Common.RouteResult[];
	}
	interface NonRevenueUnitResult {
		ID: number;
		UnitID: number;
		CITServiceID: number;
		EffectiveDate: Date;
		EndDate: Date;
		IsActive: boolean;
		IsAreaUnit: boolean;
		CreatedBy: number;
		CreatedOn: Date;
		LastUpdatedBy: number;
		LastUpdatedOn: Date;
		NonRevenueServiceDays: Garda.CashTrackNG.Core.Contracts.Models.Common.Results.WeekDayResult[];
		Area: CashTrack.Common.AreaResult;
	}
	interface SystemSettingResult {
		CurrentBillingYearMonth: Date;
		LastBillingTransferredOn: Date;
		NextBillingTransferredOn: Date;
		NextMonthEndTransferredOn: Date;
		BillingAuthorizedOn: Date;
		BillingAuthorizedBy: number;
		LastJDEContractLineImportedOn: Date;
		NumberOfMasterRoutePlannedWeeks: number;
		LastNextPlannedDateGeneratedOn: Date;
	}
	interface BooleanResult {
		Result: boolean;
	}
	interface SubdivisionCountryResult {
		Code: string;
		CountryCode: string;
	}
	interface HolidayServiceSoldResult {
		ID: number;
		HolidayCode: string;
		ReportCode: string;
		ReportCodeDescription: string;
		UnitID: number;
		AreaID: number;
		IsHolidayServiced: boolean;
		IsHolidayBilled: boolean;
		IsReportHolidayBilled: boolean;
		HolidayDate: Date;
	}
	interface NonBillableReasonCodeResult {
		ID: number;
		Description: string;
		IsSelectedInDashboard: boolean;
		IsVariableServiceRequired: boolean;
	}
	interface MissedStopReasonResult {
		Code: string;
		IsGardaResponsibility: boolean;
		Description: string;
		HHTCommodityCode: string;
		HHTCommodityDescription: string;
		IsActive: boolean;
		IsHHTCommodity: boolean;
		HHTSortOrder: number;
	}
	interface ManualStopResult {
		PickupCount: number;
		DeliveryCount: number;
		HasError: boolean;
		Editable: boolean;
		IsManuallyBilled: boolean;
		TotalCount: number;
		Stop: CashTrack.Common.StopResult;
	}
	interface MissingItemReasonResult {
		Code: string;
		IsDisplayedAtCheckInOnly: boolean;
		IsDisplayedAtCVSCarrierCheckInOnly: boolean;
		IsDisplayedAtEditMissingItemOnly: boolean;
	}
	interface MissedStopResult {
		ID: number;
		Code: string;
		Reason: string;
		Comments: string;
		IsGardaResponsibility: boolean;
		CreatedOn: Date;
		HasError: boolean;
		HasMissingReason: boolean;
		Editable: boolean;
		OrderByError: number;
		IsManuallyBilled: boolean;
		StopID: number;
		Stop_ID: number;
		Stop: CashTrack.Common.StopResult;
		TotalCount: number;
	}
	interface TransactionTypeResult {
		ID: number;
		Code: string;
	}
	interface AreaCommodityResult {
		AreaID: number;
		DefaultDestinationID: number;
		IsRouted: boolean;
		IsActive: boolean;
		IsCommodityScanned: boolean;
		Commodity: CashTrack.Common.CommodityResult;
	}
	interface CoinSheetDeliveryRouteResult {
		SourceRoute: CashTrack.Common.MasterRouteResult;
		DeliveryRoute: CashTrack.Common.MasterRouteResult;
	}
	interface StopExtraFeeResult {
		ID: number;
		QuantityToBill: number;
		IsExcessBilled: boolean;
		Threshold: number;
		PickedSum: number;
		DeliverySum: number;
		ItemsList: CashTrack.OperationalFollowUp.ItemTransactionBillingDashboardResult[];
		Attribute: CashTrack.CITService.AttributeResult;
		Stop: CashTrack.Common.StopResult;
		TotalCount: number;
	}
	interface VaultDoorResult {
		ID: number;
		Barcode: string;
		AreaID: number;
	}
	interface VehicleResult {
		ID: number;
		Barcode: string;
		Make: string;
		Model: string;
		Year: number;
		VehicleIdentificationNumber: string;
		InitialMileage: number;
		LastRouteMileage: number;
		IsUnitOfMeasureByMile: boolean;
		NumberOfKeys: number;
		IsActive: boolean;
	}
	interface ReturnECashQuery extends CashTrack.Common.Request {
		RouteID: number;
		ItemID: number;
		TankerID: number;
		EmployeeID: number;
		CurrentTankerID: number;
		Comments: string;
		DestinationAreaID: number;
	}
	interface RouteByAreaAndDateQuery {
		AreaID: number;
		RouteNumber: string;
		RouteDate: Date;
		MaxElementsNumber: number;
		IncludeUnloadingRoutes: boolean;
	}
	interface ItemByShipperQuery {
		Barcode: string;
		Shipper: string;
		ShipperID?: number;
	}
	interface SimilarItemQuery {
		Barcode: string;
		AreaID: number;
	}
	interface MasterStopDailySequenceQuery {
		UnitID: number;
		MasterRouteDetailID: number;
	}
	interface StopsByMasterRouteDetailAndDateQuery {
		MasterRouteDetailID: number;
		Date: Date;
	}
	interface SiteDeliveryAreaByAreaQuery {
		AreaID: number;
		IncludeExpiredValues: boolean;
	}
	interface TransactionByIDQuery extends CashTrack.Common.Request {
		TransactionID: number;
	}
	interface LastTransactionMissingItemQuery {
		LastTransactionID: number;
	}
	interface RelatedMasterStopQuery {
		RelatedMasterStopID: number;
		MasterRouteDetailID: number;
		UnitID: number;
		StopID: number;
	}
	interface MasterStopByDeliverySiteQuery {
		DeliveryAreaID: number;
		DeliverySiteID: number;
		EffectiveDate: Date;
		EndDate: Date;
		SiteDeliveryServiceDay: string[];
	}
	interface MasterStopByDeliveryAreaQuery {
		DeliveryAreaID: number;
		SourceAreaID: number;
		EffectiveDate: Date;
		EndDate: Date;
		SiteDeliveryServiceDay: string[];
	}
	interface MasterStopInfoByMasterRouteDetailQuery extends CashTrack.Common.MasterRouteDetailsByIdAndUnitQuery {
		StopTypeCode: string;
	}
	interface MasterRouteDetailsByIdAndUnitQuery extends CashTrack.Common.Request {
		MasterRouteDetailID: number;
		UnitID: number;
		ServiceDateBegunOn: Date;
		ServiceDateEndedOn: Date;
		StopServiceID: number;
	}
	interface SiteDeliveryAreaQuery {
		ID: number;
	}
	interface ActiveBankSourcesForAreaCommodityShipperQuery {
		AreaID: number;
		ShipperID: number;
		DefaultCommodityId: number;
	}
	interface MasterRouteDetailsByMasterRouteIdAndUnitQuery extends CashTrack.Common.Request {
		MasterRouteID: number;
		ServiceDayCode: string;
		RouteCategoryCode: string;
		ServiceDateBegunOn: Date;
		ServiceDateEndedOn: Date;
	}
	interface MasterRouteSourceRouteByAreaDate {
		AreaID: number;
		ForDate: Date;
	}
	interface OverrideUserQuery {
		UserID: string;
		RequestDateTime: Date;
		OverrideTypeCode: string;
	}
	interface RouteByAreaUnitQuery extends CashTrack.Common.RouteBaseQuery {
		AreaUnitID: number;
	}
	interface RouteBaseQuery extends CashTrack.Common.Request {
		IncludeOpen: boolean;
		IncludeInService: boolean;
		IncludeUnloading: boolean;
		IncludeArrived: boolean;
		IncludeClosed: boolean;
		IncludeHHTLoading?: boolean;
	}
	interface RouteForOtherAreaByUnitAreaQuery extends CashTrack.Common.Request {
		AreaID: number;
		AreaUnitID: number;
		RouteNumber: string;
		MaxElementsNumber: number;
		IncludeUnloadingRoutes: boolean;
	}
	interface RouteForTransferFromOtherAreaQuery {
		AreaUnitID: number;
	}
	interface TankerMasterRouteQuery {
		TankerID: number;
		MasterRouteID: number;
	}
	interface RouteByMasterStopIdAndPlannedDateQuery {
		MasterStopID: number;
		PlannedDate: Date;
	}
	interface ItemBarcodeByShipperQuery {
		Barcode: string;
		ShipperID: number;
	}
	interface UnitsByBarcodeQuery {
		Filter: string;
		IsActive?: boolean;
	}
	interface UnitsByDateQuery {
		EndOfToday: Date;
	}
	interface CoinDenominationByBankSourceTypeIDQuery {
		BankSourceTypeID: number;
	}
	interface DatedBankSourceTypesByAreaAndDateQuery {
		PreparedOn: Date;
		AreaID: number;
	}
	interface BankSourceTypeByDBSTIDQuery {
		DatedBankSourceTypeID: number;
	}
	interface FindActiveAreaQuery extends CashTrack.Common.Request {
		LinkedToUnit: boolean;
	}
	interface MasterStopEditQuery {
		ID: number;
		ServiceDate: Date;
	}
	interface RouteByRouteNumberForDipatchLogQuery {
		RouteNumber: string;
		Today: Date;
	}
	interface VaultDoorByBarcodeAreaQuery {
		Barcode: string;
		AreaID: number;
	}
	interface VerifyAreaSourceByRouteNumberAreaIDQuery {
		RouteNumber: string;
		AreaID: number;
	}
	interface VehicleEquipmentQuery {
		VehicleID: number;
		Type: string;
	}
	interface VehicleKeyByBarcodeQuery {
		KeyBarcode: string;
		VehicleID: number;
	}
	interface BankSourceTypeByAreaQuery {
		AreaID: number;
		SearchCriteria: string;
	}
	interface CommoditySearchQuery {
		SearchText: string;
	}
	interface DatedBankSourceTypeBySourcesAndDateQuery {
		BankSourceTypesID: number[];
		PreparedOn: Date;
		DeliveryDate: Date;
		IsCoin: boolean;
		MasterRouteID: number;
		SourceTypeID: number;
		IsUsingSourceRoute: boolean;
	}
	interface CoinSheetRoutesQuery {
		DeliveryDate: Date;
		AreaID: number;
		AMorPMCode: string;
	}
	interface AreasQuery {
		Areas: number[];
	}
	interface BankSourceTypesByMasterRouteAndTypeQuery {
		MasterRouteID: number;
		SourceTypeID: number;
	}
	interface DatedBankSourceTypesByMasterRouteIDAndDateQuery {
		MasterRouteID: number;
		PreparedOn: Date;
		SourceTypeID: number;
	}
	interface DatedBankSourceTypesBySourceAndDateQuery {
		BankSourceTypeIDs: number[];
		PreparedOn: Date;
		DeliveryDate: Date;
		IsCoin: boolean;
	}
	interface ItemIDsQuery {
		IDs: number[];
	}
	interface MasterRouteByAreaIDQuery {
		AreaID: number;
		IsInternalRoute?: boolean;
	}
	interface RouteByAreaAndStatusQuery extends CashTrack.Common.RouteBaseQuery {
		AreaID: number;
		IsInternalRoute: boolean;
		IsParcelCheckInCompleted: boolean;
	}
	interface SourceTypeQuery {
		Code: string;
	}
	interface StopByEquipmentCodeAndRouteIdQuery {
		RouteID: number;
		EquipmentType: string;
	}
	interface TankerByAreaAndTypeQuery {
		AreaID: number;
		TankerType: CashTrack.Consts.TankerTypeEnum;
	}
	interface ByUnitAndAreaAndDateQuery extends Garda.CashTrackNG.Core.Contracts.Models.Common.Queries.ByUnitAndDateQuery {
		AreaID: number;
	}
	interface VerifyRouteKeyQuery {
		RouteNumber: string;
		PlannedDate: Date;
	}
	interface UnitByCvsAreaIDQuery {
		CvsAreaID: number;
	}
	interface AreaCommodityByIDQuery {
		AreaID: number;
		CommodityID: number;
	}
	interface CommodityByPackageSizeAndDenimnationQuery {
		CoinDenominationID: number;
		CoinPackageSizeID: number;
	}
	interface CustomerShipmentsQuery {
		DestinationUnitID: number;
		PreparedOn: Date;
		InitialDeliveryDate: Date;
		ManifestingDate: Date;
		IsCODPayment: boolean;
	}
	interface DatedBankSourceTypeBySourceByIDQuery {
		ID: number;
	}
	interface EquipmentByUnitIDQuery {
		UnitID: number;
		EquipmentTypeCode: string;
	}
	interface ItemShipmentByRouteNumberAndPlannedDateQuery {
		RouteNumber: string;
		PlannedDate: Date;
		ExcludeStopNoShipment: boolean;
	}
	interface MasterRouteByAreaAndDateQuery extends CashTrack.Common.RouteBaseQuery {
		AreaID: number;
		PlannedDate: Date;
		IncludeSourceRoute: boolean;
		AMorPMCode?: string;
		ExcludeInternalRoutes: boolean;
	}
	interface MissingItemReasonQuery {
		IsDisplayedAtCheckInOnly?: boolean;
		IsDisplayedAtCvsCarrierCheckInOnly?: boolean;
		IsDisplayedAtEditMissingItemOnly?: boolean;
	}
	interface RouteByEmployeeIdQuery extends CashTrack.Common.Request {
		EmployeeId: number;
	}
	interface RouteByMasterRouteIDAndRouteTypeQuery {
		MasterRouteID: number;
		PlannedDate: Date;
		RouteStatusCode: string;
	}
	interface RouteByPlannedDateAndEmployeeIdQuery extends CashTrack.Common.RouteByEmployeeIdQuery {
		PlannedDate: Date;
	}
	interface RouteByMasterRouteIDAndPlannedDateQuery {
		MasterRouteID: number;
		PlannedDate: Date;
	}
	interface RouteByVehicleIdQuery extends CashTrack.Common.Request {
		VehicleId: number;
	}
	interface RouteTypeQuery {
		Code?: string;
		IsServiceByTruck?: boolean;
		IsDisplayedInMasterRouteDashboard?: boolean;
		IsDailyGenerated?: boolean;
		IsInternalRoute?: boolean;
		IsSimulated?: boolean;
	}
	interface StopServiceByIDQuery {
		StopServiceID: number;
	}
	interface UnitsByBarcodeOrNameQuery {
		Filter: string;
		IsActive?: boolean;
	}
	interface UnitsByCityQuery {
		Filter: string;
		IsActive: boolean;
	}
	interface UnitsByAddressQuery {
		Filter: string;
		IsActive: boolean;
	}
	interface UnitsByZipQuery {
		Filter: string;
		IsActive: boolean;
	}
	interface ActiveBankSourcesForAreaQuery {
		PeriodStart: Date;
		PeriodEnd: Date;
		AreaID: number;
	}
	interface SearchAreaQuery {
		SearchText: string;
		LinkedToUnit: boolean;
	}
	interface AreaByMasterStop {
		MasterStopID: number;
	}
	interface DatedBankSourceTypeBySourceAndDateQuery {
		BankSourceTypeID: number;
		PreparedOn: Date;
		DeliveryDate: Date;
		IsCoin: boolean;
		MasterRouteID: number;
		SourceTypeID: number;
		IsUsingSourceRoute: boolean;
	}
	interface RouteByMasterRouteAndDateQuery {
		MasterRouteID: number;
		Date: Date;
	}
	interface BankSourceTypeByIDQuery {
		ID: number;
	}
	interface CoinDenominationByCoinPackageSizeIDQuery {
		CoinPackageSizeID: number;
	}
	interface CoinInputSequenceByBankSourceTypeIDQuery {
		BankSourceTypeID: number;
	}
	interface StopServiceByStopIDQuery {
		StopID: number;
	}
	interface RoutesByAreaAndRouteTypeQuery extends CashTrack.Common.RoutesByAreaQuery {
		RouteTypeCode: string;
	}
	interface RoutesByAreaQuery extends CashTrack.Common.RouteBaseQuery {
		AreaID: number;
	}
	interface RoutesByAreaForTransferOtherQuery extends CashTrack.Common.RoutesByAreaQuery {
		AreaUnitID: number;
		RouteTypeCode: string;
		PlannedDate: Date;
	}
	interface NonRevenueUnitByUnitIDQuery {
		UnitID: number;
	}
	interface NonRevenueUnitQuery {
		ID: number;
	}
	interface SearchCityQuery {
		SearchText: string;
	}
	interface MissingItemByItemIdQuery {
		ItemId: number;
	}
	interface AreaByPlannedDateAndFilterQuery {
		PlannedDate: Date;
		Filter: string;
	}
	interface RouteByAreaAndPlannedDateQuery {
		AreaID: number;
		PlannedDate: Date;
		AMorPMCode: string;
	}
	interface StopWithRouteEquipmentByRouteIDsQuery {
		RouteIDs: number[];
		EquipmentType: string;
		IsDraft: boolean;
		PrintedOn: Date;
	}
	interface TankerByIDQuery {
		TankerID: number;
	}
	interface AreaByIDQuery extends CashTrack.Common.Request {
		AreaID: number;
	}
	interface CommodityByAreaListQuery {
		AreaIDs: number[];
	}
	interface EquipmentByBarcodeQuery {
		Barcode: string;
		EquipmentTypeCode: string;
	}
	interface RouteByAreaQuery extends CashTrack.Common.Request {
		AreaID: number;
		RouteNumber: string;
		MaxElementsNumber: number;
		IncludeUnloadingRoutes: boolean;
	}
	interface UnitByDateQuery {
		UnitID: number;
		PlannedServiceDate: Date;
	}
	interface HHTBeginDayProcessingQuery {
		PlannedProcessingHour: System.TimeSpan;
	}
	interface EmployeeByFilterQuery {
		Filter: string;
		ExcludeDispatcherEmployees: boolean;
	}
	interface EmployeeByIDQuery extends CashTrack.Common.Request {
		ID: number;
	}
	interface RouteEmployeeByEmployeeIDQuery {
		EmployeeID: number;
	}
	interface UnitTypeQuery {
		IsNonBillableUnit: boolean;
		IsCustomerUnit: boolean;
	}
	interface UnitInformationQuery extends CashTrack.Common.Request {
		UnitID: number;
		AreaID: number;
	}
	interface UserLevelByCodeQuery {
		Code: string;
	}
	interface UserLevelQuery {
	}
	interface VehicleByIDQuery {
		ID: number;
	}
	interface MasterStopsByIdQuery {
		IDs: number[];
	}
	interface RoutesByFilterQuery extends CashTrack.Common.Request {
		AreaID: number;
		OnlyWithoutTanker: boolean;
		RouteNumber: string[];
		DayTime: CashTrack.Consts.TimeOfTheDayEnum;
		RouteCategories: CashTrack.Consts.RouteCategoryEnum[];
		DayCodes: CashTrack.Consts.DayOfWeekEnum[];
		StartDate: Date;
		EndDate: Date;
	}
	interface VehicleByRouteIDQuery {
		RouteID: number;
	}
	interface UnitByAreaListQuery {
		AreaIDs: number[];
	}
	interface MasterRouteVersionQuery extends CashTrack.Common.Request {
		EffectiveDateTime: Date;
		EndDateTime: Date;
		MasterRouteVersionID: number;
		AreaId: number;
		MasterRouteID: number;
	}
	interface AreaByRouteIDQuery {
		RouteID: number;
	}
	interface AreaQuery {
		ID: number;
		Date?: Date;
	}
	interface MasterRouteByFilterQuery extends CashTrack.Common.SearchTableQuery {
		AreaID: number;
		RouteNumbers: string[];
		Zips: string[];
		Address: string;
		WeekDate: Date;
		TimeOfDays: string[];
		RouteTypes: string[];
		City: string;
		IsSimulation: boolean;
		IncludeInactive: boolean;
		IncludeSuspended: boolean;
		RouteCategories: CashTrack.Consts.RouteCategoryEnum[];
		DayCodes: CashTrack.Consts.DayOfWeekEnum[];
	}
	interface MasterStopByIdQuery {
		ID: number;
	}
	interface MasterRouteDetailByIdQuery extends CashTrack.Common.Request {
		ID: number;
	}
	interface MasterRouteByIDQuery extends CashTrack.Common.Request {
		ID: number;
	}
	interface MasterRouteDetailsQuery extends CashTrack.Common.Request {
		MasterRouteId: number;
		EffectiveDateTime: Date;
		AreaId: number;
		IsRouteAM: boolean;
		IsSimulated: boolean;
		IsSpecial: boolean;
	}
	interface NotConfirmedAreasByNextAuthorizedDateQuery extends CashTrack.Common.Request {
		NextDate: Date;
	}
	interface RouteByOnDemandIdQuery {
		OnDemandID: number;
	}
	interface ItemsByStopIDsQuery extends CashTrack.Common.SearchTableQuery {
		StopIDs: number[];
	}
	interface StopByIDQuery {
		ID: number;
	}
	interface UnitByBarcodeOrNameQuery extends CashTrack.Common.SearchTableQuery {
		SearchText: string;
		IsActive: boolean;
	}
	interface UnitByIDQuery extends CashTrack.Common.Request {
		ID: number;
	}
	interface ZipCodeCityByAreaQuery {
		ZipCodeSearchText: string;
		CitySearchText: string;
		AreaID: number;
	}
	interface UnitByAreaIDQuery {
		AreaID: number;
		RequestDateTime: Date;
	}
	interface CommodityByIDQuery {
		ID: number;
		Code: string;
	}
	interface MasterRouteByAreaQuery extends CashTrack.Common.RouteBaseQuery {
		AreaID: number;
		IncludeSourceRoute: boolean;
		IsInternalRoute?: boolean;
		ExcludeCVSRoute?: boolean;
	}
	interface ShipperDestinationByIDQuery {
		AreaID: number;
		CommodityID: number;
		ShipperID: number;
		DestinationID: number;
	}
	interface CommodityByAreaQuery {
		AreaID: number;
		CommodityID: number;
		IsActive: boolean;
		IsCheckInAllowed: boolean;
		IsCommodityScanned: boolean;
		IsECashSource?: boolean;
	}
	interface ItemByIDQuery extends CashTrack.Common.Request {
		ID: number;
	}
	interface ShipperDestinationQuery extends CashTrack.Common.Request {
		AreaID: number;
		CommodityID: number;
		ShipperID: number;
	}
	interface UnitByBarcodeQuery extends CashTrack.Common.Request {
		Barcode: string;
		IsActive?: boolean;
		UnitTypeCode?: number;
	}
	interface StopByRouteIdQuery {
		RouteID: number;
	}
	interface VaultDoorByIDQuery {
		ID: number;
	}
	interface ItemByRouteQuery {
		AreaID: number;
		RouteID: number;
		IncludeScannedCommodity: boolean;
	}
	interface MissingItemQuery extends CashTrack.Common.SearchTableQuery {
		From: Date;
		To: Date;
		AreaId: number;
		Routes: string[];
		TransactionType: string;
		RouteNumber: string;
		Area: string;
		Truck: string;
		ItemId: string;
		Shipper: string;
		Commodity: string;
		Destination: string;
		EmployeeId: string;
		IncludeRetrievedItems: boolean;
		IncludeDefinitelyMissingItems: boolean;
		UnitId: number;
	}
	interface AreaCommodityQuery {
		AreaID: number;
		CommodityID: number;
		CommodityCode: string;
	}
	interface MasterRouteByRouteNumberQuery {
		RouteNumber: string;
	}
	interface RouteByIdQuery {
		ID: number;
	}
	interface RouteByUnitQuery {
		UnitID: number;
	}
	interface RouteEmployeeByRouteQuery {
		RouteID: number;
	}
	interface ItemByBarcodeQuery {
		Barcode: string;
		Status: CashTrack.Consts.ItemStatusEnum;
	}
	interface RouteByRouteNumberQuery extends CashTrack.Common.RouteBaseQuery {
		RouteNumber: string;
		Date?: Date;
	}
	interface RouteByVehicleQuery extends CashTrack.Common.RouteBaseQuery {
		VehicleID: number;
		IsServiceByTruck: boolean;
		IsInternalRoute: boolean;
	}
	interface RouteVehicleQuery {
		RouteID?: number;
		VehicleID?: number;
	}
	interface StopByUnitIDQuery {
		RouteID: number;
		UnitID: number;
	}
	interface TankerByBarcodeQuery {
		Barcode: string;
	}
	interface UnitQuery extends CashTrack.Common.Request {
		SearchText: string;
		IsActive?: boolean;
		AreaID?: number;
		includeNonBillable?: boolean;
		Force?: boolean;
		ATMOnly?: boolean;
	}
	interface DateRangeQuery {
		PeriodStart: Date;
		PeriodEnd: Date;
	}
	interface EmployeeByBarcodeQuery {
		Barcode: string;
	}
	interface VaultDoorByBarcodeQuery {
		Barcode: string;
	}
	interface VehicleByBarcodeQuery {
		Barcode: string;
	}
	interface CancelManifestedItemsCommand {
		ItemIDs: number[];
	}
	interface CancelRelatedStopOnMasterRouteCommand extends CashTrack.Common.Request {
		StopID: number;
		RelatedStops: CashTrack.Common.RelatedStopResult[];
		LastUpdatedBy: number;
		ConfirmedErrors: CashTrack.BusinessRuleValidationResult[];
	}
	interface CreateMasterStopHolidayBulkCommand extends CashTrack.Common.Request {
		MasterRouteDetailID: number;
		InternalSequenceNumber: number;
		ServiceDateBegunOn: Date;
		ServiceDateEndedOn: Date;
		AreaHolidayDateID: number;
		Stops: number[];
	}
	interface CreateMasterStopNCommand extends CashTrack.Common.Request {
		AreaHolidayDateID: number;
		ShuttleMasterStopID: number;
		AreaID: number;
		InternalMasterRouteID: number;
		DefaultTankerID: number;
		RouteCategoryCode: string;
		ServiceDayCode: string;
		MasterRouteVersionID: number;
		DayNumber: number;
		Duration: number;
		DayDate: Date;
		EndDate: Date;
		InternalStop: CashTrack.Common.CreateInternalStopCommand;
	}
	interface CreateInternalStopCommand {
		UnitID: number;
		StopTypeCode: string;
		IsDepositStop: boolean;
		IsShipmentStop: boolean;
		EndDate: Date;
	}
	interface MergeItemShipmentECashCommand {
		ItemID: number;
		FromRouteID: number;
		ToRouteID: number;
		LastTransactionID: number;
	}
	interface InternalStopNInfoQuery {
		DestinationAreaUnitID: number;
		AreaUnitIDs: number[];
		EffectiveDate: Date;
	}
	interface InternalStopNInfoCommand extends CashTrack.Common.Request {
		AreaUnitID: number;
		MasterStops: CashTrack.Common.MasterStopResult[];
		ServiceDayCode: string;
		IsDepositStop: boolean;
		IsShipmentStop: boolean;
		ShuttleMasterStopID: number;
	}
	interface CreateInternalRouteCommand extends CashTrack.Common.Request {
		ID: number;
		ShuttleMasterStopID: number;
		ShuttleMasterRouteID: number;
		InternalStops: CashTrack.Common.CreateInternalStopCommand[];
		AreaHolidayDateID: number;
		AreaID: number;
		AreaUnitID: number;
		RouteTypeCode: string;
		AMorPMCode: string;
		RouteNumber: string;
		Name: string;
		IsHHTRoute: boolean;
		NumberOfCrew: number;
		IsHHTPrinterRequired: boolean;
		IsSpecialRoute: boolean;
		IsOverDaysRoute: boolean;
		ODRDaysQty: number;
		IsTransferIn: boolean;
		IsTransferOut: boolean;
		TankerName: string;
		RouteCategoryCode: string;
		ServiceDayCode: string;
		IsDepositStop: boolean;
		IsShipmentStop: boolean;
		DayDate: Date;
		EndDate: Date;
	}
	interface CreateInternalStopInfoCommand {
		ShuttleMasterRouteID: number;
		StopTypeCode: string;
		AreaID: number;
		AreaUnitID: number;
		AreaName: string;
		RouteTypeCode: string;
		AMorPMCode: string;
		RouteNumber: string;
		Name: string;
		ShuttleMasterStopID: number;
		RouteCategoryCode: string;
		IsDepositStop: boolean;
		IsShipmentStop: boolean;
		EndDate: Date;
	}
	interface UpdateBankSourceTypeDeliveryAreaListCommand {
		ID: number;
		DeliveryAreaIDs: number[];
	}
	interface UpdateInternalMasterStopCommand extends CashTrack.Common.Request {
		ShuttleOriginUnitAreaID: number;
		AreaUnitID: number;
		IsDepositStop: boolean;
		IsShipmentStop: boolean;
		ShuttleMasterStopID: number;
		DayDate: Date;
		EndDate: Date;
		AreaStops: CashTrack.Route.StopInformationResult[];
	}
	interface CreateInternalRoutesCommand extends CashTrack.Common.Request {
		InternalRoutes: CashTrack.Common.CreateInternalStopInfoCommand[];
		AreaStops: CashTrack.Route.StopInformationResult[];
		ShuttleOriginUnitAreaID: number;
		ShuttleAreaID: number;
		ServiceDayCode: string;
		ShuttleRouteCategoryCode: string;
		DayDate: Date;
	}
	interface CreateInternalMasterRouteCommand extends CashTrack.Common.Request {
		ID: number;
		ShuttleMasterStopID: number;
		ShuttleMasterRouteID: number;
		InternalStops: CashTrack.Common.CreateInternalStopCommand[];
		AreaID: number;
		AreaUnitID: number;
		RouteTypeCode: string;
		AMorPMCode: string;
		RouteNumber: string;
		Name: string;
		IsHHTRoute: boolean;
		NumberOfCrew: number;
		IsHHTPrinterRequired: boolean;
		IsSpecialRoute: boolean;
		IsOverDaysRoute: boolean;
		ODRDaysQty: number;
		IsTransferIn: boolean;
		IsTransferOut: boolean;
		TankerName: string;
		RouteCategoryCode: string;
		ServiceDayCode: string;
		IsDepositStop: boolean;
		IsShipmentStop: boolean;
	}
	interface CreateSiteDeliveryAreaCommand extends CashTrack.Common.Request {
		SourceAreaID: number;
		DeliveryAreaID: number;
		UnitID: number;
		SiteDeliveryServiceDay: string[];
		EffectiveDate: Date;
		EndDate: Date;
	}
	interface UpdateSiteDeliveryAreaCommand extends CashTrack.Common.Request {
		ID: number;
		SourceAreaID: number;
		DeliveryAreaID: number;
		UnitID: number;
		SiteDeliveryServiceDay: string[];
		EffectiveDate: Date;
		EndDate: Date;
	}
	interface UpdateODRRouteDataCommand {
		MasterRouteID: number;
		MasterRouteDetailID: number;
		DayNumber: number;
		ODRDaysQty: number;
	}
	interface CreateCheckOutFromCoinTransactionCommand extends CashTrack.Common.CreateTransactionCommandBase {
		From: number;
		To: number;
		StopID: number;
		ToID: number;
	}
	interface CancelManifestItemShipmentCommand {
		ItemID: number;
		ItemShipmentRouteID: number;
		ItemStatusCode: string;
		RouteID: number;
		ShipperID: number;
		CurrentTankerID: number;
		TankerTypeID: number;
		PreparedOn: Date;
		SourceMasterRouteID: number;
		Comment: string;
		CurrentDateTime: Date;
		EmployeeID: number;
	}
	interface JDECreateStopCommand {
		ID: number;
		RouteID: number;
		MasterStopID: number;
		IsManual: boolean;
		IsCancelled: boolean;
		CustomerEndedOn: Date;
		CustomerStartedOn: Date;
		EndedOn: Date;
		StartedOn: Date;
		SequenceNumber: number;
		InternalSequenceNumber: number;
		CreatedBy?: number;
		LastUpdatedBy?: number;
		CreatedOn?: Date;
		LastUpdatedOn?: Date;
		IsBillable: boolean;
	}
	interface CancelItemShipmentCommand {
		ItemID: number;
	}
	interface NewItemCommand {
		Barcode: string;
		ShipperID: number;
		CommodityID: number;
		DestinationID: number;
		Amount: number;
		Quantity: number;
		RouteID: number;
		ItemStatusCode: string;
		CurrentTankerID: number;
		Comments: string;
		EmployeeID: number;
		CurrentDateTime: Date;
		AreaID: number;
		RouteDate: Date;
		AreaDestinationID: number;
		DeliveryRouteID: number;
	}
	interface CreateCustomerShipmentCommand {
		DestinationUnitID: number;
		PreparedOn: Date;
		InitialDeliveryDate: Date;
		DeliveredOn: Date;
		IsCODPayment: boolean;
		ShipmentSourceID: number;
	}
	interface CreateDatedBankSourceTypeCommand {
		CreatedBy: number;
		CreatedOn: Date;
		InputDateTime: Date;
		InputFileName: string;
		LastUpdatedBy: number;
		LastUpdatedOn: Date;
		BankSourceTypeID: number;
		RowVersion: number[];
		ShipmentSourceID: number;
		PreparedOn: Date;
		DefaultDeliveryOn: Date;
	}
	interface CreateCoinShipmentCommand {
		BarCode: string;
	}
	interface DeleteEquipmentCommand {
		UnitID: number;
		EquipmentTypeCode: string;
	}
	interface DeleteItemShipmentCommand {
	}
	interface DeleteRouteEmployeeCommand {
		RouteID: number;
		EmployeeID: number;
	}
	interface ManifestDestinationCommand {
		ShipperID: number;
		CommodityID: number;
		Barcode: string;
		Name: string;
		Address: string;
		CityState: string;
		DeliveryDate: Date;
		DestinationShipmentCoinDenomination: CashTrack.Common.DestinationShipmentCoinDenominationResult[];
		DatedBankSourceTypeID: number;
		RouteNumber: string;
	}
	interface CreateOrUpdateCommodityAreaCommand {
		AreaID: number;
		CommodityID: number;
		DefaultDestination: CashTrack.Common.UnitResult;
		IsRouted: boolean;
		IsActive: boolean;
		IsAddModeActive: boolean;
		ConfirmedErrors: CashTrack.BusinessRuleValidationResult[];
	}
	interface CreateOrUpdateHolidayAreaCommand {
		AreaID: number;
		AreaHolidayDateID: number;
		HolidayCode: string;
		AreaHolidayDate: Date;
		IsHolidayAM: boolean;
		IsHolidayPM: boolean;
		ConfirmedErrors: CashTrack.BusinessRuleValidationResult[];
	}
	interface CreateOrUpdateSourceRouteAreaCommand {
		AreaID: number;
		MasterRouteID: number;
		MasterRouteVersionID: number;
		RouteTypeCode: string;
		RouteNumber: string;
		Name: string;
		AMorPMCode: string;
		NumberOfCrew: number;
		IsHHTPrinterRequired: boolean;
		LastGeneratedDailyRouteDateOn: Date;
		IsSpecialRoute: boolean;
		EffectiveDateOn: Date;
		EndDateOn: Date;
		IsSimulationRouteVersion: boolean;
		ConfirmedErrors: CashTrack.BusinessRuleValidationResult[];
	}
	interface CreateOrUpdateVaultdoorAreaCommand {
		AreaID: number;
		VaultDoorID: number;
		Barcode: string;
		Description: string;
		IsActive: boolean;
		IsSameDayDoor: boolean;
		ConfirmedErrors: CashTrack.BusinessRuleValidationResult[];
	}
	interface CreateVehicleEquipmentCommand {
		VehicleID: number;
		Barcode: string;
		EquipmentTypeCode: string;
	}
	interface DeleteAreaHolidayDateCommand {
		AreaHolidayID: number;
	}
	interface DeleteAreaCommodityCommand {
		AreaID: number;
		CommodityID: number;
	}
	interface DeleteAreaSourceRouteCommand {
		MasterRouteID: number;
		ConfirmedErrors: CashTrack.BusinessRuleValidationResult[];
	}
	interface MergeStopCommandWithOverwriteNullDate {
		MergeStopCommands: CashTrack.Common.MergeStopCommand[];
		OverwriteNullDate: boolean;
	}
	interface MergeStopCommand {
		ID: number;
		RouteID: number;
		UnitBarcode: string;
		UnitID: number;
		DeliveryManifest: string;
		DeliveryDate: string;
		PickupManifest: string;
		PickupDate: string;
		CustomerEndedOn: Date;
		CustomerStartedOn: Date;
		EndedOn: Date;
		StartedOn: Date;
	}
	interface OpenBagoutByIDCommand extends CashTrack.Common.Request {
		ID: number;
		AreaID?: number;
		AreaToID?: number;
		TankerID?: number;
		RouteID?: number;
		CreatedBy?: number;
		CheckInMode?: CashTrack.Consts.TankerCheckInMode;
		Bagout?: CashTrack.ManageItems.BagOutDetailsResult;
	}
	interface OverrideUserTypeCommand {
		UserID: string;
		PIN: string;
		RequestDateTime: Date;
		OverrideTypeCode: string;
	}
	interface StopsForPeriodQuery {
		MasterStopID: number;
		ServiceDateBegunOn: Date;
		EndedTemporaryBegunOn: Date;
		EndedTemporaryEndedOn: Date;
		ServiceDateEndedOn: Date;
	}
	interface UpdateAreaMaintenanceCommand {
		AreaID: number;
		MaxRouteMileage: number;
		IsManagingBranchTanker: boolean;
		IsPieceCountToGardaCVSAllowed: boolean;
		IsManagingOverride: boolean;
		CutOffAreaTimeAsDate: Date;
		CutOffAreaTime: System.TimeSpan;
	}
	interface UpdateBankSourceTypeUseCustomListCommand {
		ID: number;
		UseCustomList: boolean;
	}
	interface VerifyMasterStopStopCommand extends CashTrack.Common.Request {
		MasterRouteDetailID: number;
		UnitID: number;
		PlannedDate: Date;
		ServiceDateBegunOn: Date;
		EndedTemporaryBegunOn: Date;
		EndedTemporaryEndedOn: Date;
		ServiceDateEndedOn: Date;
	}
	interface UpdateStopAndMasterStopCommand {
		Id: number;
		SequnceNumber: number;
		InternalSequnceNumber: number;
	}
	interface UpdateManageAreaCommand {
		AreaID: number;
		MaxRouteMileage: number;
		IsManagingBranchTanker: boolean;
		IsPieceCountToGardaCVSAllowed: boolean;
	}
	interface CreateOrUpdateTankerCommand {
		ID: number;
		AreaID: number;
		Barcode: string;
		Name: string;
		TankerTypeID: number;
		IsActive: boolean;
	}
	interface ScanRouteKeyCommand {
		EmployeeID: number;
		PlannedDate: Date;
		MasterRouteID: number;
		RouteNumber: string;
		EquipmentBarcode: string;
		EquipmentTypeCode: string;
		CreateDate: Date;
	}
	interface SourceDestinationUnitsCommand {
		BankSourceTypeID: number;
		DestinationUnitID: number;
		SundayCode: boolean;
		MondayCode: boolean;
		TuesdayCode: boolean;
		WednesdayCode: boolean;
		ThursdayCode: boolean;
		FridayCode: boolean;
		SaturdayCode: boolean;
		FilteredDayOfService: string;
	}
	interface UpdateMissingItemReasonAndCommentCommand extends CashTrack.Common.Request {
		ID: number;
		LastTransactionID: number;
		LastTransactionRouteID: number;
		MissingItemReasonCode: string;
		Comment: string;
		CommodityCode: string;
		ShippableItemID: number;
		DestinationID: number;
		ClosedOn?: Date;
		ClosedBy?: number;
		ItemStatusCode: string;
	}
	interface UpdateItemCommand {
		ID: number;
		VehicleID: number;
		TankerID: number;
		IsCancelled: boolean;
		ItemStatus: CashTrack.Consts.ItemStatusEnum;
	}
	interface CreateDatedBankSourceTypeForManifestCommand extends CashTrack.Common.CreateDatedBankSourceTypeCommand {
		MasterRouteID: number;
	}
	interface CreateItemManifestCommand {
		Barcode: string;
		CommodityID: number;
		DestinationID: number;
		ShipperID: number;
		Amount: number;
		Quantity: number;
		ItemStatusCode: string;
		BarcodePrefix: string;
		GroupID: number;
		CoinDenominationID: number;
		CustomerShipmentID: number;
		DatedBankSourceTypeID: number;
		InputManifestSequenceNumber: number;
		CoinShipmentID: number;
		PreparedOn: Date;
		DeliveryDate: Date;
		AreaID: number;
		BagSeal?: string;
		ExpirationDate?: Date;
		CreatedOn?: Date;
		CreatedBy?: number;
	}
	interface DeleteItemCommand {
		RouteID: number;
	}
	interface UpdateItemManifestCommand {
		Amount: number;
		Quantity: number;
	}
	interface UpdateCoinShipmentCommand {
		ID: number;
		BarCode: string;
	}
	interface UpdateMissedStopUnitCommand {
		StopID: number;
		RescheduledStopID: number;
	}
	interface UpdateStopServiceCommand {
		ServiceIDs: number[];
		StopID: number;
		MasterStopID: number;
	}
	interface CreateCheckinFromBankTransactionCommand extends CashTrack.Common.CreateTransactionCommandBase {
		ToTankerID: number;
		VaultDoorID: number;
		ItemTransactionTypeCode: string;
	}
	interface CreateOrUpdateNonRevenueServiceDayCommand {
		NonRevenueUnitID: number;
		ServiceDayCode: string[];
	}
	interface CreateEquipmentCommand {
		UnitID: number;
		Barcode: string;
		EquipmentTypeCode: string;
	}
	interface CreateCITUnitCommand extends CashTrack.Common.Request {
		Barcode: string;
		Name: string;
		UnitTypeCode: string;
		IsActive: boolean;
		SiteLocationID: number;
		CityID: number;
		Address1: string;
		Address2: string;
		Address3: string;
		ZipPostalCode: string;
		IsMixedCommodityAllowed?: boolean;
	}
	interface CreateNonRevenueUnitCommand extends CashTrack.Common.Request {
		AreaID: number;
		UnitID: number;
		EffectiveDate: Date;
		EndDate: Date;
		IsActive: boolean;
		CITServiceID: number;
		NonRevenueServiceDays: string[];
		activateUnitId: number;
	}
	interface DeleteNonRevenueUnitCommand {
		IDs: number[];
	}
	interface UpdateCITUnitOperationCommand extends CashTrack.Common.Request {
		ID: number;
		UnitTypeCode: string;
		AreaID: number;
		OldAreaID: number;
		IsMixedCommodityAllowed: boolean;
		TimeWindowStartedOn: Date;
		TimeWindowEndedOn: Date;
		ExceptTimeWindowStartedOn: Date;
		ExceptTimeWindowEndedOn: Date;
		MissedStopRequiredActionID: number;
		Latitude: number;
		Longitude: number;
		OutsourcedReferenceNumber: string;
		CVSAreaID: number;
		CVSBankID: number;
		MilesFromArea: number;
		CassetteQuantity: number;
		ContactName: string;
		ContactEmail: string;
		ContactPhoneNumber: string;
		AlternateDestinationID: number;
	}
	interface UpdateBankSourceTypeCommand {
		ID: number;
		AreaID: number;
		Name: string;
		LongDescription: string;
		SourceTypeID: number;
		ShipperUnitID: number;
		ShortDescription: string;
		ReportDescription: string;
		SourceMasterRouteID: number;
		EffectiveDate: Date;
		EndDate: Date;
		DefaultCommodityID: number;
		IsCodPayment: boolean;
		IsBankSourcePrintedOnPod: boolean;
		CoinPackageSizeID: number;
		IsCoinInputByAmount: boolean;
		IsECashSource: boolean;
		OddCoinID: number;
		ECashExpirationDays: number;
	}
	interface UpdateEquipmentCommand {
		UnitID: number;
		Barcode: string;
		EquipmentTypeCode: string;
	}
	interface CreateEmployeeCommand {
		Barcode: string;
		FirstName: string;
		MiddleInitial: string;
		LastName: string;
		IsActive: boolean;
		UserLevelCode: string;
		AreaID: number;
		CreatedOn: Date;
	}
	interface CancelStopOnMasterRouteCommand extends CashTrack.Common.Request {
		StopID: number;
		LastUpdatedBy: number;
		ConfirmedErrors: CashTrack.BusinessRuleValidationResult[];
	}
	interface CreateMasterRouteDetailCommand extends CashTrack.Common.MasterRouteDetailCommand {
		SundayDate: Date;
	}
	interface MasterRouteDetailCommand {
		ID: number;
		MasterRouteVersionID: number;
		ServiceDayCode: string;
		CutoffRouteTimeOn: System.TimeSpan;
		BeginRouteTimeOn: System.TimeSpan;
		EndRouteTimeOn: System.TimeSpan;
		DefaultTankerID: number;
		AreaHolidayDateID: number;
		RouteCategoryCode: string;
		IsActive: boolean;
		FirstStopRecordID: number;
		LastStopRecordID: number;
		CreatedBy: number;
		CreatedOn: Date;
		LastUpdatedBy: number;
		LastUpdatedOn: Date;
		SuspendedBegunOn: Date;
		SuspendedEndedOn: Date;
		Duration: number;
		MasterRouteID: number;
		CutoffRouteTimeOnAsDate: Date;
		BeginRouteTimeOnAsDate: Date;
		EndRouteTimeOnAsDate: Date;
		Date: Date;
		OldDefaultTankerID: number;
		DayNumber: number;
		IsSuspended: boolean;
		IsCanSuspend: boolean;
	}
	interface AddAutomaticOnDemandCommand extends CashTrack.Common.Request {
		StopID: number;
		MasterStopID: number;
		ContractLineIDs: number[];
		Source: Garda.CashTrackNG.Core.Tooling.AutomaticOnDemandSourceType;
	}
	interface UpdateCITUnitCommand extends CashTrack.Common.Request {
		ID: number;
		Barcode: string;
		Name: string;
		IsActive: boolean;
		UnitTypeCode: string;
		SiteLocationID: number;
		CityID: number;
		Address1: string;
		Address2: string;
		Address3: string;
		ZipPostalCode: string;
		CassetteQuantity?: number;
		IsMixedCommodityAllowed?: boolean;
	}
	interface UpdateNonRevenueUnitCommand extends CashTrack.Common.Request {
		ID: number;
		AreaID: number;
		UnitID: number;
		EffectiveDate: Date;
		EndDate: Date;
		IsActive: boolean;
		CITServiceID: number;
		NonRevenueServiceDays: string[];
		activateUnitId: number;
	}
	interface UpdateVehicleRouteCommand {
		RouteID: number;
		VehicleID: number;
		EndMileage: number;
		StartMileage: number;
	}
	interface UpdateMasterStopCommand extends CashTrack.Common.Request {
		ID: number;
		IsDepositStop: boolean;
		IsShipmentStop: boolean;
		IsDeliveryStop: boolean;
		ServiceDateBegunOn: Date;
		ServiceDateEndedOn: Date;
		EndedTemporaryBegunOn: Date;
		EndedTemporaryEndedOn: Date;
		IsKeyRequired: boolean;
		IsDeliveredOnSameRoute: boolean;
		IsBilled: boolean;
		CreatedBy: number;
		CreatedOn: Date;
		LastUpdatedBy: number;
		LastUpdatedOn: Date;
		OperationComment: string;
		SelectedDate: Date;
		RelatedMasterStopID: number;
		StopTypeCode: string;
	}
	interface UpdateRoutesTankersCommand extends CashTrack.Common.Request {
		IsUpdateMasterRoute: boolean;
		AssignedTankers: CashTrack.Common.AssignedTankerToRoute[];
	}
	interface AssignedTankerToRoute {
		RouteID: number;
		MasterRouteDetailID: number;
		TankerID: number;
		PlannedDate: Date;
	}
	interface CreateVehicleCommand {
		Barcode: string;
		Make: string;
		Model: string;
		Year: number;
		VehicleIdentificationNumber: string;
		InitialMileage: number;
		NumberOfKeys: number;
		IsActive: boolean;
		IsUnitOfMeasureByMile: boolean;
		Keys: string[];
	}
	interface UpdateEmployeeCommand {
		ID: number;
		Barcode: string;
		FirstName: string;
		MiddleInitial: string;
		LastName: string;
		IsActive: boolean;
		UserLevelCode: string;
		AreaID: number;
		LastUpdatedOn: Date;
	}
	interface MasterRouteVersionCommand extends CashTrack.Common.Request {
		ID: number;
		MasterRouteID: number;
		AreaId: number;
		EffectiveDateOn: Date;
		EndDateOn: Date;
		IsSimulationRouteVersion: boolean;
		MasterRouteDetails: CashTrack.Common.MasterRouteDetailCommand[];
	}
	interface RemoveStopsCommand {
		MasterStopIDs: number[];
		Date: Date;
	}
	interface UpdateVehicleCommand {
		ID: number;
		Barcode: string;
		Make: string;
		Model: string;
		Year: number;
		VehicleIdentificationNumber: string;
		NumberOfKeys: number;
		IsActive: boolean;
		IsUnitOfMeasureByMile: boolean;
		InitialMileage: number;
		Keys: string[];
	}
	interface CreateMasterRouteCommand extends CashTrack.Common.Request {
		ID: number;
		AreaID: number;
		RouteTypeCode: string;
		AMorPMCode: string;
		RouteNumber: string;
		Name: string;
		IsHHTRoute: boolean;
		NumberOfCrew: number;
		IsHHTPrinterRequired: boolean;
		IsSpecialRoute: boolean;
		IsOverDaysRoute: boolean;
		ODRDaysQty: number;
		IsTransferIn: boolean;
		IsTransferOut: boolean;
		IsDefaultSource: boolean;
	}
	interface UpdateShipperDestinationCommand extends CashTrack.Common.Request {
		ShipperID: number;
		DestinationID: number;
		CommodityID: number;
		LastUpdatedBy: number;
		LastUpdatedOn: Date;
		EffectiveDate: Date;
		EndDate: Date;
		IsSameDay: boolean;
	}
	interface CreateMasterStopCommand extends CashTrack.Common.Request {
		ID: number;
		MasterRouteDetailID: number;
		UnitID: number;
		PreviousMasterStopID: number;
		NextMasterStopID: number;
		StopTypeCode: string;
		InternalSequenceNumber: number;
		IsDepositStop: boolean;
		IsShipmentStop: boolean;
		IsDeliveryStop: boolean;
		ServiceDateBegunOn: Date;
		ServiceDateEndedOn: Date;
		EndedTemporaryBegunOn: Date;
		EndedTemporaryEndedOn: Date;
		IsKeyRequired: boolean;
		IsDeliveredOnSameRoute: boolean;
		IsBilled: boolean;
		CreatedBy: number;
		CreatedOn: Date;
		LastUpdatedBy: number;
		LastUpdatedOn: Date;
		OperationComment: string;
		AreaHolidayDateID: number;
		RelatedMasterStopID: number;
	}
	interface DeleteStopCommand {
		StopID: number;
	}
	interface AddMissedStopCommand extends CashTrack.Common.Request {
		StopID: number;
		MissedStopReasonCode: string;
		Comments: string;
	}
	interface ChangeItemDestinationCommand {
		ItemID: number;
		DestinationID: number;
	}
	interface ChangeItemCommand {
		ID: number;
		CommodityID: number;
		DestinationID: number;
		ShipperID: number;
		Amount: number;
		Quantity: number;
		ScannedCode: CashTrack.Consts.ItemShipmentScannedCode;
		IsReturnedToOrigin: boolean;
	}
	interface ChangeStopCommand {
		ID: number;
		StartedOn: Date;
		EndedOn: Date;
		IsManual: boolean;
		SequenceNumber?: number;
	}
	interface CreateStopCommand {
		ID: number;
		RouteID: number;
		UnitID: number;
		MasterStopID: number;
		IsManual: boolean;
		IsCancelled: boolean;
		CustomerEndedOn: Date;
		CustomerStartedOn: Date;
		EndedOn: Date;
		StartedOn: Date;
		SequenceNumber: number;
		InternalSequenceNumber: number;
		CreatedBy?: number;
		LastUpdatedBy?: number;
		CreatedOn?: Date;
		LastUpdatedOn?: Date;
		IsBillable: boolean;
	}
	interface MergeItemShipmentCommand {
		RouteID: number;
		ItemID: number;
		IsCheckedIn: boolean;
		LastTransactionID: number;
		IsTransferredToOtherArea: boolean;
		ItemShipmentStatus: CashTrack.Consts.ItemShipmentStatusEnum;
		AreaDestinationID: number;
		ScannedCode: CashTrack.Consts.ItemShipmentScannedCode;
	}
	interface CreateCheckInTruckTransactionCommand extends CashTrack.Common.CreateTransactionCommandBase {
		TankerID: number;
		VaultDoorID: number;
		ToID: number;
	}
	interface CreateDeliveryTransactionCommand extends CashTrack.Common.CreateTransactionCommandBase {
		StopID: number;
		VaultDoorID: number;
		ToID: number;
	}
	interface CreateLoadTruckTransactionCommand extends CashTrack.Common.CreateTransactionCommandBase {
		ToID: number;
	}
	interface CreatePickUpTransactionCommand extends CashTrack.Common.CreateTransactionCommandBase {
		StopID: number;
		ToID: number;
		IsVerified: boolean;
	}
	interface MergeEquipmentToRouteCommand {
		RouteID: number;
		EquipmentBarcode: string;
		EquipmentTypeCode: string;
	}
	interface MergeVehicleToRouteCommand {
		RouteID: number;
		VehicleBarcode: string;
		EndMileage: number;
		StartMileage: number;
	}
	interface AddShipperDestinationCommand extends CashTrack.Common.Request {
		AreaID: number;
		CommodityID: number;
		ShipperID: number;
		DestinationID: number;
		EffectiveDate: Date;
	}
	interface ReplaceShipperDestinationCommand extends CashTrack.Common.Request {
		AreaID: number;
		CommodityID: number;
		DestinationID: number;
		ShipperID: number;
		EffectiveDate: Date;
	}
	interface ChangeItemBarcodeCommand extends CashTrack.Common.Request {
		ID: number;
		NewBarcode: string;
	}
	interface ChangeRouteStatusCommand {
		RouteID: number;
		RouteStatus: string;
		ArrivedOn: Date;
		DepartedOn: Date;
		LoadStartedOn: Date;
		UnloadStartedOn: Date;
		LoadEndedOn: Date;
		LastUpdatedOn: Date;
		CurrentRouteStatuses: string[];
	}
	interface CloseMissingItemCommand extends CashTrack.Common.Request {
		ItemID: number;
		EmployeeID: number;
		ClosedOn?: Date;
	}
	interface AddVehicleToRouteCommand {
		RouteID: number;
		VehicleID: number;
		EndMileage: number;
		StartMileage: number;
	}
	interface ChangeItemLocationCommand {
		ID: number;
		VehicleID: number;
		TankerID: number;
	}
	interface ChangeItemStatusCommand {
		ID: number;
		ItemStatus: CashTrack.Consts.ItemStatusEnum;
	}
	interface DeclareMissingItemCommand {
		ItemID: number;
		MissingItemType: CashTrack.Consts.MissingItemTypeEnum;
		OpenedDate: Date;
		EmployeeID: number;
		MissingItemReason: CashTrack.Consts.MissingItemReasonEnum;
		Comment: string;
	}
	interface RelationCITServiceExtraResult {
		RelationCITServiceExtraResultsID: number;
		IsExtraServiceFee: boolean;
		IsTransportationFee: boolean;
		AttributeBillingCode: string;
		ConjunctiveServiceCode: string;
		CITServiceID: number;
		ServiceCode: string;
		ServiceDescription: string;
		ShortAttributeCodeID: number;
		ShortAttributeCode: string;
		ShortAttributeDescription: string;
		UnitOfMeasure: string;
		VariableBillingCode: string;
		IsSeasonalService: boolean;
		IsAppliedToConjunctiveService: boolean;
		CITServiceAttributeID: number;
		AttributeID: number;
		ServiceTypeCode: string;
		IsPickupService: boolean;
		IsDeliveryService: boolean;
	}
	interface ClientPortalDomainNameResult {
		DomainName: string;
	}
	interface ContractLineLightLoadResult {
		ID: number;
		ContractID: number;
		UnitID: number;
		ServiceFrequencyCode: string;
		ProductCodeID: number;
		ProductBillingCode: string;
		TriPartyCode: string;
		ContractLineNumber: number;
		EffectiveDate: Date;
		EndDate: Date;
		ServiceChangeStartedOn: Date;
		LiabilityTreshold: number;
		PremiseTimeThreshold: number;
		ItemThreshold: number;
		IsActive: boolean;
		IsNew: boolean;
	}
	interface ContractLineAttributeResult {
		ID: number;
		ContractLineID: number;
		UnitID: number;
		AttributeBillingCode: string;
		ShortAttributeCodeID: number;
		IsActive: boolean;
		LastUpdatedOn: Date;
	}
	interface ContractLineResult {
		ID: number;
		ContractID: number;
		UnitID: number;
		ServiceFrequencyCode: string;
		ProductCodeID: number;
		ProductBillingCode: string;
		TriPartyCode: string;
		ContractLineNumber: number;
		EffectiveDate: Date;
		EndDate: Date;
		LiabilityTreshold: number;
		PremiseTimeThreshold: number;
		ItemThreshold: number;
		IsActive: boolean;
		CreatedBy: number;
		CreatedOn: Date;
		LastUpdatedBy: number;
		LastUpdatedOn: Date;
		ServiceChangeStartedOn: Date;
		Description: string;
		IsCheckLiabilityBilled: boolean;
		SiteLocationID: number;
		Unit: CashTrack.Common.UnitResult;
		ProductCode: Garda.CashTrackNG.Core.Contracts.Models.Common.Results.ProductCodeResult;
		Contract: CashTrack.Common.ContractResult;
		Attributes: CashTrack.Common.ContractLineAttributeResult[];
		ContractLineServiceChanges: Garda.CashTrackNG.Core.Contracts.Models.Common.Results.ContractLineServiceChangeJdeResult[];
		ContractLineNextPlannedServiceDates: Date[];
	}
	interface ContractsByExtranetUserQuery {
		ExtranetUserId: number;
	}
	interface ExtranetUserEmailUniqueQuery {
		Email: string;
	}
	interface ExtranetUserByUsernameQuery {
		Username: string;
	}
	interface ContractByExtranetUserQuery extends CashTrack.Common.SearchTableQuery {
		ExtranetUserId: number;
	}
	interface ContractQuery extends CashTrack.Common.SearchTableQuery {
		ContractId: string;
		ContractNumber: string;
		ContractDescription: string;
		CustomerNumber: string;
		CustomerName: string;
	}
	interface ExtranetUserByIDQuery {
		ExtranetUserId: number;
	}
	interface ExtranetUserNameUniqueQuery {
		Username: string;
	}
	interface ExtranetUserQuery extends CashTrack.Common.SearchTableQuery {
		FirstName: string;
		LastName: string;
		UserName: string;
		IsActive: boolean;
		ContractNumber: string;
		ContractDescription: string;
		CustomerNumber: string;
		CustomerName: string;
	}
}
declare module CashTrack.OperationalFollowUp {
	interface UpdateAreaBillingDateCommand {
		ID: number;
		BillingConfirmedOn: Date;
		BillingAuthorizedOn: Date;
		BillingAuthorizedBy: number;
	}
	interface UpdateOnDemandBillingDashboardCommand {
		CurrentStopID: number;
		StopVariableServiceID: number;
		NonBillableReasonCode: number;
		QuantityToBillAsDate: Date;
	}
	interface SetStopManualyBilledCommand {
		StopID: number;
		ManualyBilled: boolean;
	}
	interface RemoveMissedStopReasonCommand {
		StopID: number;
	}
	interface AddAreaBillingDateCommand {
		AreaID: number;
		BillingConfirmedOn: Date;
		BillingAuthorizedOn: Date;
		BillingAuthorizedBy: number;
	}
	interface ChangeBatchMissedStopCommand extends CashTrack.Common.Request {
		Commands: CashTrack.Common.ChangeMissedStopCommand[];
	}
	interface UpdateStopExtraFeeCommand {
		QuantityToBill: number;
		StopExtraFeeID: number;
		IsExcessBilled: boolean;
	}
	interface CalculateOverrideNumberCommand extends CashTrack.Common.Request {
		InputNumber: number;
		RouteNumber: string;
	}
	interface AddRouteEventCommand extends CashTrack.Common.Request {
		CreatedOn: Date;
		DispatcherBarcode?: string;
		CreatedBy?: number;
		RouteEventTypeNumber: number;
		MessengerID: number;
		UnitID: number;
		RouteID: number;
		Description: string;
		ItemBarcodes: string;
		FirstName: string;
		LastName: string;
	}
	interface BillingDashboardCountWithErrorResult {
		CountWithError: number;
	}
	interface CountBillingDashboardWithErrorResult {
		CountMissedStopWithError: number;
		CountManualStopWithError: number;
		CountOnDemandWithError: number;
		CountNotClosedRoutesWithError: number;
	}
	interface MissedStopsRecapResult {
		PlannedDate: Date;
		RouteNumber: string;
		RouteName: string;
		SequenceNumber: number;
		UnitBarcode: string;
		UnitTypeCode: string;
		UnitName: string;
		Address: string;
		CityName: string;
		CountryCode: string;
		Reason: string;
		ReasonCode: string;
		IsGardaResponsibility: boolean;
		Comments: string;
		StopID: number;
		RescheduledStopID: number;
		RouteStatusCode: string;
		RequiredActionDescription: string;
		NextServiceDate: Date;
		RescheduledServiceStatus: string;
	}
	interface LocationDisplayShipmentToResult {
		ItemBarcode: string;
		OriginalBarcode: string;
		ItemID: number;
		ShipperUnitBarcode: string;
		ShipperUnitName: string;
		Quantity: number;
		Commodity: string;
		Amount: number;
		DeliveryDate: Date;
		RouteNumber: string;
		Source: string;
		SourceDescription: string;
		TransactionItemsList: CashTrack.ClientPortal.PackagesTransactionResult[];
	}
	interface LocationDisplayShipmentFromResult {
		ID: number;
		ItemBarcode: string;
		OriginalBarcode: string;
		ItemID: number;
		ShipperUnitBarcode: string;
		ShipperUnitName: string;
		Quantity: number;
		Commodity: string;
		Amount: number;
		DeliveryDate: Date;
		RouteNumber: string;
		Source: string;
		SourceDescription: string;
		TransactionItemsList: CashTrack.ClientPortal.PackagesTransactionResult[];
	}
	interface RouteAnalysisInfoResult {
		ID: number;
		RouteID: number;
		PlannedDate: Date;
		MasterRouteNumber: string;
		MasterRouteName: string;
		VehiculeBarCode: string;
		VaultBalanceStartDateTime: Date;
		VaultBalanceEndDateTime: Date;
		VaultBalanceItemCount: number;
		Messenger: string;
		Driver: string;
		AsstMessenger: string;
		RouteStatus: string;
		BeginOdom: string;
		EndOdom: string;
		Mileage: number;
		BegTime: Date;
		ArrTime: Date;
		CheckinStart: Date;
		Complete: Date;
		NumberOfStops: number;
		NumberOfAtmStops: number;
		NumberOfBankBranchStops: number;
		NumberOfCommercialStops: number;
		NumberOfProcessCenterStops: number;
		StreetHrs: number;
		RouteHrs: number;
		StopsPerHour: number;
		DriverBarcode: string;
		MessengerBarcode: string;
		AsstMessengerBarcode: string;
		RouteEmployees: CashTrack.Common.RouteEmployeeResult[];
		UnloadEndedOn: Date;
		LoadStartedOn: Date;
		BegBalance: Date;
		Vehicles: CashTrack.Common.RouteVehicleResult[];
	}
	interface RouteResultBillingDashboard {
		RouteNumber: string;
		RouteType: string;
		PlannedDate: Date;
		LoadEndTime: Date;
		UnloadStartTime: Date;
		Checkout: boolean;
		ParcelCheckIn: boolean;
		CoinCheckIn: boolean;
		TransferCheckIn: boolean;
		TotalCount: number;
	}
	interface LocationDisplayRoutingResult {
		StopID: number;
		RouteID: number;
		MasterRouteID: number;
		MasterStopID: number;
		PlannedDate: Date;
		StartedOn: Date;
		StopStatus: string;
		DayOfWeek: string;
		RouteNumber: string;
		RouteName: string;
		SequenceNumber: number;
		IsKeyRequired: boolean;
		MasterStopType: string;
		OperationComment: string;
		StopBillingStatusCode: string;
		RouteStatusCode: string;
		StopBillingStatusDescription: string;
		MissedStopID: number;
		IsGardaResponsibility: boolean;
		UnitID: number;
	}
	interface AreaBillingDateResult {
		ID: number;
		AreaID: number;
		BillingConfirmedOn: Date;
		BillingAuthorizedOn: Date;
		BillingAuthorizedBy: number;
		AuthorizedBy: string;
		Area: CashTrack.Common.AreaResult;
	}
	interface BillingConfirmValidationResult {
		ManualStopWithMissingTimes: boolean;
		MissedStopWithError: boolean;
		RouteWithCoinCheckInNotDone: boolean;
		RouteWithParcelCheckInNotDone: boolean;
		StopOnDemandServiceWithError: boolean;
	}
	interface CountUpdatedStopsResult {
		Count: number;
		Result: boolean;
	}
	interface CountStopWithErrorResult {
		StopWithError: number;
	}
	interface CountRoutesInSelectedDatesBillingDashboardResult {
		RoutesInSelectedDatesBillingDashboard: number;
	}
	interface ItemTransactionBillingDashboardResult {
		ID: number;
		Barcode: string;
		Amount: number;
		Quantity: number;
		Commodity: CashTrack.Common.CommodityResult;
		Shipper: CashTrack.Common.UnitResult;
		Destination: CashTrack.Common.UnitResult;
		ItemTransactionTypeCode: string;
		StopID: number;
	}
	interface OnDemandBillingDashboardResult {
		ID: number;
		ServiceDescription: string;
		CITServiceID: number;
		NonBillableReasonCodeID: number;
		NonBillableReasonCode: string;
		ShortAttributeCodeID: number;
		ShortAttributeCodeDescription: string;
		IsManuallyBilled: boolean;
		IsBillable: boolean;
		IsAllStopServiceReadyForBilling: boolean;
		StopBillingStatus: number;
		QuantityToBill: System.TimeSpan;
		QuantityToBillAsDate: Date;
		StopVariableServiceID: number;
		StopID: number;
		UnitID: number;
		RouteID: number;
		PlannedDate: Date;
		DepartedOn: Date;
		ArrivedOn: Date;
		StartedOn: Date;
		EndedOn: Date;
		RouteNumber: string;
		AreaID: number;
		UnitOfMeasureCode: string;
		CITAttributeServiceID: number;
		IsSelectedInDashboard: boolean;
		PlannedServiceTimeStart: System.TimeSpan;
		PlannedServiceTimeStartAsDate: Date;
		PlannedServiceTimeEnd: System.TimeSpan;
		PlannedServiceTimeEndAsDate: Date;
		Barcode: string;
		UnitName: string;
		SequenceNumber: number;
		InError: boolean;
		Editable: boolean;
		ServicePerHour: boolean;
		Address: string;
		OrderByError: number;
		IsFixedService: boolean;
		TotalCount: number;
	}
	interface RouteEventResult {
		ID: number;
		RouteEventTypeNumber: number;
		Messenger: string;
		Dispatcher: string;
		Unit: string;
		RouteNumber: string;
		CreatedOn: Date;
		Description: string;
		ItemBarcodes: string;
		AreaName: string;
	}
	interface RouteEventTypeResult {
		Number: number;
		Description: string;
	}
	interface CalculateOverrideNumberResult {
		ReturnNumber: number;
	}
	interface BillingDashboardSearchErrorCountQuery {
		MissedAndManualStopsOnly: boolean;
		AreaIDs: number[];
		From: Date;
		To: Date;
		Unconfirmed: boolean;
		Confirmed: boolean;
		Billed: boolean;
		ClosedRoutes: boolean;
		Routes: string[];
		Units: number[];
	}
	interface BillingDashboardGridSearchQuery extends CashTrack.Common.SearchTableQuery {
		AreaIDs: number[];
		From: Date;
		To: Date;
		Unconfirmed: boolean;
		Confirmed: boolean;
		Billed: boolean;
		Error: boolean;
		ClosedRoutes: boolean;
		Routes: string[];
		Units: number[];
	}
	interface MissedStopsRecapQuery {
		AreaID: number;
		MasterRouteID: number;
		UnitID: number;
		FromDate: Date;
		ToDate: Date;
		RequiredActionID: number;
	}
	interface OnDemandBillingDashboardGridSearchQuery extends CashTrack.OperationalFollowUp.BillingDashboardGridSearchQuery {
		PerTrip: boolean;
		PerHour: boolean;
	}
	interface PackagesDisplayFilterQuery extends CashTrack.Common.SearchTableQuery {
		ItemBarcode: string;
		UnitID: string;
		AreaID: string;
		RouteNumber: string;
		JDEContract: string;
		VehicleBarcode: string;
		StartPlannedDate: Date;
		EndPlannedDate: Date;
	}
	interface RouteAnalysisSearchQuery extends CashTrack.Common.SearchTableQuery {
		AreaID: number;
		FromDate: Date;
		ToDate: Date;
		MasterRouteID: number;
	}
	interface PackagesByFilterAndDateQuery extends CashTrack.Common.SearchTableQuery {
		FilterType: string;
		Filter: string;
		StartPlannedDate: Date;
		EndPlannedDate: Date;
	}
	interface ShipmentByUnitIDAndDateRangeQuery extends CashTrack.Common.SearchTableQuery {
		UnitID: number;
		StartDate: Date;
		EndDate: Date;
	}
	interface StopsByFilterAndDateQuery extends CashTrack.Common.SearchTableQuery {
		Filter: string;
		FilterType: string;
		StartPlannedDate: Date;
		EndPlannedDate: Date;
	}
	interface AreaBillingDateQuery {
		AreaID: number;
	}
	interface AvailableUnitsByMultipleAreasQuery {
		AreaIDs: number[];
	}
	interface StopExtraFeeBillingDashboardSearchQuery extends CashTrack.OperationalFollowUp.BillingDashboardSearchQuery {
		VariableBillingCodeExtraFee: CashTrack.Consts.VariableBillingCodeEnum;
	}
	interface BillingDashboardSearchQuery extends CashTrack.Common.SearchTableQuery {
		AreaIDs: number[];
		From: Date;
		To: Date;
		Unconfirmed: boolean;
		Confirmed: boolean;
		Billed: boolean;
		Error: boolean;
		ClosedRoutes: boolean;
		Routes: string[];
		Units: number[];
	}
	interface OnDemandBillingDashboardSearchQuery extends CashTrack.OperationalFollowUp.BillingDashboardSearchQuery {
		PerTripPerHour: CashTrack.Consts.PerTripPerHourEnum;
	}
	interface RouteEventQuery extends CashTrack.Common.SearchTableQuery {
		From: Date;
		To: Date;
		Messenger: string;
		Dispatcher: string;
		Unit: string;
		RouteEventTypeNumber: number;
		RouteNumber: string;
		AreaID?: number;
	}
}
declare module CashTrack.ClientPortal {
	interface PackagesTransactionResult {
		Date: Date;
		ItemID: number;
		TransactionCode: string;
		FromDescription: string;
		ToDescription: string;
		Comments: string;
		EmployeeBarcode: string;
		BagoutNumber: string;
		Quantity: number;
		Commodity: string;
		Value: number;
		StopID: number;
		PickupSignature: string;
		DeliverySignature: string;
		ItemTransactionTypeCode: string;
	}
	interface AddressByUserQuery extends CashTrack.Common.SearchTableQuery {
		Username: string;
		IsActive: boolean;
		Subdivision?: string;
		City?: string;
		Address?: string;
	}
	interface ReceiptQuery {
		StopID: number;
		ReceiptType: string;
	}
	interface StopsReportsByIDsQuery {
		StopIDs: number[];
	}
	interface PackagesReportByStopIDQuery extends CashTrack.Common.SearchTableQuery {
		ID: number;
	}
	interface PackagesTransactionQuery {
		ItemIDs: number[];
		StopIDs: number[];
	}
	interface StopsReportQuery extends CashTrack.Common.SearchTableQuery {
		Username: string;
		SearchType: string;
		FromDate: Date;
		ToDate: Date;
		Subdivision?: string;
		SubdivisionList?: string[];
		City?: string;
		CityList?: number[];
		UnitID?: number;
		IsActive?: boolean;
		IsBillable?: boolean;
	}
	interface PackagesReportMultiSelectQuery extends CashTrack.Common.SearchTableQuery {
		Username: string;
		SearchType: string;
		FromDate: Date;
		ToDate: Date;
		Subdivision?: string;
		City?: string;
		UnitID?: number;
		Barcode?: string;
	}
	interface PackagesReportQuery extends CashTrack.Common.SearchTableQuery {
		Username: string;
		SearchType: string;
		FromDate: Date;
		ToDate: Date;
		SubdivisionList?: string[];
		CityList?: number[];
		UnitID?: number;
		Barcode?: string;
		IsActive?: boolean;
		IsBillable?: boolean;
	}
	interface PackagesReportByUserQuery extends CashTrack.Common.SearchTableQuery {
		Username: string;
	}
	interface ExtranetUserUnitTypeByUsernameQuery extends CashTrack.Common.SearchTableQuery {
		Username: string;
	}
	interface UnitByUserQuery {
		Username: string;
	}
	interface PackagesDisplayItemResult {
		ID: number;
		ItemID: number;
		ItemBarcode: string;
		ItemStatusCode: string;
		Date: Date;
		MasterRouteNumber: string;
		MasterRouteName: string;
		CTNumber: string;
		SiteLocationName: string;
		Address1: string;
		CityName: string;
		SubdivisionCountryCode: string;
		FromDescription: string;
		ToDescription: string;
		Commodity: string;
		TransactionCode: string;
		Value: number;
		Quantity: number;
		EmployeeFullName: string;
		EmployeeFirstName: string;
		EmployeeLastName: string;
		EmployeeBarcode: string;
		IsBagout: boolean;
		BagoutType: string;
		RouteID: number;
		AreaID: number;
		TransactionItemsList: CashTrack.ClientPortal.PackagesDisplayTransactionResult[];
		TotalCount: number;
	}
	interface PackagesDisplayTransactionResult {
		ItemID: number;
		Date: Date;
		TransactionCode: string;
		FromDescription: string;
		ToDescription: string;
		PickupSignature: string;
		DeliverySignature: string;
		Comments: string;
		Commodity: string;
		Quantity: number;
		Value: number;
		EmployeeBarcode: string;
		ItemTransactionTypeCode: string;
		StopID: number;
	}
	interface ClientPortalCityResult {
		CityID: number;
		CityName: string;
		SubdivisionCountryCode: string;
		Username: string;
		CityNameAndSubdivisionCountryCode: string;
	}
	interface ReceiptFooterResult {
		UnitID: number;
		RouteID: number;
		StopID: number;
		Username: string;
	}
	interface ReceiptDetailResult {
		LineNumber: number;
		ItemBarcode: string;
		Quantity: number;
		Amount: number;
		CommodityDescription: string;
		DestinationBarcode: string;
		DestinationName: string;
		ShipperBarcode: string;
		ShipperName: string;
		UnitID: number;
		RouteID: number;
		StopID: number;
		Name: string;
		Address1: string;
		CityID: number;
		ZipPostalCode: string;
		Username: string;
	}
	interface ReceiptHeaderResult {
		CreatedOn: Date;
		CreatedBy: number;
		CreatedByBarcode: string;
		UnitID: number;
		RouteID: number;
		StopID: number;
		RouteNumber: string;
		UnitBarcode: string;
		Name: string;
		Address1: string;
		CityName: string;
		CityID: number;
		Username: string;
		ZipPostalCode: string;
	}
	export interface StopPackagesReportResult {
		PlannedDate: Date;
		StartedOn: Date;
		RouteStatusCode: string;
		RouteID: number;
		MasterRouteName: string;
		AreaID: number;
		RouteNumber: string;
		Barcode: string;
		UnitID: number;
		UnitName: string;
		CityID: number;
		CityName: string;
		StopID: number;
		CustomerStartedOn: Date;
		CustomerEndedOn: Date;
		EndedOn: Date;
		MinInStop: number;
		NumberOfDeliveries: number;
		NumberOfPickups: number;
		StopBillingStatusDescription: string;
		MissedStopReason: string;
		MissedStopID: number;
		Comments: string;
		IsGardaResponsibility: boolean;
		SubdivisionCountryCode: string;
		Address1: string;
		StopStatus: string;
		TransactionCode: string;
		TransactionID: number;
		Value: number;
		Commodity: string;
		Quantity: number;
		ItemID: number;
		ItemBarcode: string;
		TruckID: string;
		EmployeeID: string;
		EmployeeBarcode: string;
		Date: Date;
		CTNumber: string;
		SiteLocationName: string;
		FromDescription: string;
		ToDescription: string;
		TransactionItemsList: CashTrack.ClientPortal.PackagesTransactionResult[];
		UserName: string;
	}
	interface StopsReportResult {
		ID: number;
		PlannedDate: Date;
		StartedOn: Date;
		StartedOnTime: System.TimeSpan;
		DayOfWeek: string;
		DayNum: number;
		RouteStatusCode: string;
		RouteID: number;
		MasterRouteName: string;
		AreaID: number;
		RouteNumber: string;
		Barcode: string;
		UnitID: number;
		UnitName: string;
		CityID: number;
		CityName: string;
		StopID: number;
		CustomerStartedOn: Date;
		CustomerStartedOnTime: System.TimeSpan;
		CustomerEndedOn: Date;
		CustomerEndedOnTime: System.TimeSpan;
		EndedOn: Date;
		EndedOnTime: System.TimeSpan;
		MinInStop: number;
		NumberOfDeliveries: number;
		NumberOfSignatures: number;
		NumberOfPickups: number;
		StopBillingStatusDescription: string;
		MissedStopReason: string;
		MissedStopID: number;
		Comments: string;
		IsGardaResponsibility: boolean;
		SubdivisionCountryCode: string;
		Address1: string;
		StopStatus: string;
		PackagesList: CashTrack.ClientPortal.PackagesReportResult[];
		UserName: string;
		IsBillable: boolean;
		IsActive: boolean;
		StopBillingStatusCode: string;
		AreaNumber: number;
		AreaName: string;
		CustomerNumber: number;
		VehicleBarcode: string;
		ContractID: number;
	}
	interface PackagesReportResult {
		ID: number;
		TransactionCode: string;
		TransactionID: number;
		StopID: number;
		Value: number;
		Commodity: string;
		Quantity: number;
		ItemID: number;
		ItemBarcode: string;
		ItemStatusCode: string;
		TruckID: string;
		EmployeeID: number;
		EmployeeBarcode: string;
		Date: Date;
		CTNumber: string;
		UnitID: number;
		AddressID: number;
		SiteLocationName: string;
		Address1: string;
		CityID: number;
		CityName: string;
		SubdivisionCountryCode: string;
		FromDescription: string;
		ToDescription: string;
		TransactionItemsList: CashTrack.ClientPortal.PackagesTransactionResult[];
		Username: string;
		OriginalBarcode: string;
		IsBillable: boolean;
		Isactive: boolean;
		MasterRouteNumber: string;
		MasterRouteName: string;
		AreaNumber: number;
		AreaName: string;
		EmployeeFullName: string;
		CustomerNumber: number;
		ContractID: number;
		BagoutType: string;
		AreaID: number;
		RouteID: number;
		IsBagout: boolean;
		EmployeeFirstName: string;
		EmployeeLastName: string;
	}
	interface SubDivisionResult {
		SubdivisionCountryCode: string;
		Username: string;
	}
	interface AddressResult {
		Username: string;
		Name: string;
		UnitID: number;
		SearchAddress: string;
		Address1: string;
		Address2: string;
		Address3: string;
		CityName: string;
		CityID: number;
		ZipPostalCode: string;
		SubdivisionCountryCode: string;
		IsActive: boolean;
	}
	interface ClientPortalUnitResult {
		UnitID: number;
		Name: string;
		Barcode: string;
		Username: string;
	}
}
declare module System {
	interface TimeSpan {
		Ticks: number;
		Days: number;
		Hours: number;
		Milliseconds: number;
		Minutes: number;
		Seconds: number;
		TotalDays: number;
		TotalHours: number;
		TotalMilliseconds: number;
		TotalMinutes: number;
		TotalSeconds: number;
	}
	interface Exception {
		Message: string;
		Data: any;
		InnerException: System.Exception;
		TargetSite: System.Reflection.MethodBase;
		StackTrace: string;
		HelpLink: string;
		Source: string;
		HResult: number;
	}
	interface Type extends System.Reflection.MemberInfo {
		MemberType: System.Reflection.MemberTypes;
		DeclaringType: System.Type;
		DeclaringMethod: System.Reflection.MethodBase;
		ReflectedType: System.Type;
		StructLayoutAttribute: System.Runtime.InteropServices.StructLayoutAttribute;
		GUID: System.Guid;
		DefaultBinder: System.Reflection.Binder;
		Module: System.Reflection.Module;
		Assembly: System.Reflection.Assembly;
		TypeHandle: System.RuntimeTypeHandle;
		FullName: string;
		Namespace: string;
		AssemblyQualifiedName: string;
		BaseType: System.Type;
		TypeInitializer: System.Reflection.ConstructorInfo;
		IsNested: boolean;
		Attributes: System.Reflection.TypeAttributes;
		GenericParameterAttributes: System.Reflection.GenericParameterAttributes;
		IsVisible: boolean;
		IsNotPublic: boolean;
		IsPublic: boolean;
		IsNestedPublic: boolean;
		IsNestedPrivate: boolean;
		IsNestedFamily: boolean;
		IsNestedAssembly: boolean;
		IsNestedFamANDAssem: boolean;
		IsNestedFamORAssem: boolean;
		IsAutoLayout: boolean;
		IsLayoutSequential: boolean;
		IsExplicitLayout: boolean;
		IsClass: boolean;
		IsInterface: boolean;
		IsValueType: boolean;
		IsAbstract: boolean;
		IsSealed: boolean;
		IsEnum: boolean;
		IsSpecialName: boolean;
		IsImport: boolean;
		IsSerializable: boolean;
		IsAnsiClass: boolean;
		IsUnicodeClass: boolean;
		IsAutoClass: boolean;
		IsArray: boolean;
		IsGenericType: boolean;
		IsGenericTypeDefinition: boolean;
		IsConstructedGenericType: boolean;
		IsGenericParameter: boolean;
		GenericParameterPosition: number;
		ContainsGenericParameters: boolean;
		IsByRef: boolean;
		IsPointer: boolean;
		IsPrimitive: boolean;
		IsCOMObject: boolean;
		HasElementType: boolean;
		IsContextful: boolean;
		IsMarshalByRef: boolean;
		GenericTypeArguments: System.Type[];
		IsSecurityCritical: boolean;
		IsSecuritySafeCritical: boolean;
		IsSecurityTransparent: boolean;
		UnderlyingSystemType: System.Type;
	}
	interface Attribute {
		TypeId: any;
	}
	interface Guid {
	}
	interface RuntimeFieldHandle {
		Value: number;
	}
	interface ModuleHandle {
		MDStreamVersion: number;
	}
	interface RuntimeTypeHandle {
		Value: number;
	}
	interface RuntimeMethodHandle {
		Value: number;
	}
}
declare module CashTrack.MM {
	interface ExportForImmediateCreditQuery {
		SiteID: number;
		BankID: number;
		Date: Date;
		IsFullExport: boolean;
		IsChaseFormat: boolean;
	}
	interface FileExportForImmediateCreditQuery extends CashTrack.MM.ExportForImmediateCreditQuery {
		Filename: string;
	}
	interface MMImportLogReportQuery extends CashTrack.Common.SearchTableQuery {
		Areas: number[];
		DateFrom: Date;
		DateTo: Date;
	}
	interface ExportForImmediateCreditResult {
		Amount: number;
		BankName: string;
		CommodityType: string;
		CustomerID: string;
		ItemBarcode: string;
		ItemID: number;
		ReferenceNumber: string;
		SiteNumber: string;
		BagoutBarcode: string;
		DatasetNumber: number;
	}
	interface MMErrorEventResult {
		ID: number;
		ImportLogID: number;
		ErrorEventTypeCode: number;
		RecordTypeCode: string;
		InvalidValue: string;
		RecordInfo: string;
		MMImportLog: Garda.CashTrackNG.Core.Contracts.Models.MM.Results.MMImportLogResult;
		RecordTypeCodeDescription: string;
		ErrorEventTypeCodeDescription: string;
		CreatedOn: Date;
	}
}
declare module CashTrack.ManageItems.Checkout {
	interface TransferToExtAreaResult {
		Amount: number;
		BankName: string;
		CommodityType: string;
		CustomerID: string;
		ItemBarcode: string;
		ItemID: number;
		ReferenceNumber: string;
		SiteNumber: string;
		BagoutBarcode: string;
		DatasetNumber: number;
	}
}
declare module Garda.CashTrackNG.Core.Contracts.Models.MM.Results {
	interface MMImportLogResult {
		ID: number;
		CreatedOn: Date;
		AreaID: number;
		CSVAreaNumber: string;
		CSVBankNumber: string;
		CSVCustomerNumber: string;
		DeliveryDate: Date;
		Area: CashTrack.Common.AreaResult;
	}
}
declare module CashTrack.Billing {
	interface JdeChangedLogReportQuery extends CashTrack.Common.SearchTableQuery {
		AreaId: number;
		ContractNumber: string;
		DateFrom: Date;
		DateTo: Date;
		UnitID: number;
		ChangedCodes: string[];
		JDEServiceLocationNumber: number;
	}
	interface JdeImportErrorLogReportQuery extends CashTrack.Common.SearchTableQuery {
		Areas: number[];
		ContractNumber: string;
		ImportStatusCode: string;
		RecordTypes: string[];
		DateFrom: Date;
		DateTo: Date;
		UnitID: number;
	}
	interface JdeChangedLogResult {
		ID: number;
		CreatedOn: Date;
		ContractNumber: number;
		ProductDescription: string;
		Description: string;
		PreviousValue: string;
		NewValue: string;
		ServiceChangeStartedOn: Date;
		ServiceChangeEndedOn: Date;
		Area: CashTrack.Common.AreaResult;
		Unit: CashTrack.Common.UnitResult;
	}
	interface JdeImportErrorLogResult {
		ID: number;
		CreatedOn: Date;
		ContractNumber: number;
		ContractLineNumber: number;
		AttributeBillingCode: string;
		JdeImportStatusCode: string;
		AreaID: number;
		ErrorMessage: string;
		InvalidValue: string;
		IsField: boolean;
		Area: CashTrack.Common.AreaResult;
		RecordType: string;
	}
	interface UnitsResult extends CashTrack.Common.UnitResult {
		Products: CashTrack.Common.ContractLineServiceChangeResult[];
		Destinations: CashTrack.Common.ShipperDestinationCitResult[];
		NonRevenueUnits: CashTrack.Common.NonRevenueUnitResult[];
	}
	interface CITUnitResult extends CashTrack.Common.UnitResult {
		Products: CashTrack.Common.ContractLineServiceChangeResult[];
		Holidays: CashTrack.Common.HolidayServiceSoldResult[];
		Destinations: CashTrack.Common.ShipperDestinationCitResult[];
		NonRevenueUnits: CashTrack.Common.NonRevenueUnitResult[];
		Area: CashTrack.Common.AreaDetailResult[];
		ClientKeys: CashTrack.Common.EquipmentResult;
		CVSArea: CashTrack.Common.CvsAreaResult;
		CVSBank: CashTrack.Common.CvsBankResult;
		AlternateDestination: CashTrack.Common.UnitResult;
	}
	interface AuthorizeBillingSettingResult {
		CurrentBillingYearMonth: Date;
		LastBillingTransferredOn: Date;
		NextBillingTransferredOn: Date;
		NextMonthEndTransferredOn: Date;
	}
	interface AuthorizeBillingExtCommand extends CashTrack.Common.Request {
		NextMonthEndTransferredOn: Date;
		NextBillingTransferredOn: Date;
		CurrentBillingYearMonth: Date;
		BillingAuthorizedBy: number;
	}
	interface AuthorizeBillingCommand extends CashTrack.Common.Request {
		NextBillingTransferredOn: Date;
		NextMonthEndTransferredOn: Date;
	}
}
declare module CashTrack.Route {
	interface MasterRouteServiceDayBase extends CashTrack.Common.Request {
		MasterRouteDetailID: number;
	}
	interface OnDemandCountByStopIdQuery {
		StopID: number;
	}
	interface AssignOnDemandSequenceResult {
		InternalSequenceNumber: number;
		SequenceNumber: number;
	}
	interface ChangeServiceDayResult {
		Success: boolean;
		CanRetry: boolean;
		NonBillableStopsNeedToBeReassign: boolean;
		ValidationResults: CashTrack.BusinessRuleValidationResult[];
	}
	interface ChangeServiceDayStage1Result {
		Today: Date;
		MaxPlannedDate: Date;
		SourceRouteIsOpened: boolean;
		SourceRouteIsInternal: boolean;
		ItemAttachedToStop: boolean;
		SourceRouteInPast: boolean;
	}
	interface ChangeServiceDayStage2Result {
		Routes: CashTrack.Route.ChangeServiceDayTargetRouteResult[];
	}
	interface ChangeServiceDayTargetRouteResult {
		RouteID: number;
		MasterRouteID: number;
		MasterRouteDetailID: number;
		RouteNumber: string;
		RouteTypeCode: string;
		AMorPMCode: string;
	}
	interface ChangeServiceDayStage3Result {
		Sequences: CashTrack.Route.UnitStopSequenceResult[];
		UnitAlreadyServicedOnTargetRoute: boolean;
	}
	interface UnitStopSequenceResult extends CashTrack.Route.StopSequenceResult {
		Barcode: string;
		Address: string;
	}
	interface StopSequenceResult {
		StopID: number;
		InternalSequenceNumber: number;
	}
	interface CopyHolidayRouteConfirmationResult {
		Selected: boolean;
		IsExist: boolean;
		MasterStopID: number;
		UnitID: number;
		MasterRouteDetailID: number;
		Category: string;
		Route: string;
		Description: string;
		RouteType: string;
		Cit: string;
		HolidayContracted: boolean;
		UnitName: string;
		UnitAddress: string;
		UnitCity: string;
		UnitZip: string;
		UnitType: string;
		IsUnitNonBillable: boolean;
	}
	interface CopyHolidayRouteSearchDataAreaResult {
		Holidays: CashTrack.Common.HolidayResult[];
		Routes: CashTrack.Route.HolidayMasterRouteResult[];
	}
	interface HolidayMasterRouteResult extends CashTrack.Common.MasterRouteResult {
		MasterRouteID: number;
		AreaHolidayDateID: number;
		MasterStopCount: number;
	}
	interface CopyHolidayRouteResult {
		Selected: boolean;
		ContractSelected: boolean;
		Category: string;
		Route: string;
		Day: string;
		Description: string;
		RouteType: string;
		RouteID: number;
		RouteDetailID: number;
	}
	interface InternalRouteConfigurationResult {
		UnitID: number;
		MasterStopID: number;
		DestinationAreaID: number;
		PlannedDate: Date;
		TransferInMasterStopID: number;
		TransferOutMasterStopID: number;
	}
	interface MasterRouteAddStage1Result {
		Area: CashTrack.Route.MasterRoutePerUnitAreaResult;
		MasterRoute: CashTrack.Route.MasterRoutePerUnitAddStage1MasterRouteResult;
		MasterRouteDetail: CashTrack.Route.MasterRoutePerUnitAddStage2MasterRouteDetailResult;
	}
	interface MasterRoutePerUnitAreaResult {
		AreaID: number;
		Name: string;
		Number: number;
	}
	interface MasterRoutePerUnitAddStage1MasterRouteResult {
		ID: number;
		Name: string;
		RouteNumber: string;
		AMorPMCode: string;
		RouteTypeCode: string;
		IsSpecialRoute: boolean;
		VersionEndDate: Date;
	}
	interface MasterRoutePerUnitAddStage2MasterRouteDetailResult {
		ID: number;
		ServiceDayCode: string;
		RouteCategoryCode: string;
		Holiday: CashTrack.Common.HolidayResult;
	}
	interface MasterRouteAddStage2Result {
		StopTypes: CashTrack.Route.MasterRoutePerUnitAddStageStopTypeResult[];
		IsUnitSuspended: boolean;
		IsUnitBillable: boolean;
		IsUnitNonBillable: boolean;
		IsHolidaySupposedToBeReported: boolean;
		UnitHasNoContractAtAll: boolean;
		ExistingMasterStopFlags: CashTrack.Route.MasterStopFlagsSummaryResult[];
		ExistingOtherMasterStopFlags: CashTrack.Route.MasterStopFlagsSummaryOtherRouteGroupResult[];
		IsRouteNotCVSOrUnitIsProcessCenterOrRouteUnit: boolean;
		ServiceDateStartedOn: Date;
		IsHolidayRoute: boolean;
		IsUsedAsAlternateDestination: boolean;
	}
	interface MasterRoutePerUnitAddStageStopTypeResult extends CashTrack.Common.StopTypeResult {
		IsShipmentStopFlagEnabled: boolean;
		IsDepositStopFlagEnabled: boolean;
		IsDeliveryStopFlagEnabled: boolean;
		IsDeliveredOnSameRouteFlagEnabled: boolean;
		IsKeyRequiredFlagEnabled: boolean;
	}
	interface MasterStopFlagsSummaryResult {
		ID: number;
		MasterStopID: number;
		UnitID: number;
		Sequence: number;
		IsDeliveryStop: boolean;
		IsShipmentStop: boolean;
	}
	interface MasterStopFlagsSummaryOtherRouteGroupResult {
		RouteID: number;
		RouteNumber: string;
		ExistingMasterStopFlags: CashTrack.Route.MasterStopFlagsSummaryResult[];
	}
	interface MasterRouteDetailSummaryResult {
		MasterRouteDetailID: number;
		IsSimulation: boolean;
		RouteCategoryCode: string;
		ServiceDayCode: string;
		Holiday: CashTrack.Common.HolidayResult;
	}
	interface MasterRouteFutureVersionResult {
		MasterRouteVersionID: number;
		EffectiveDateOn: Date;
	}
	interface MasterRouteAndDetailResult extends CashTrack.Route.MasterRouteDetailSummaryResult {
		MasterRouteID: number;
		Area: CashTrack.Common.AreaResult;
		RouteNumber: string;
		Name: string;
		AMorPMCode: string;
		RouteTypeCode: string;
		VersionsMinEffectiveDate: Date;
		VersionsMaxEndDate: Date;
		PlannedRouteDates: Date[];
		LastGeneratedDailyRouteDateOn?: Date;
	}
	interface AddMasterStopResult {
		Success: boolean;
		NonBillableStopsNeedToBeReassign: boolean;
		FutureVersionOfTheRouteExistOn: Date;
		SimulationVersionExistOn: Date;
		CanRetry: boolean;
		ValidationResults: CashTrack.BusinessRuleValidationResult[];
	}
	interface MasterRoutePerUnitAddRescheduleStopResult {
		IsDailyRouteAlreadyGenerated: boolean;
		MissedStop: CashTrack.Common.MissedStopToRescheduleResult;
	}
	interface MasterRoutePerUnitAddReportHolidayResult {
		Holiday: CashTrack.Common.HolidayResult;
		ReportCode: string;
		RegularDayHasUnit: boolean;
		IsReportHolidayBillable: boolean;
		IsReportHolidayBeingAddedAfterHolidayButShouldnt: boolean;
		IsReportHolidayBeingAddedBeforeHolidayButShouldnt: boolean;
	}
	interface MasterRoutePerUnitAddOnDemandResult {
		HasOnDemandServicesADay: boolean;
		HasOnDemandServicesADifferentDay: boolean;
		CustomerRequiresAuthorization: boolean;
		UnitIsAllowedOffDayServicing: boolean;
	}
	interface MasterRoutePerUnitAddStage1Result {
		MasterRoutes: CashTrack.Route.MasterRoutePerUnitAddStage1MasterRouteResult[];
	}
	interface MasterRoutePerUnitAddStage2Result {
		MasterRouteDetails: CashTrack.Route.MasterRoutePerUnitAddStage2MasterRouteDetailResult[];
	}
	interface MasterRoutePerUnitAddStage3Result extends CashTrack.Route.MasterRouteAddStage2Result {
		Sequences: CashTrack.Route.UnitStopSequenceResult[];
		DayIsNormallyServicedPerUnitContract: boolean;
		IsValidForProcessCenterUnit: boolean;
		IsValidForRouteUnit: boolean;
		IsValidForAreaUnit: boolean;
		IsHolidayRoute: boolean;
		IsSpecialRoute: boolean;
	}
	interface MasterRouteAddStage4Result {
		ReportHoliday: CashTrack.Route.MasterRoutePerUnitAddReportHolidayResult;
		RescheduleStop: CashTrack.Route.MasterRoutePerUnitAddRescheduleStopResult;
		OnDemand: CashTrack.Route.MasterRoutePerUnitAddOnDemandResult;
		IsTemporaryStopAddedOnHolidaySold: boolean;
		IsRegularStopAddedOnHolidaySold: boolean;
		ServiceDateBegunOn: Date;
		HasMasterStops: boolean;
		DoesMasterStopHasStop: boolean;
		IsUnitServicedOnTheDay: boolean;
		IsTemporaryStopAddedOnServicedDayHoliday: boolean;
		IsRegularStopAddedOnHolidaySoldNotServicedDay: boolean;
	}
	interface MasterRoutePerUnitInformationResult extends CashTrack.Route.StopInformationResult {
		MasterRoute: CashTrack.Route.MasterRouteSummaryResult;
		Sequence: number;
	}
	interface StopInformationResult {
		MasterStop: CashTrack.Route.MasterStopSummaryResult;
		Stop: CashTrack.Route.StopSummaryResult;
		Unit: CashTrack.Route.UnitSummaryResult;
		IsLinkedToOnDemandService: boolean;
	}
	interface MasterStopSummaryResult {
		ID: number;
		InternalSequenceNumber: number;
		StopTypeCode: string;
		IsShipmentStop: boolean;
		IsDepositStop: boolean;
		IsDeliveryStop: boolean;
		IsDeliveredOnSameRoute: boolean;
		IsKeyRequired: boolean;
		OperationComment: string;
		ServiceDateBegunOn: Date;
		ServiceDateEndedOn: Date;
		EndedTemporaryBegunOn: Date;
		EndedTemporaryEndedOn: Date;
		RelatedMasterStopID: number;
	}
	interface StopSummaryResult {
		ID: number;
		MasterStopID: number;
		InternalSequenceNumber: number;
		PlannedDate: Date;
		RouteStatusCode: string;
		StartedOn: Date;
	}
	interface UnitSummaryResult {
		ID: number;
		Barcode: string;
		Name: string;
		Address: string;
		City: string;
		SubdivisionCountry: string;
		ZipPostalCode: string;
		SuspendedBeganOn: Date;
		SuspendedEndedOn: Date;
		UnitTypeCode: string;
	}
	interface MasterRouteSummaryResult {
		MasterRouteID: number;
		MasterRouteDetailID: number;
		RouteCategoryCode: string;
		RouteNumber: string;
		Name: string;
		RouteTypeCode: string;
		AMorPMCode: string;
		ServiceDayCode: string;
		VersionEffectiveDateOn: Date;
		VersionEndDateOn: Date;
		IsSuspended: boolean;
	}
	interface MasterRoutePerUnitInitialViewResult {
		Areas: CashTrack.Route.MasterRoutePerUnitAreaResult[];
		RouteTypes: CashTrack.Common.RouteTypeResult[];
		NumberOfMasterRoutePlannedWeeks: number;
	}
	interface MasterRoutePerUnitUnitResult {
		UnitID: number;
		Barcode: string;
		Name: string;
		UnitTypeCode: string;
	}
	interface MasterRouteStopInformationResult {
		MasterRouteDetail: CashTrack.Route.MasterRouteDetailSummaryResult;
		RouteGenerated: boolean;
		RouteStatusCode: string;
		Stops: CashTrack.Route.StopInformationResult[];
	}
	interface MasterRouteTargetSequencesResult {
		Sequences: CashTrack.Route.UnitStopSequenceResult[];
	}
	interface MasterStopFlagsSummaryOtherRouteResult {
		ID: number;
		RouteID: number;
		RouteNumber: string;
		MasterStopID: number;
		UnitID: number;
		Sequence: number;
		IsDeliveryStop: boolean;
		IsShipmentStop: boolean;
	}
	interface MoveStopOnMasterRouteResult {
		MasterStopID: number;
		Success: boolean;
		CanRetry: boolean;
		ValidationResults: CashTrack.BusinessRuleValidationResult[];
	}
	interface MoveStopOnMasterRouteTargetSequencesResult {
		Sequences: CashTrack.Route.UnitStopSequenceResult[];
	}
	interface OnDemandServicesResult {
		BillingComment: string;
		PlannedServiceDate: Date;
		RequestedOn: Date;
		StopServiceID: number;
		ServiceCode: string;
		ServiceDescription: string;
		StopServiceBillingTypeID: number;
	}
	interface OnDemandToAssignResult {
		ServiceID: number;
		ServiceName: string;
		OnDemandNumber: number;
		OperationComment: string;
		BillingComment: string;
		MainStop: CashTrack.Common.StopServiceResult;
		Stop1: CashTrack.Common.StopServiceResult;
		Stop2: CashTrack.Common.StopServiceResult;
		RelatedStops: CashTrack.Common.StopServiceResult[];
		Route: CashTrack.Common.RouteResult;
		AvailableRoutesForMainUnit: CashTrack.Common.RouteResult[];
		TimeWindowFrom: Date;
		TimeWindowTo: Date;
		PlannedServiceDate: Date;
		RequestedOn: Date;
		TempTimeWindowFrom: System.TimeSpan;
		TempTimeWindowTo: System.TimeSpan;
		TempPlannedServiceDate: Date;
		TempRequestedOn: Date;
		IsCheckInStarted: boolean;
		ReadOnly: boolean;
		MasterRoute: CashTrack.Common.MasterRouteResult;
	}
	interface PrepareStopSequenceforRouteResult {
		ID: number;
		ReturnCode: string;
	}
	interface RemoveRelatedMasterStopResult extends CashTrack.Route.RemoveMasterStopResult {
		RelatedMasterStopResults: CashTrack.Common.RelatedMasterStopResult[];
	}
	interface RemoveMasterStopResult {
		Success: boolean;
		FutureVersionDate: Date;
		SimulationVersionDate: Date;
		CanRetry: boolean;
		ValidationResults: CashTrack.BusinessRuleValidationResult[];
		RouteIsOpen: boolean;
		RelatedMasterStopToRemove: string;
	}
	interface CopyHolidayRouteSearchDataResult {
		RouteTypes: CashTrack.Common.RouteTypeResult[];
	}
	interface ResequenceStopResult {
		Success: boolean;
		InternalSequenceNumber: number;
	}
	interface TargetMasterRouteResult {
		MasterRouteDetailID: number;
		RouteNumber: string;
		Name: string;
		RouteTypeCode: string;
	}
	interface ValidateUnitStopsReportResult {
		UnitBarcode: string;
		JDEServiceLocationNumber: number;
		Contracts: string;
		ContractLines: string;
		UnitNameAddress: string;
		ContractFrequencies: string;
		Date: Date;
		IsContractValid: string;
		IsRouteValid: string;
		IsNBValid: string;
		DatasetNumber: number;
	}
	interface ValidateUnitStopsResult {
		UnitBarcode: string;
		UnitNameAddress: string;
		ContractFrequencies: string;
		Contracts: string;
		ContractLines: string;
		JDEServiceLocationNumber: number;
		ValidationResultPerDay: Garda.CashTrackNG.Core.Contracts.Models.Route.Results.ValidateUnitStopsPerDayResult[];
	}
	interface ChangeServiceDayStage1Query {
		StopID: number;
	}
	interface ChangeServiceDayStage2Query {
		StopID: number;
		PlannedDate: Date;
	}
	interface ChangeServiceDayStage3Query extends CashTrack.Common.Request {
		RouteID: number;
		StopID: number;
		MasterRouteDetailID: number;
		Date: Date;
	}
	interface FindCopyHolidayRouteDataQuery extends CashTrack.Common.SearchTableQuery {
		AreaID: number;
		HolidayDateID: number;
		IsAM: boolean;
		QueryDate?: Date;
		HolidayDateCode?: string;
		RouteTypeCodes: string[];
	}
	interface CopyHolidayRouteStopsQuery extends CashTrack.Common.Request {
		AreaID: number;
		RouteID: number;
		HolidayDate: Date;
		AreaHolidayDateID: number;
		StopIDs: number[];
		UnitIDs: number[];
	}
	interface FindCopyHolidayConfirmationQuery {
		AreaID: number;
		HolidayDate: Date;
		SelectedHolidayRouteID: number;
		HolidayRouteID: number[];
		HolidayCode: string;
		RouteIDs: number[];
		RouteIDsOnlyContracts: number[];
		IncludeNonBillable: boolean;
	}
	interface ShuttleRouteForRouteConfigurationQuery extends CashTrack.Common.Request {
		UnitID: number;
		PlannedDate: Date;
		MasterStopID: number;
		InternalSequenceNumber: number;
	}
	interface ShuttleRoutesForRouteConfigurationQuery extends CashTrack.Common.Request {
		ShuttleRoutes: CashTrack.Route.ShuttleRouteForRouteConfigurationQuery[];
	}
	interface VerifyNetworkAreaQuery {
		ManifestingList: CashTrack.Route.VerifyNetworkQueryList;
		PickUpList: CashTrack.Route.VerifyNetworkQueryList;
	}
	interface VerifyNetworkQueryList {
		List: CashTrack.Route.VerifyNetworkQuery[];
	}
	interface VerifyNetworkQuery extends CashTrack.Common.Request {
		ShipperID: number;
		SourceAreaID: number;
		DestinationID: number;
		CommodityID: number;
		VerifyNetworkID: number;
	}
	interface FindHolidayRoutesQuery extends CashTrack.Common.Request {
		AreaID: number;
		QueryDate?: Date;
	}
	interface IsUnitBillableQuery {
		UnitID: number;
		AreaID: number;
		Date: Date;
	}
	interface MasterRouteAddStage1Query {
		MasterRouteDetailID: number;
	}
	interface MasterRouteAddStage2Query extends CashTrack.Common.Request {
		MasterRouteDetailID: number;
		WeekOfDate: Date;
		UnitID: number;
	}
	interface MasterRoutePerUnitAddStage2Query extends Garda.CashTrackNG.Core.Contracts.Models.Route.Queries.MasterRoutePerUnitAddStageBaseQuery {
		MasterRouteID: number;
	}
	interface MasterRoutePerUnitAddStage3Query extends Garda.CashTrackNG.Core.Contracts.Models.Route.Queries.MasterRoutePerUnitAddStageBaseQuery {
		MasterRouteDetailID: number;
	}
	interface MasterRouteAddStage4Query extends Garda.CashTrackNG.Core.Contracts.Models.Route.Queries.MasterRoutePerUnitAddStageBaseQuery {
		MasterRouteDetailID: number;
		StopTypeCode: string;
	}
	interface MasterRoutePerUnitAddStage1Query extends Garda.CashTrackNG.Core.Contracts.Models.Route.Queries.MasterRoutePerUnitAddStageBaseQuery {
		AreaID: number;
	}
	interface MasterRoutePerUnitInformationQuery {
		AreaID: number;
		UnitID: number;
		RouteTypeCodes: string[];
		WeekOfDate: Date;
		OnlySuspended: boolean;
		Simulation: boolean;
		RouteCategoryCodes: string[];
		ServiceDayCodes: string[];
		AMorPMRouteCodes: string[];
	}
	interface OnDemandServicesQuery {
		UnitID: number;
		AreaID: number;
		Date: Date;
	}
	interface RouteSheetReportQuery {
		PlannedDate: Date;
		AllRoutes: boolean;
		AreaID: number;
		RouteNumbers: string[];
		ReportType: CashTrack.Consts.RouteSheetReportTypeEnum;
		IsDraft: boolean;
		PrintedOn: Date;
	}
	interface MasterRouteFutureVersionQuery extends CashTrack.Route.MasterRouteServiceDayBase {
	}
	interface MasterRouteInformationQuery extends CashTrack.Route.MasterRouteServiceDayBase {
		IsCurrentDay: boolean;
	}
	interface MasterRouteStopInformationQuery extends CashTrack.Route.MasterRouteServiceDayBase {
		IsCurrentDay: boolean;
		WeekOfDate: Date;
		DailySequenceDate?: Date;
	}
	interface MoveTargetMasterRouteQuery extends CashTrack.Route.MasterRouteServiceDayBase {
		Date: Date;
	}
	interface OnDemandByAreaAnDatesQuery extends CashTrack.Common.SearchTableQuery {
		AreaID: number;
		RouteStatus?: CashTrack.Consts.AssignOnDemandRouteStatusEnum[];
		FromDate: Date;
		ToDate: Date;
	}
	interface StopAssignmentInfoQuery {
		OnDemandNumber: number;
		UnitID: number;
		SequenceNumber: number;
		RouteID: number;
	}
	interface ValidateUnitStopsQuery {
		AreaID: number;
		SelectedWeek: Date;
		FromDate: Date;
		ToDate: Date;
		SelectedDays: CashTrack.Consts.DayOfWeekEnum[];
		ShowInconsistentOnly: boolean;
		ValidateRoute: boolean;
		ValidateContract: boolean;
		ValidateNonBillable: boolean;
	}
	interface RemoveRelatedMasterStopsCommand extends CashTrack.Common.Request {
		MasterStopID: number;
		RelatedMasterStops: CashTrack.Common.RelatedMasterStopResult[];
		Date: Date;
		ConfirmedErrors: CashTrack.BusinessRuleValidationResult[];
	}
	interface SeasonalCITServiceQuery {
		UnitID: number;
		MasterRouteDetailID: number;
		RequestDateTime: Date;
	}
	interface AssignMasterStopCommand {
		StopServiceID: number;
		MasterStopID: number;
		SequenceNumber?: number;
		AfterStopID?: number;
		BeforeStopID?: number;
	}
	interface AssignOnDemandServiceCommand {
		AssignmentInfo: CashTrack.Route.AssignOnDemandCommand[];
	}
	interface AssignOnDemandCommand extends CashTrack.Common.Request {
		OnDemandNumber: number;
		UnitID: number;
		SequenceNumber: number;
		RouteID: number;
		MasterRouteDetailID: number;
		WeekOfDate: Date;
		StopID: number;
		AfterMasterStopID: number;
		BeforeMasterStopID: number;
		SkipExistingStop: boolean;
		ServiceDate: Date;
	}
	interface ChangeServiceDayCommand extends CashTrack.Common.Request {
		StopID: number;
		RouteID: number;
		BeforeMasterStopID: number;
		AfterMasterStopID: number;
		ConfirmedErrors: CashTrack.BusinessRuleValidationResult[];
	}
	interface AddMasterStopCommand extends CashTrack.Common.Request {
		UnitID: number;
		WeekOfDate: Date;
		MasterRouteDetailID: number;
		BeforeMasterStopID: number;
		AfterMasterStopID: number;
		StopTypeCode: string;
		IsShipmentStop: boolean;
		IsDepositStop: boolean;
		IsDeliveryStop: boolean;
		IsDeliveredOnSameRoute: boolean;
		IsKeyRequired: boolean;
		OperationComment: string;
		ServiceDateEndedOn: Date;
		EndedTemporaryBegunOn: Date;
		EndedTemporaryEndedOn: Date;
		AreaHolidayDateID: number;
		MissedStopID: number;
		OnDemandServiceIDs: number[];
		ConfirmedErrors: CashTrack.BusinessRuleValidationResult[];
		AttachedStop: boolean;
		RelatedMasterStopID: number;
	}
	interface ResequenceStopForRouteCommand {
		RouteID: number;
	}
	interface AssignStopServiceCommand {
		StopServiceID: number;
		StopID: number;
		MasterStopID?: number;
		SequenceNumber?: number;
		AfterStopID?: number;
		BeforeStopID?: number;
	}
	interface CreateRouteCommand {
		MasterRouteID: number;
		PlannedDate: Date;
		EndDate: Date;
		RouteStatusCode: string;
		MasterRouteDetailID: number;
		TankerID: number;
		CreatedBy?: number;
	}
	interface MoveStopOnMasterRouteCommand extends CashTrack.Route.MasterRouteServiceDayBase {
		MasterStopID: number;
		BeforeStopID: number;
		AfterStopID: number;
		Date: Date;
		ConfirmedErrors: CashTrack.BusinessRuleValidationResult[];
	}
	interface RemoveMasterStopCommand extends CashTrack.Common.Request {
		MasterStopID: number;
		Date: Date;
		ConfirmedErrors: CashTrack.BusinessRuleValidationResult[];
	}
	interface RemoveMasterStopStopsCommand {
		MasterStopID: number;
		EndedTemporaryBegunOn: Date;
		EndedTemporaryEndedOn: Date;
	}
	interface RemoveOrphandStopsCommand {
		StopIdList: number[];
	}
	interface ResequenceStopCommand {
		BeforeStopID: number;
		AfterStopID: number;
		StopID: number;
	}
}
declare module Garda.CashTrackNG.Core.Contracts.Models.Route.Results {
	interface ValidateUnitStopsPerDayResult {
		Date: Date;
		IsContractValid: boolean;
		IsRouteValid: boolean;
		IsNBValid: boolean;
	}
}
declare module Garda.CashTrackNG.Core.Contracts.Models.Route.Queries {
	interface MasterRoutePerUnitAddStageBaseQuery extends CashTrack.Common.Request {
		UnitID: number;
		WeekOfDate: Date;
	}
}
declare module CashTrack.Reporting {
	interface ECashItemsReportQuery extends CashTrack.Common.Request {
		AreaID: number;
		InInventory: boolean;
		OnRoute: boolean;
		CVSBankList: string;
		CVSBankNumbers: string;
	}
	interface EmployeeReportQuery {
		AreaID: number;
	}
	interface ValidateUnitStopReportQuery {
		StartDate: Date;
		IsPDF: boolean;
		AreaID: number;
		IsContracts: boolean;
		IsRoutes: boolean;
		IsNonBillable: boolean;
		UnitsWithInconsitenciesOnly: boolean;
		PrintedOn: Date;
		SelectedWeek: Date;
		FromDate: Date;
		ToDate: Date;
		SelectedDays: CashTrack.Consts.DayOfWeekEnum[];
	}
	interface ValidateUnitStopReportModel {
		ValidateUnitStopReportInfo: Garda.CashTrackNG.Core.Contracts.Models.Reporting.ValidateUnitStopReportInfo[];
		ValidateUnitStopsResult: CashTrack.Route.ValidateUnitStopsReportResult[];
		Footer: Garda.CashTrackNG.Core.Contracts.Models.Reporting.RouteReportFooterResult[];
	}
	interface CoinPullSheetsReportQuery {
		DeliveryDate: Date;
		SourceAreaID: number;
		UnitID: number;
		SourceRoutes: number[];
		DeliveryRoutes: number[];
		CoinUnits: string[];
		CustomerDetail: boolean;
		CoinDenomination: string;
		SearchSourceRoutes: string;
		SearchDeliveryRoutes: string;
		SearchCoinUnits: string;
		PrintedOn: Date;
	}
	interface CoinPullSheetsReportModel {
		CoinPullSheetInfo: Garda.CashTrackNG.Core.Contracts.Models.Reporting.CoinPullSheetsReportInfo[];
		CoinByBank: Garda.CashTrackNG.Core.Contracts.Models.Reporting.CoinByBankResult[];
		Footer: Garda.CashTrackNG.Core.Contracts.Models.Reporting.RouteReportFooterResult[];
	}
}
declare module Garda.CashTrackNG.Core.Contracts.Models.Reporting {
	interface ValidateUnitStopReportInfo {
		FromDate: string;
		ToDate: string;
		SearchArea: string;
		IsContracts: boolean;
		IsRoutes: boolean;
		IsNonBillable: boolean;
		DatasetNumber: number;
	}
	interface RouteReportFooterResult {
		PrintedOn: Date;
		DatasetNumber: number;
	}
	interface CoinPullSheetsReportInfo {
		SearchAreas: string;
		SearchDate: string;
		SearchDeliveryRoutes: string;
		SearchListCoinUnits: string;
		SearchSourceRoutes: string;
		DatasetNumber: number;
	}
	interface CoinByBankResult {
		BankSource: string;
		CoinDenominationCode: string;
		CoinDenominationID: number;
		CoinPackageCode: string;
		CoinPackageID: number;
		CoinUnits: string[];
		DeliveryArea: number;
		DeliveryDate: Date;
		DeliveryRoute: string;
		IsRedelivery: boolean;
		ParamDeliveryRoute: number[];
		ParamSourceRoute: number[];
		Quantity: number;
		SourceArea: number;
		SourceRouteID: number;
		SourceRoute: string;
		UnitDestination: string;
		UnitID: number;
		DatasetNumber: number;
	}
	interface ManifestSummaryItemReportResult {
		Barcode: string;
		DelDate: Date;
		DelRoute: string;
		DestinationBarcode: string;
		DestinationName: string;
		Id: number;
		Items: number;
		Value: number;
		IsCancelled: boolean;
		DatasetNumber: number;
	}
	interface TankerManifestReportItem {
		CommodityCode: string;
		DeliveryDate: Date;
		DestinationID: string;
		DestinationName: string;
		ItemAmount: number;
		ItemBarcode: string;
		ItemID: number;
		RouteID: number;
		RouteNumber: string;
		ShipperID: string;
		ShipperName: string;
		DatasetNumber: number;
	}
}
declare module CashTrack.ItemManifest {
	interface ManifestSummaryReportQuery {
		ParcelDatedBankSourceTypeID: number[];
		CoinDatedBankSourceTypeID: number[];
		PrintedOn: Date;
		AreaID: number;
		FromDate: Date;
		ToDate: Date;
		ManifestedBy: string;
	}
	interface CVSBankResult {
		ID: number;
		CVSBankNumber: string;
		CVSBankName: string;
	}
	interface CVSBankSourceResult {
		ID: number;
		CVSAreaNumber: string;
		CVSBankNumber: string;
		BankSourceTypeID: number;
		DenominationGroupCode: string;
	}
	interface DestinationSourceWithDeliveryDayCodeResult {
		TotalCount: number;
		UnitID: number;
		Barcode: string;
		Name: string;
		AreaName: string;
		SUN: string;
		MON: string;
		TUE: string;
		WED: string;
		THU: string;
		FRI: string;
		SAT: string;
	}
	interface ECashBankNumberResult {
		CVSBankNumber: string;
	}
	interface ExistingCVSBankSourceResult {
		ID: number;
		CVSAreaNumber: string;
		CVSBankNumber: string;
		BankSourceTypeID: number;
		DenominationGroupCode: string;
		Denomination: string;
		BankSourceTypeName: string;
	}
	interface OddCoinResult {
		ID: number;
		Code: string;
		Description: string;
	}
	interface VerifyNetworkRoutesResult {
		UnitID: number;
		DestinationAreaID: number;
		SourceAreaID: number;
		SourceArea: string;
		RouteID: number;
		FinalRoute: string;
		ShuttleRouteID: number;
		ShuttleRoute: string;
		TransferRouteID: number;
		TransferRoute: string;
		CommodityID: number;
		CommodityName: string;
		DestinationName: string;
		DestinationBarcode: string;
		ShipperName: string;
		ShipperBarcode: string;
	}
	interface ManifestCoinItemCommand {
		CoinDenominationID: number;
		CoinDenominationCode: string;
		Amount: number;
		Quantity: number;
		CommodityID: number;
	}
	interface UpdateManifestedItemCommand {
		ItemID: number;
		Amount: number;
		DeliveryDate: Date;
		RouteNumber: string;
		CoinResults: CashTrack.ItemManifest.CoinResult[];
		CoinShipmentID: number;
		CoinShipmentBarcode: string;
		RouteID: number;
		DatedBankSourceTypeID: number;
		UnitID: number;
		ShipperUnitID: number;
		CustomerShipmentID: number;
		IsClone: boolean;
		CommodityID: number;
		AreaDestinationID: number;
		AreaSourceID: number;
		BagSeal?: string;
		ExpirationDate?: Date;
		CreatedBy?: number;
		CreatedOn?: Date;
		OriginalItemID?: number;
		OrderNumber?: string;
	}
	interface CoinResult {
		ItemID: number;
		CoinDenominationID: number;
		CoinDenominationCode: string;
		Amount: number;
		Quantity: number;
		SequenceNumber: number;
		Description: string;
		CoinPackageSizeDescription: string;
		CommodityID: number;
	}
	interface CoinKeyinOrderResult {
		SelectedCoinDenominationResult: CashTrack.Common.CoinDenominationResult;
		CoinDenominationResults: CashTrack.Common.CoinDenominationResult[];
	}
	interface EditSourceResult {
		SourceResult: CashTrack.ItemManifest.SourceResult;
		Areas: CashTrack.Common.AreaResult[];
		Commodities: CashTrack.Common.CommodityResult[];
		Shippers: CashTrack.Common.UnitResult[];
		SourceRoutes: CashTrack.Common.MasterRouteResult[];
		CoinPackageSizes: CashTrack.Common.CoinPackageSizeResult[];
		CoinKeyingOrders: CashTrack.ItemManifest.CoinKeyinOrderResult[];
		DeliveryAreas: CashTrack.Common.AreaResult[];
		OddCoins: CashTrack.ItemManifest.OddCoinResult[];
		CVSBanks: CashTrack.ItemManifest.CVSBankResult[];
		CvsBankSources: CashTrack.ItemManifest.CVSBankSourceResult[];
		CVSAreas: CashTrack.Common.CvsAreaResult[];
	}
	interface SourceResult {
		BankSourceTypeID: number;
		AreaID: number;
		Name: string;
		LongDescription: string;
		SourceTypeID: number;
		ShipperUnitID: number;
		ShortDescription: string;
		ReportDescription: string;
		SourceMasterRouteID: number;
		EffectiveDate: Date;
		EndDate: Date;
		DefaultCommodityID: number;
		IsCodPayment: boolean;
		IsBankSourcePrintedOnPod: boolean;
		CoinPackageSizeID: number;
		IsCoinInputByAmount: boolean;
		UseCustomList: boolean;
		IsECashSource: boolean;
		OddCoinID: number;
		ECashExpirationDays: number;
		CoinInputSequenceResults: CashTrack.Common.CoinInputSequenceResult[];
		Errors: CashTrack.Common.ValidationErrorResult[];
	}
	interface EnterShipmentResult {
		DatedBankSourceTypeResult: CashTrack.Common.DatedBankSourceTypeResult[];
		DestinationShipmentResults: CashTrack.ItemManifest.DestinationShipmentResult[];
		ManifestedShipmentResults: CashTrack.ItemManifest.ManifestedItemResult[];
		CoinDenominationResults: CashTrack.Common.CoinDenominationResult[];
		CommodityDescription: string;
	}
	interface DestinationShipmentResult {
		ID: number;
		ShipperID: number;
		DestinationID: number;
		BankSourceTypeID: number;
		CommodityID: number;
		Barcode: string;
		Name: string;
		Address: string;
		CityState: string;
		DeliveryDate: Date;
		AreaID: number;
		DestinationShipmentCoinDenominationResults: CashTrack.Common.DestinationShipmentCoinDenominationResult[];
		RouteNumber: string;
		Amount: number;
		CVSBankNumber: string;
	}
	interface ManifestedItemResult {
		ItemID: number;
		UnitBarcode: string;
		Name: string;
		Address: string;
		CityState: string;
		Amount: number;
		ItemBarcode: string;
		DeliveryDate: Date;
		PlannedDate: Date;
		RouteNumber: string;
		CoinResults: CashTrack.ItemManifest.CoinResult[];
		ItemStatusCode: string;
		IsItemPrintedOnRunSheet: boolean;
		CoinShipmentID: number;
		CoinShipmentBarcode: string;
		DatedBankSourceTypeID: number;
		PreparedOn: Date;
		SourceMasterRouteID: number;
		Description: string;
		UnitID: number;
		ShipperUnitID: number;
		CustomerShipmentID: number;
		RouteID: number;
		CommodityID: number;
		IsClone: boolean;
		AreaDestinationID: number;
		MissingClosedOn: Date;
		CurrentTankerID: number;
		TankerTypeID: number;
		ItemShipmentStatusCode: string;
		ItemShipmentRouteID: number;
		ShipperID: number;
		IsCancelled: boolean;
		IsMissing?: boolean;
	}
	interface ManifestedShipmentItemisedResult {
		ItemID: number;
		InputSeq: number;
		Unit: string;
		Name: string;
		Address: string;
		CityState: string;
		Quantity: number;
		Amount: number;
		Barcode: string;
		Route: string;
		ItemStatusCode: string;
		IsItemPrintedOnRunSheet: boolean;
		CoinDenominationID: number;
		CoinDenominationCode: string;
		CoinShipmentID: number;
	}
	interface RouteShipmentResult {
		AreaDestinationID: number;
		RouteID: number;
		ShuttleRouteID: number;
		CustomerShipmentID: number;
		BankSourceTypeName: string;
		BankSourceTypeDesc: string;
		UnitID: number;
		UnitBarcode: string;
		UnitName: string;
		UnitAddress: string;
		UnitCityName: string;
		UnitCountryCode: string;
		UnitZipCode: string;
		IsUnitSuspended: boolean;
		DeliveryDate: Date;
		NextDeliveryDate: Date;
	}
	interface DestinationSourceResult {
		UnitID: number;
		Barcode: string;
		Name: string;
		AreaName: string;
		AreaServiceDays: string[];
	}
	interface SearchSourceResult {
		ID: number;
		Name: string;
		Description: string;
		CommodityGroup: string;
		ShipperUnitId: number;
		ShipperUnit: string;
		ShipperName: string;
		SourceRoute: string;
		EffectiveDate: Date;
		EndDate: Date;
	}
	interface ManifestSummaryItemCoinResult {
		NameShortDescription: string;
		Quantity: number;
		Order: number;
	}
	interface ManifestSummaryItemResult {
		DatedBankSourceTypeID: number;
		DeliveryDate: Date;
		TotalParcel: number;
		TotalParcelAmount: number;
		TotalCoinAmount: number;
		CoinCommodities: CashTrack.ItemManifest.ManifestSummaryItemCoinResult[];
	}
	interface ManifestSummaryResult {
		DatedBankSourceTypeID: number;
		BSTDescription: string;
		IsParcelSourceType: boolean;
		IsCoinSourceType: boolean;
		ManifestItems: CashTrack.ItemManifest.ManifestSummaryItemResult[];
	}
	interface ShipmentLabelResult {
		ItemID: number;
		Barcode: string;
		Source: string;
		SourceDesc: string;
		RouteNumber: string;
		UnitBarcode: string;
		Name: string;
		Address: string;
		City: string;
		State: string;
		Amount: number;
		DeliveredOn: Date;
		LabelPrintedOn: Date;
		AreaID: number;
	}
	interface ConfirmRouteShipmentResult {
		Success: boolean;
		RunSheetPrinted: boolean;
	}
	interface SearchRouteResult {
		RouteID: number;
		MasterRouteDetailID: number;
		RouteNumber: string;
		PlannedDate: Date;
		CutoffTime: Date;
		Unit: CashTrack.Route.UnitSummaryResult;
		ShuttleRouteID: number;
		AreaDestinationID: number;
		MasterRouteID?: number;
		IsPlannedStop?: boolean;
	}
	interface CVSBankSourceQuery {
		CVSAreaNumber: string;
		BankSourceTypeID: number;
	}
	interface ECashBankNumberQuery {
		BankSourceTypeID: number;
		UnitID: number;
	}
	interface PrintDeliveryPickupQuery {
		StopId: number;
		ItemTransactionTypeCode: string;
	}
	interface DestinationShipmentQuery extends CashTrack.Common.SearchTableQuery {
		BankSourceTypeID: number;
		ManifestDate: Date;
		DeliveryDate: Date;
		MasterRouteID: number;
		SourceTypeCode: string;
		IsUsingSourceRoute: boolean;
	}
	interface ManifestedItemsQuery {
		BankSourceTypeID: number;
		ManifestDate: Date;
		MasterRouteID: number;
		SourceTypeCode: string;
		IsUsingSourceRoute: boolean;
	}
	interface EditSourceQuery {
		BankSourceTypeID: number;
		AreaID: number;
		IsECashSource: boolean;
		CVSAreaNumber?: string;
	}
	interface ManifestSummaryByDBSTQuery {
		DateBankSourceTypesID: number[];
	}
	interface ManifestQuery extends CashTrack.Common.Request {
		DatedBankSourceTypeIds: number[];
		AreaID?: number;
		ManifestingDate: Date;
		DateFrom: Date;
		DateTo: Date;
	}
	interface RoutedStopQuery {
		UnitID: number;
		AreaDestinationID: number;
		DeliveryDate: Date;
	}
	interface EnterShipmentQuery {
		BankSourceTypeID: number;
		DatedBankSourceTypeID: number;
		IsCoin: boolean;
		IsCoinInputByAmount: boolean;
		PreparedOn: Date;
	}
	interface ShippingSheetReportQuery extends CashTrack.Common.Request {
		PlannedDate: Date;
		AreaID: number;
		RouteNumbers: string[];
		AllRoutes: boolean;
		ExcludeStopNoShipment: boolean;
		IsDraft: boolean;
		PrintedOn: Date;
	}
	interface PrintDeliveryReceiptQuery {
		PlannedDate: Date;
		AreaID: number;
		RouteNumbers: string[];
		IsDraft: boolean;
	}
	interface GetSourceForDestinationQuery {
		SourceID: number;
		SourceRouteID: number;
		IsUsingSourceRoute: boolean;
		UnitID: number;
	}
	interface SourceDestinationUnitsQuery extends CashTrack.Common.SearchTableQuery {
		BankSourceTypeID: number;
		ShipperUnitID: number;
		DayCode: string;
		QueryDate: Date;
	}
	interface SearchSourceByParamsQuery extends CashTrack.Common.SearchTableQuery {
		AreaID: number;
		Source: string;
		CommodityGroup: number;
		Shipper: number;
		Active: boolean;
		Inactive: boolean;
		Today: Date;
	}
	interface SearchRouteQuery extends CashTrack.Common.Request {
		DeliveryDate: Date;
		Zipcode: string;
		Address: string;
		ExcludeInternal: boolean;
		AreaID?: number;
		SourceAreaID?: number;
		City: string;
		UnitID?: number;
	}
	interface RouteShipmentByItemIdQuery {
		ItemIDs: number[];
		StartAt: number;
	}
	interface UpdateECashDestinationAreaCommand {
		ItemID: number;
		AreaID: number;
	}
	interface ECashItemCommand {
		ItemID: number;
		ExpirationDate: Date;
		ECashStatusID: number;
		SourceAreaID: number;
		DestinationAreaID: number;
		CVSBankNumber: string;
	}
	interface CVSBankSourceCommand {
		ID?: number;
		CVSAreaNumber: string;
		CVSBankNumber: string;
		BankSourceTypeID: number;
		DenominationGroupCode: string;
	}
	interface CVSBankSourceListCommand {
		BankSourceTypeID: number;
		CVSBankSourceCommands: CashTrack.ItemManifest.CVSBankSourceCommand[];
	}
	interface SaveEnterShipmentCommand extends CashTrack.Common.Request {
		ManifestItems: CashTrack.ItemManifest.ManifestItemCommand[];
		ManifestedItemsForUpdate: CashTrack.ItemManifest.UpdateManifestedItemCommand[];
		EmployeeID: number;
		BankSourceTypeID: number;
		ManifestDate: Date;
		DeliveryDate: Date;
		MasterRouteID: number;
		SourceTypeCode: string;
		IsUsingSourceRoute: boolean;
	}
	interface ManifestItemCommand {
		DestinationID: number;
		CommodityID: number;
		BankSourceTypeID: number;
		DeliveryDate: Date;
		Quantity: number;
		Amount: number;
		DestinationAreaID: number;
		SourceAreaID: number;
		ManifestCoinItems: CashTrack.ItemManifest.ManifestCoinItemCommand[];
		BagSeal?: string;
		ExpirationDate?: Date;
		CreatedBy?: number;
		CreatedOn?: Date;
		OrderNumber?: string;
		CVSBankNumber: string;
	}
	interface CloneShipmentCommand {
		ItemID: number;
		AreaID: number;
		DatedBankSourceTypeID: number;
		InputManifestSequenceNumber: number;
		CoinResults: CashTrack.ItemManifest.CoinResult[];
	}
	interface ConfirmRouteShipmentNextDeliveryDateCommand {
		CustomerShipmentID: number;
		AreaDestinationID: number;
		DeliveryDate: Date;
		RouteID: number;
		ShuttleRouteID: number;
		DatedBankSourceTypeIds: number[];
	}
	interface ConfirmRouteShipmentRouteCommand extends CashTrack.Common.Request {
		CustomerShipmentID: number;
		AreaSourceID: number;
		AreaDestinationID: number;
		PlannedDate: Date;
		MasterRouteDetailID: number;
		RouteID: number;
		ShuttleRouteID: number;
		EmployeeID: number;
		DatedBankSourceTypeIds: number[];
	}
	interface UpdateShippableItemPrintDateCommand extends CashTrack.Common.Request {
		ItemIDs: number[];
	}
	interface UpdateSourceCommand {
		BankSourceTypeID: number;
		AreaID: number;
		Name: string;
		LongDescription: string;
		SourceTypeID: number;
		ShipperUnitID: number;
		ShortDescription: string;
		ReportDescription: string;
		SourceMasterRouteID: number;
		EffectiveDate: Date;
		EndDate: Date;
		DefaultCommodityID: number;
		IsCodPayment: boolean;
		IsBankSourcePrintedOnPod: boolean;
		CoinPackageSizeID: number;
		IsCoinInputByAmount: boolean;
		CoinDenominationIDs: number[];
		DeliveryAreaIDs: number[];
		IsECashSource: boolean;
		OddCoinID: number;
		ECashExpirationDays: number;
		CVSBankSourceCommands: CashTrack.ItemManifest.CVSBankSourceCommand[];
	}
}
declare module CashTrack.ItemManifest.Reporting {
	interface ManifestSummaryReportResult {
		Id: number;
		Area: string;
		BankSource: string;
		Unit: string;
		Shipper: string;
		CommodityGroup: string;
		ManifestingDate: Date;
		DenominationQty: string;
		Items: Garda.CashTrackNG.Core.Contracts.Models.Reporting.ManifestSummaryItemReportResult[];
	}
	interface TotalCoinCommodity {
		CommodityQuantity: number;
		CommodityAmount: number;
		ShortDescription: string;
		Description: string;
		IsRedelivery: boolean;
		RouteID: number;
		DisplaySortOrder: number;
		DatasetNumber: number;
	}
	interface TankerManifestReportQuery {
		AreaID: number;
		TankerID: number;
		TankerBarcode: string;
		TankerName: string;
		Option: CashTrack.Consts.TankerManifestReportEnum;
		From: Date;
		To: Date;
		PrintedOn: Date;
	}
	interface TankerManifestReportHeader {
		FromDate: string;
		ReportOption: string;
		Tanker: string;
		Title: string;
		ToDate: string;
		UserName: string;
		DatasetNumber: number;
	}
	interface TankerManifestReportModel {
		Headers: CashTrack.ItemManifest.Reporting.TankerManifestReportHeader[];
		Items: Garda.CashTrackNG.Core.Contracts.Models.Reporting.TankerManifestReportItem[];
		Footers: Garda.CashTrackNG.Core.Contracts.Models.Reporting.RouteReportFooterResult[];
	}
	interface RouteReportStopResult {
		Address: string;
		Amount: number;
		AreaID: number;
		Barcode: string;
		City: string;
		Coin: string;
		CoinShipmentID: number;
		Commodity: string;
		Date: Date;
		DestinationID: number;
		DisplaySortOrder: number;
		ExcludeStopNoShipment: boolean;
		Key: string;
		MasterStopKey: boolean;
		NewStop: string;
		No: number;
		Order: number;
		PhoneNumber: string;
		Quantity: number;
		Remarks: string;
		RouteID: number;
		SequenceNumber: number;
		ShortDescription: string;
		StopDeletedOn: Date;
		StopID: number;
		StopTypeCode: string;
		UnitBarcode: string;
		UnitID: number;
		UnitKey: boolean;
		UnitName: string;
		IsRedelivery: boolean;
		DatasetNumber: number;
	}
}
declare module CashTrack.ManageItems {
	interface ItemRouteResult {
		ItemID: number;
		RouteID: number;
	}
	interface MovableItemsToOtherRouteResult {
		ID: number;
		RouteTankerBarcode: string;
		Stops: CashTrack.ManageItems.MovableStopResult[];
	}
	interface MovableStopResult extends CashTrack.ManageItems.CheckedInItemTankerResult {
		Index: number;
		StopID: number;
		UnitID: number;
		UnitBarcode: string;
		UnitName: string;
		IsMissingTargetUnitStop: boolean;
		CustomerShipmentID: number;
		InternalSequenceNumber: number;
	}
	interface CheckedInItemTankerResult {
		ID: number;
		Barcode: string;
		Amount: number;
		Quantity: number;
		CurrentTankerID: number;
		CheckedInOn: Date;
		Commodity: CashTrack.Common.CommodityTypeResult;
		Shipper: CashTrack.Common.UnitBriefResult;
		Destination: CashTrack.Common.UnitBriefResult;
		CurrentRouteID?: number;
		BaggedOutItems: CashTrack.ManageItems.BaggedOutItemTankerResult[];
		ItemShipmentStatusCode: string;
		IsATanker: boolean;
		IsABagout: boolean;
		CoinShipmentID: number;
		ScannedCode: string;
		ECashExpirationDate?: Date;
	}
	interface BaggedOutItemTankerResult {
		ID: number;
		Barcode: string;
		Amount: number;
		Quantity: number;
		Commodity: CashTrack.Common.CommodityTypeResult;
		Shipper: CashTrack.Common.UnitBriefResult;
		Destination: CashTrack.Common.UnitBriefResult;
		CheckedInOn: Date;
	}
	interface ItemToExportToExtAreaResult {
		MoneyManagerSite: string;
		BankID: string;
		CustomerID: string;
		Barcode: string;
		TotalCurrency: number;
		PenniesFullBox: number;
		PenniesHalfBox: number;
		PenniesFullBag: number;
		PenniesHalfBag: number;
		PenniesOddRoll: number;
		NickelsFullBox: number;
		NickelsHalfBox: number;
		NickelsFullBag: number;
		NickelsHalfBag: number;
		NickelsOddRoll: number;
		DimesFullBox: number;
		DimesHalfBox: number;
		DimesFullBag: number;
		DimesHalfBag: number;
		DimesOddRoll: number;
		QuartersFullBox: number;
		QuartersHalfBox: number;
		QuartersFullBag: number;
		QuartersHalfBag: number;
		QuartersOddRoll: number;
		HalvesFullBox: number;
		HalvesHalfBox: number;
		HalvesFullBag: number;
		HalvesHalfBag: number;
		HalvesOddRoll: number;
		SBAsFullBox: number;
		SBAsHalfBox: number;
		SBAsFullBag: number;
		SBAsHalfBag: number;
		SBAsOddRoll: number;
		IKESStateQtrsFullBox: number;
		IKESStateQtrsHalfBox: number;
		IKESStateQtrsFullBag: number;
		IKESStateQtrsHalfBag: number;
		IKESStateQtrsOddRoll: number;
		PrepDate: Date;
		DeliveryDate: Date;
		Carrier: string;
		CustomerName: string;
		RouteNumber: string;
		IncludeOddRollCoinInCurrency: string;
	}
	interface VaultTankerMissingResult {
		Barcode: string;
	}
	interface VaultTankerDataResult {
		RemainingItems: CashTrack.ManageItems.CheckedInItemTankerResult[];
		SuggestedRoutes: CashTrack.Common.SuggestedRouteResult[];
		ItemRouteMaps: CashTrack.ManageItems.ItemRouteResult[];
		MissingItems: CashTrack.ManageItems.VaultTankerMissingResult[];
		AllowedCommodities: CashTrack.Common.CommodityResult[];
		AreaTankers: CashTrack.Common.TankerResult[];
		AreaHolidays: CashTrack.Common.HolidayResult[];
	}
	interface RemainingItemTankerResult {
		ID: number;
		Barcode: string;
		Amount: number;
		Quantity: number;
		CurrentTankerID: number;
		CheckedInOn: Date;
		Commodity: CashTrack.Common.CommodityResult;
		Shipper: CashTrack.Common.UnitDestinationResult;
		Destination: CashTrack.Common.UnitDestinationResult;
		SelectedShipperDestination: CashTrack.Common.ShipperDestinationResult;
		BaggedOutItems: CashTrack.ManageItems.BaggedOutItemTankerResult[];
		ItemShipmentStatusCode: string;
		CurrentRoute: CashTrack.Common.SuggestedRouteResult;
		CurrentTanker: CashTrack.Common.TankerResult;
		IsATanker: boolean;
		IsABagout: boolean;
		BagOutTypeCode: string;
		CoinShipmentID: number;
		ScannedCode: string;
		ECashExpirationDate?: Date;
	}
	interface CheckedInCvsItemResult {
		ID: number;
		Barcode: string;
		Amount: number;
		Quantity: number;
		CommodityCategoryType: string;
		CommodityID: number;
		ShipperID: number;
		ShipperBarcode: string;
		ShipperName: string;
		DestinationID: number;
		DestinationBarcode: string;
		DestinationName: string;
		CheckedInOn: Date;
		ItemShipmentStatusCode: string;
		DatedBankSourceTypeID: number;
		CustomerShipmentID: number;
		LabelPrintedOn: Date;
		RouteID: number;
		ToRoute: string;
		ToRouteAMorPMCode: string;
		ToRoutePLannedDate: Date;
		ToRouteCutOffDate: Date;
		TankerID: number;
		ToTankerBarcode: string;
		ToTankerName: string;
		EmployeeID: number;
		EmployeeFirstName: string;
		EmployeeLastName: string;
	}
	interface CheckInByPieceAllowedResult {
		IsCheckInByPieceAllowed: boolean;
	}
	interface ItemInSourceRouteResult {
		RouteID: number;
		RouteNumber: string;
		PlannedDate: Date;
	}
	interface NewItemResult {
		Success: boolean;
		ErrorCode: number;
	}
	interface MoveAndBalanceDataResult {
		CheckedInItems: CashTrack.ManageItems.CheckedInItemTankerResult[];
		RemainingItems: CashTrack.ManageItems.CheckedInItemTankerResult[];
	}
	interface MoveItemsToOtherRouteResult {
		ID: number;
		Success: boolean;
		ErrorCode: number;
	}
	interface UnitDestinationWithSuggestedRoutesResult extends CashTrack.Common.UnitDestinationResult {
		SuggestedRoutes?: CashTrack.Common.SuggestedRouteResult[];
	}
	interface VaultTankerResult {
		ID: number;
		RemainingItems: CashTrack.ManageItems.CheckedInItemTankerResult[];
		SuggestedRoutes: CashTrack.Common.SuggestedRouteResult[];
		ItemRouteMaps: CashTrack.ManageItems.ItemRouteResult[];
		MissingItems: CashTrack.ManageItems.VaultTankerMissingResult[];
	}
	interface CheckInByPieceCountResult {
		Count: number;
	}
	interface CheckInItemResult {
		ID: number;
		ShipperID: number;
	}
	interface RegisterItemByPieceCountResult {
		Success: boolean;
	}
	interface ItemShipmentCheckOutDetailsResult {
		ItemShipmentStatus: CashTrack.Consts.ItemShipmentStatusEnum;
		LastItemTransactionType: CashTrack.Consts.ItemTransactionTypeEnum;
		Item: CashTrack.Common.ItemDetailsResult;
		IsCheckedOut: boolean;
	}
	interface BagOutDetailsResult {
		ID: number;
		NextRouteID: number;
		RouteID: number;
		Commodity: CashTrack.Common.CommodityResult;
		ItemsList: CashTrack.ManageItems.CheckedInItemResult[];
		Barcode: string;
		CreatedOn: Date;
		Area: CashTrack.Common.AreaResult;
		CurrentTanker: CashTrack.Common.TankerResult;
		Errors: CashTrack.Common.ValidationErrorResult[];
		IsMissing: boolean;
	}
	interface CheckedInItemResult {
		ID: number;
		Barcode: string;
		CurrentTankerID?: number;
		Amount: number;
		Quantity: number;
		NextRouteID: number;
		NextRouteNumber: string;
		Commodity: CashTrack.Common.CommodityResult;
		Shipper: CashTrack.Common.UnitDestinationResult;
		Destination: CashTrack.Common.UnitDestinationResult;
		BaggedOutItems: CashTrack.ManageItems.BaggedOutItemResult[];
		CheckedInOn: Date;
		Area: CashTrack.Common.AreaResult;
		IsInMultibank: boolean;
		ItemShipmentStatusCode: string;
		IsABagout: boolean;
		IsATanker: boolean;
		BagoutTypeCode: string;
		ShipperID?: number;
		DestinationID?: number;
		CommodityID?: number;
	}
	interface BaggedOutItemResult {
		ID: number;
		Barcode: string;
		Amount: number;
		Quantity: number;
		Commodity: CashTrack.Common.CommodityResult;
		Shipper: CashTrack.Common.UnitDestinationResult;
		Destination: CashTrack.Common.UnitDestinationResult;
		CheckedInOn: Date;
		BagoutTypeCode: string;
	}
	interface RouteCheckOutStartResult {
		RouteEmployees: CashTrack.Common.RouteEmployeeResult[];
		ShipmentsInfo: CashTrack.ManageItems.CheckedOutItemsInRouteResult;
	}
	interface CheckedOutItemsInRouteResult extends CashTrack.ManageItems.ItemsInRouteResult<CashTrack.ManageItems.CheckedOutItemResult> {
	}
	interface ItemsInRouteResult<T> {
		ItemCount: number;
		CompletedItems: T[];
		RemainingItems: CashTrack.Common.ItemDetailsResult[];
		ExpectedItems: CashTrack.ManageItems.ItemInfo[];
	}
	interface ItemInfo {
		ID: number;
		Item: CashTrack.Common.ItemDetailsResult;
		ShipperDestinations: CashTrack.Common.ShipperDestinationResult[];
		SelectedShipperDestination: CashTrack.Common.ShipperDestinationResult;
		ItemShipment: CashTrack.ManageItems.ItemShipmentResult;
		LastMissingItem: CashTrack.ManageItems.MissingItemResult;
		SelectedShipperDestinationID?: number;
	}
	interface ItemShipmentResult {
		ItemID: number;
		RouteID: number;
		ItemShipmentStatusCode: string;
		IsCheckedIn: boolean;
		LastTransactionID: number;
		IsTransferredToOtherArea: boolean;
		AreaDestinationID: number;
		Route: CashTrack.Common.RouteListResult;
		ItemStatusCode: string;
	}
	interface MissingItemResult {
		ID: number;
		ItemID: number;
		OpenedOn: Date;
		ClosedOn: Date;
		OpenedBy: number;
		ClosedBy: number;
		MissingItemTypeCode: string;
		MissingItemReasonCode: string;
		Comment: string;
		Description: string;
		OpenedByEmployee: CashTrack.Common.EmployeeResult;
	}
	interface CheckedOutItemResult extends CashTrack.Common.ItemDetailsResult {
		CheckedOutOn: Date;
	}
	interface ItemResult {
		ID: number;
		Barcode: string;
		CurrentTankerID: number;
		Commodity: CashTrack.Common.CommodityResult;
		Shipper: CashTrack.Common.ShipperResult;
		Destination: CashTrack.Common.UnitResult;
		Amount: number;
		LastTransaction: CashTrack.Common.ItemTransactionResult;
		LastMissingItem: CashTrack.ManageItems.MissingItemResult;
		IsMissingItemReasonCodeReadOnly: boolean;
		IsInterAreaTransfer: boolean;
		IsCommodityScanned: boolean;
		DestinationAreaID: number;
	}
	interface UpdateAfterBalanceItemsCompletedResult {
		CanRetry: boolean;
		Success: boolean;
		ValidationResults: CashTrack.BusinessRuleValidationResult[];
	}
	interface RegisteredItemResult<T> {
		Success: boolean;
		ErrorCode: string;
		ItemBarcode: string;
		BagOutBarcode: string;
		ShipmentInRoute: CashTrack.ManageItems.ItemsInRouteResult<T>;
	}
	interface ConfirmMissingItemResult {
		Success: boolean;
	}
	interface CheckInItemTankerInfo extends CashTrack.ManageItems.CheckInItemInfoResult {
		Item: CashTrack.Common.ItemDetailsTankerResult;
		MissingItem: CashTrack.Common.MissingItemTankerResult;
		ParentBagout: CashTrack.ManageItems.BagOutDetailsResult;
	}
	interface CheckInItemInfoResult extends CashTrack.ManageItems.ItemInfo {
		SuggestedRoutes: CashTrack.Common.SuggestedRouteResult[];
		NextBusinessDayResult?: Garda.CashTrackNG.Core.Contracts.Models.Common.Results.NextBusinessDayResult;
	}
	interface CheckInTransferItemInfo extends CashTrack.ManageItems.CheckInItemInfoResult {
		Item: CashTrack.Common.ItemDetailsTransferResult;
		MissingItem: CashTrack.ManageItems.MissingItemResult;
		ParentBagout: CashTrack.ManageItems.BagOutDetailsResult;
	}
	interface ItemShipmentInRouteResult {
		ID: number;
		ItemCount: number;
		CheckedInItems: CashTrack.ManageItems.CheckedInItemResult[];
		CheckedInBagouts: CashTrack.ManageItems.CheckedInItemResult[];
		CheckedInBigBagouts: CashTrack.ManageItems.CheckedInItemResult[];
		RemainingItems: CashTrack.Common.ItemDetailsResult[];
		ManifestedItems: CashTrack.ManageItems.CheckInItemInfoResult[];
		RouteTo: CashTrack.Common.RouteResult;
	}
	interface IsCheckInPossibleResult {
		IsCheckInPossible: boolean;
		RouteHasItems: boolean;
	}
	interface IsThereItemsTransferredFromOtherAreaResult {
		IsThereItemsTransferredFromOtherArea: boolean;
	}
	interface ItemShipmentByAreaAndRouteResult {
		ID: number;
	}
	interface MoveAndBalanceItemResult {
		ItemID: number;
		TankerID: number;
	}
	interface RegisterTransferItemResult {
		ItemID: number;
		Success: boolean;
		BarcodeExistsError: boolean;
	}
	interface RegisterTankerItemResult extends CashTrack.ManageItems.RegisterItemResult {
		CheckedInItems: CashTrack.ManageItems.CheckedInItemTankerResult[];
		CheckedInBagouts: CashTrack.ManageItems.CheckedInItemTankerResult[];
		CheckedInBigBagouts: CashTrack.ManageItems.CheckedInItemTankerResult[];
		RemainingItems: CashTrack.ManageItems.CheckedInItemTankerResult[];
		RemainingItemsForCreateBagout: CashTrack.ManageItems.RemainingItemTankerResult[];
	}
	interface RegisterItemResult {
		ID: number;
		Success: boolean;
		ErrorCode: string;
		ItemBarcode: string;
		BagOutBarcode: string;
		CheckedInItem: CashTrack.ManageItems.CheckedInItemResult;
		ShipmentInRoute: CashTrack.ManageItems.ItemShipmentInRouteResult;
	}
	interface TankerCheckInLoadInfoResult {
		CheckedInItems: CashTrack.ManageItems.CheckedInItemTankerResult[];
		CheckedInBagouts: CashTrack.ManageItems.CheckedInItemTankerResult[];
		CheckedInBigBagouts: CashTrack.ManageItems.CheckedInItemTankerResult[];
		CheckedInTankers: CashTrack.ManageItems.CheckedInItemTankerResult[];
		RemainingItems: CashTrack.ManageItems.CheckedInItemTankerResult[];
		RemainingItemsForCreateBagout: CashTrack.ManageItems.RemainingItemTankerResult[];
		AllowedCommodities: CashTrack.Common.CommodityResult[];
		BankCommodities: CashTrack.ManageItems.BankCommodityResult[];
		AreaTankers: CashTrack.Common.TankerResult[];
		AreaHolidays: CashTrack.Common.HolidayResult[];
	}
	interface BankCommodityResult {
		ID: number;
		Bank: CashTrack.Common.UnitResult;
		Commodity: CashTrack.Common.CommodityResult;
	}
	interface ValidateEndMileageResult {
		PreviousMileage: number;
		EndMileage: number;
		ValidationErrors: CashTrack.Common.ValidationErrorResult[];
	}
	interface CheckInListResult {
		ID: number;
		Barcode: string;
		CommodityCode: string;
		CommodityDescription: string;
		CommodityTypeCode: string;
		ShipperBarcode: string;
		ShipperName: string;
		DestinationBarcode: string;
		DestinationName: string;
		Amount: number;
		Quantity: number;
		EmployeeFullName: string;
		EmployeeBarcode: string;
		CreatedOn: Date;
		RouteNumber: string;
		AreaName: string;
		VaultDoorBarcode: string;
		VehicleBarcode: string;
		TotalCount: number;
	}
	interface ItemShipmentCheckInDetailsResult {
		ItemShipmentStatus: CashTrack.Consts.ItemShipmentStatusEnum;
		LastItemTransactionType: CashTrack.Consts.ItemTransactionTypeEnum;
		LastTransactionType: string;
		Item: CashTrack.Common.ItemDetailsResult;
		IsCheckedIn: boolean;
		IsLoadTruck: boolean;
		IsPickup: boolean;
		IsSameDay: boolean;
		IsReturnToSender: boolean;
		IsRedelivery: boolean;
	}
	interface MissingItemInRouteResult {
		MissingItem: CashTrack.Common.MissingItemResult;
		SimilarItem: CashTrack.ManageItems.SimilarItemResult;
	}
	interface SimilarItemResult {
		ID: number;
		Barcode: string;
		Amount: number;
		Quantity: number;
		Commodity: CashTrack.Common.CommodityResult;
		Shipper: CashTrack.Common.UnitResult;
		Destination: CashTrack.Common.UnitResult;
		LastCheckInTransaction: CashTrack.ManageItems.SimilarItemLastTransactionResult;
		LastTransaction: CashTrack.ManageItems.SimilarItemLastTransactionResult;
		MissingItem: CashTrack.Common.MissingItemResult;
	}
	interface SimilarItemLastTransactionResult {
		ID: number;
		CreatedOn: Date;
		Route: CashTrack.Common.RouteResult;
		CreatedBy: CashTrack.Common.EmployeeResult;
	}
	interface RouteCheckInStartResult {
		ID: number;
		BankCommodities: CashTrack.ManageItems.BankCommodityResult[];
		RouteEmployees: CashTrack.Common.RouteEmployeeResult[];
		AllowedCommodities: CashTrack.Common.CommodityResult[];
		ShipmentsInfo: CashTrack.ManageItems.ItemShipmentInRouteResult;
		Shippers?: CashTrack.Common.UnitDestinationResult[];
		Destinations?: CashTrack.ManageItems.UnitDestinationWithSuggestedRoutesResult[];
		ShipmentsList: CashTrack.ManageItems.ItemShipmentCheckInDetailsResult[];
		ExpectedCoins: CashTrack.Common.ItemDetailsResult[];
	}
	interface StartProcessResult {
		Success: boolean;
	}
	interface SimilarItemInfoQuery extends CashTrack.Common.Request {
		AreaID: number;
		CheckInMode: CashTrack.Consts.TankerCheckInMode;
		RouteID: number;
		TankerID: number;
		ItemID: number;
		ShipperID: number;
		DestinationID: number;
		CommodityID: number;
		IsMissing: boolean;
		IsReturnedToOrigin: boolean;
		CurrentTankerID: number;
		BagOutBarcode: string;
		IsABagOut: boolean;
		IsInBagout: boolean;
		IsFlaggedAsReturningToOrigin: boolean;
	}
	interface CheckOutToExtAreaQuery {
		EmployeeID: number;
		RouteID: number;
		ItemID: number;
	}
	interface ExistingCustomerShipmentItemQuery extends CashTrack.Common.Request {
		ItemID: number;
	}
	interface BagoutTankerQuery {
		Barcode: string;
	}
	interface FindStopsForMoveToOtherRouteQuery {
		RouteID: number;
		TankerID: number;
		TargetRouteID: number;
	}
	interface RouteByIDAreaIDQuery extends CashTrack.Common.Request {
		AreaID: number;
		RouteID: number;
	}
	interface VaultTankerDataQuery extends CashTrack.Common.Request {
		AreaID: number;
		TankerID: number;
		NextBusinessDayAM: Date;
		NextBusinessDayPM: Date;
	}
	interface CheckInByPieceAllowedQuery {
		AreaID: number;
	}
	interface CheckInItemsQuery {
		RouteID: number;
	}
	interface CheckedInCvsItemsByRouteIDQuery extends CashTrack.Common.Request {
		AreaID: number;
		RouteID: number;
		Statuses?: CashTrack.Consts.ItemShipmentStatusEnum[];
		PrintedOn: Date;
	}
	interface TankerMissingItemsQuery extends CashTrack.ManageItems.TankerCheckInItemsQuery {
		CheckInMode: CashTrack.Consts.TankerCheckInMode;
	}
	interface TankerCheckInItemsQuery {
		AreaID: number;
		TankerID?: number;
		RouteID?: number;
		BankID?: number;
		TankerType?: CashTrack.Consts.TankerTypeEnum;
		AreaToID?: number;
		CheckInMode?: CashTrack.Consts.TankerCheckInMode;
		CommodityID?: number;
		ScannedCode?: CashTrack.Consts.ItemShipmentScannedCode;
	}
	interface RouteByIDAndAreaIDAndDestinationsQuery {
		AreaID: number;
		RouteID: number;
		DestinationAreaIDs: number[];
	}
	interface ItemInfoQuery extends CashTrack.Common.Request {
		AreaID: number;
		RouteID: number;
		ItemBarcode: string;
	}
	interface BagoutItemsQuery {
		RouteID: number;
		TankerID: number;
		ItemStatus: CashTrack.Consts.ItemStatusEnum[];
	}
	interface ItemShipmentByAreaAndRouteQuery {
		RouteID: number;
		AreaID: number;
	}
	interface TankerCheckInItemInfoQuery extends CashTrack.ManageItems.TankerCheckInLoadInfoQuery {
		ItemBarcode: string;
	}
	interface TankerCheckInLoadInfoQuery extends CashTrack.Common.Request {
		AreaID: number;
		AreaToID?: number;
		TankerID: number;
		BankID?: number;
		RouteID?: number;
		CheckInMode: CashTrack.Consts.TankerCheckInMode;
		CommodityID?: number;
		ScannedCode?: CashTrack.Consts.ItemShipmentScannedCode;
		HoldoverDate?: Date;
	}
	interface MoveAndBalanceItemsQuery {
		RouteID: number;
		TankerID: number;
		ItemStatus: CashTrack.Consts.ItemStatusEnum[];
	}
	interface MoveToOtherRouteItemsQuery {
		RouteID: number;
		TankerID: number;
		ItemStatus: CashTrack.Consts.ItemStatusEnum[];
	}
	interface OpenedRoutesQuery {
		RouteType: CashTrack.Consts.RouteTypeEnum;
		AreaID: number;
	}
	interface TransferCheckinItemInfoQuery extends CashTrack.Common.Request {
		RouteID: number;
		Barcode: string;
		AreaID: number;
	}
	interface TransferShipmentItemsQuery {
		RouteID: number;
	}
	interface ValidateEndMileageQuery {
		RouteID: number;
		VehicleID: number;
		EndMileage: number;
		AreaID: number;
	}
	interface IsCheckInPossibleQuery extends CashTrack.Common.Request {
		RouteID: number;
		IsParcelOrCoinCheckIn: boolean;
	}
	interface CheckInItemInfoQuery extends CashTrack.ManageItems.ItemInfoQuery {
		ExcludeRoutesWithoutTanker?: boolean;
	}
	interface ItemShipmentByIDQuery {
		ItemID: number;
		RouteID: number;
	}
	interface CheckInListQuery extends CashTrack.Common.SearchTableQuery {
		Date: Date;
		AreaId: number;
		Routes: string[];
		Commodities: string[];
	}
	interface ItemShipmentByRouteAdvancedQuery extends CashTrack.ManageItems.ItemShipmentByRouteQuery {
		EmployeeNumber: string;
		EmployeeFirstName: string;
		EmployeeLastName: string;
		BagoutBarcode: string;
		IsExpandBagouts?: boolean;
		WizardMode?: number;
		PrintedOn: Date;
	}
	interface ItemShipmentByRouteQuery extends CashTrack.Common.Request {
		AreaID: number;
		RouteID: number;
		IncludeScannedCommodity?: boolean;
		BankID?: number;
		VaultDoorID?: number;
		IsTransfer?: boolean;
		IsTransferExtArea?: boolean;
		CommodityCode?: string;
		AreaDestinationID?: number;
		NextBusinessDayAM?: Date;
		NextBusinessDayPM?: Date;
		TankerID?: number;
		OpenRouteIDsForRouteMasterRoute?: number[];
	}
	interface ReturningItemQuery {
		ItemID: number;
		RouteID: number;
	}
	interface SimilarInProgressItemQuery {
		AreaID: number;
		Barcode: string;
		ShipperID: number;
	}
	interface SimilarCheckedInItemQuery {
		AreaID: number;
		Barcode: string;
		ShipperID: number;
		ItemID: number;
	}
	interface RegisterCVSItemsCommand extends CashTrack.Common.Request {
		EmployeeID: number;
		RouteID: number;
		VaultDoorID: number;
		CVSItems: CashTrack.ManageItems.CheckedInCvsItemResult[];
	}
	interface RegisterManualPickupCommand extends CashTrack.Common.Request {
		RouteID: number;
		EmployeeID: number;
		VehicleID: number;
		ItemID: number;
		ShipperAreaID: number;
	}
	interface RegisterTankerItemExtCommand extends CashTrack.ManageItems.RegisterTankerItemCommand {
		IsVaultBalanceStartDateUpdated: boolean;
	}
	interface RegisterTankerItemCommand extends CashTrack.Common.Request {
		AreaID: number;
		AreaDestinationID: number;
		EmployeeID: number;
		ItemID: number;
		NextRouteID: number;
		DeliveryRouteID: number;
		RouteID: number;
		BankID: number;
		TankerFromID: number;
		TankerToID: number;
		CheckInMode: CashTrack.Consts.TankerCheckInMode;
		ScannedCode: CashTrack.Consts.ItemShipmentScannedCode;
		Commodity: CashTrack.Common.CommodityResult;
	}
	interface UpdateVaultBalanceCommand extends CashTrack.Common.Request {
		RouteID: number;
		VaultBalanceItemCount?: number;
	}
	interface UpdateVaultBalanceExtCommand extends CashTrack.ManageItems.UpdateVaultBalanceCommand {
		ItemID: number;
	}
	interface CreateItemTransactionBagoutCancelCommand extends CashTrack.Common.CreateTransactionCommandBase {
		To: number;
	}
	interface CreateItemTransactionFromBagoutCommand extends CashTrack.Common.CreateTransactionCommandBase {
		To: number;
		TankerID: number;
	}
	interface StopsItemsCommand {
		ID: number;
		ID1: number;
	}
	interface MoveItemsToOtherRouteCommand extends CashTrack.Common.Request {
		EmployeeID: number;
		FromRouteID: number;
		ToRouteID: number;
		ToRouteTankerID: number;
		Items: number[];
		Stops: number[];
		StopsItems: CashTrack.ManageItems.StopsItemsCommand[];
	}
	interface CreateTransferBagoutCommand extends CashTrack.Common.Request {
		CommodityID: number;
		RouteID: number;
		EmployeeID: number;
		AreaFromID: number;
		AreaToID: number;
		ShipperID: number;
		DestinationID: number;
		TankerID: number;
		ShuttleTankerID: number;
		Barcode: string;
		Items: number[];
		Quantity: number;
		Amount: number;
		BagoutTypeCode: string;
	}
	interface RegisterItemByPieceCountCommand extends CashTrack.Common.Request {
		EmployeeID: number;
		AreaID: number;
		RouteID: number;
		VaultDoorID: number;
	}
	interface UpdateItemShipmentRouteCommand extends CashTrack.Common.Request {
		ItemID: number;
		FromRouteID: number;
		ToRouteID: number;
	}
	interface CreateNewShippableItemCommand extends CashTrack.Common.CreateItemCommand {
		CustomerShipmentID: number;
		DatedBankSourceTypeID: number;
		CurrentTankerID: number;
		LabelPrintedOn: Date;
	}
	interface UpdateCheckOutRouteCommand extends CashTrack.Common.Request {
		RouteID: number;
		ToRouteID: number;
		EmployeeID: number;
		VehicleID: number;
	}
	interface StartCheckOutProcessWithRouteVehicleCommand extends CashTrack.ManageItems.StartCheckOutProcessCommand {
		VehicleID: number;
		EndMileage: number;
		StartMileage: number;
	}
	interface StartCheckOutProcessCommand extends CashTrack.Common.MergeEmployeeToRouteCommand {
		AreaID: number;
		EmployeeID: number;
		IsTransferToExtArea: boolean;
	}
	interface TransferFromCvsCarrierUpdateAfterBalanceItemsCompletedCommand extends CashTrack.ManageItems.UpdateAfterBalanceItemsCompletedCommand {
		ConfirmedErrors: CashTrack.BusinessRuleValidationResult[];
	}
	interface UpdateAfterBalanceItemsCompletedCommand extends CashTrack.Common.Request {
		RouteID: number;
		AreaID: number;
	}
	interface TransferFromOtherAreaRouteUpdateAfterBalanceItemsCompletedCommand extends CashTrack.ManageItems.UpdateAfterBalanceItemsCompletedCommand {
		ConfirmedErrors: CashTrack.BusinessRuleValidationResult[];
	}
	interface OpenBagoutAnotherAreaTankerCommand extends CashTrack.Common.Request {
		ID: number;
		AreaID: number;
		BaggedOutItems: Garda.CashTrackNG.Core.Contracts.Models.ManageItems.Results.BaggedOutItemsResult[];
		RegisterTransferItemCommand: CashTrack.ManageItems.RegisterTransferItemCommand;
		IsRouteCheckIn?: boolean;
	}
	interface RegisterTransferItemCommand extends CashTrack.ManageItems.RegisterItemCommand {
		IsTankerFromAnotherArea?: boolean;
		ShipperID?: number;
		DestinationID?: number;
		CommodityID?: number;
		CustomerShipmentID?: number;
		DatedBankSourceTypeID?: number;
		Barcode?: string;
		LabelPrintedOn?: Date;
	}
	interface RegisterItemCommand extends CashTrack.Common.Request {
		AreaID: number;
		EmployeeID: number;
		ItemID: number;
		NextRouteID?: number;
		RouteID: number;
		VaultDoorID?: number;
		VehicleID?: number;
		BankID?: number;
		TankerID: number;
		ItemTransactionTypeCode?: string;
		CommodityCode?: string;
		DeliveryRouteID?: number;
		DestinationAreaID?: number;
		IsAreaUnit?: boolean;
		IsReturnBagout?: boolean;
		AreaUnitID?: number;
		IsRouteCheckIn?: boolean;
		StopID?: number;
		ItemShipmentStatus_Planned?: string;
		ItemShipmentStatus_CheckedIn?: string;
		ItemTransactionTypeCode_CV?: string;
		ItemTransactonTypeCode_BI?: string;
		IsUpdate?: boolean;
	}
	interface OpenBagoutCommand {
		Barcode: string;
		OpenedOn: Date;
		VehicleID: number;
		TankerID: number;
	}
	interface CancelItemShimpmentCommand extends CashTrack.Common.CreateItemCommand {
		ItemID: number;
	}
	interface UpdateRouteParcelCoinStatusCommand extends CashTrack.Common.Request {
		IsParcelCheckInDone: boolean;
		IsCoinCheckInDone: boolean;
		RouteID: number;
		ActualBalanceStartDateTime: Date;
	}
	interface RouteNotStartedCommand extends CashTrack.Common.Request {
		RouteID: number;
		EmployeeId: number;
	}
	interface RouteNotStartedUpdateItemShipmentCommand {
		RouteID: number;
		ItemShipmentStatusCode: string;
		IsCheckedIn: boolean;
	}
	interface RouteNotStartedUpdateRouteCommand {
		RouteID: number;
		RouteStatusCode: string;
		IsParcelCheckInCompleted: boolean;
		IsCoinCheckInCompleted: boolean;
		IsTransferCheckInCompleted: boolean;
		LastUpdatedBy: number;
		LastUpdatedOn: Date;
	}
	interface CreateItemTransactionToBagoutCommand extends CashTrack.Common.CreateTransactionCommandBase {
		To: number;
		VaultDoorID: number;
		TankerID: number;
	}
	interface ConfirmMissingItemCommand extends CashTrack.Common.Request {
		ItemID: number;
		MissingItemReasonCode: string;
		DestinationID: number;
		EmployeeID: number;
		VehicleID: number;
		RouteID: number;
		VaultDoorID: number;
		AreaID: number;
		Comment: string;
		MissingItemTypeCode?: string;
	}
	interface CreateBagoutCommand extends CashTrack.Common.Request {
		BankID: number;
		CommodityID: number;
		RouteID: number;
		NextRouteID: number;
		EmployeeID: number;
		VaultDoorID: number;
		TankerID: number;
		Barcode: string;
		ItemStatusCode: string;
		ShipperID: number;
		BagoutTypeCode: string;
		Quantity: number;
		Amount: number;
		Items: number[];
	}
	interface CreateBagoutTransactionCommand extends CashTrack.Common.CreateTransactionCommandBase {
		To: number;
		VaultDoorID: number;
		TankerID: number;
	}
	interface ManageAutomaticDestinationCommand {
		AreaID: number;
		RouteID: number;
	}
	interface ManageAutomaticReturnToSenderCommand {
		AreaID: number;
		RouteID: number;
	}
	interface MergeSimilarInProgressItemsCommand extends CashTrack.Common.Request {
		EmployeeID: number;
		ItemID: number;
		RouteID: number;
		CommodityID: number;
		DestinationID: number;
		ShipperID: number;
		Amount: number;
		Quantity: number;
	}
	interface RegisterItemBatchCommand extends CashTrack.Common.Request {
		EmployeeID: number;
		RouteID: number;
		VaultDoorID: number;
		AreaID: number;
		VehicleID: number;
		Items: number[];
	}
	interface MergeSimilarCheckedInItemsCommand {
		SimilarItemID: number;
		OriginalItemID: number;
	}
	interface RegisterManualItemCommand extends CashTrack.Common.Request {
		AreaID: number;
		RouteID: number;
		EmployeeID: number;
		StopID: number;
		VehicleID: number;
		Barcode: string;
		ShipperID: number;
		DestinationID: number;
		CommodityID: number;
		Amount: number;
		Quantity: number;
	}
	interface ReturnToSenderCommand {
		ItemID: number;
	}
	interface SetDefaultDestinationCommand {
		ItemID: number;
		DestinationID: number;
	}
	interface StartProcessCommand extends CashTrack.Common.Request {
		AreaID: number;
		VehicleID?: number;
		RouteID: number;
		IncludeScannedCommodity: boolean;
		EndMileage?: number;
		IsTransfer?: boolean;
	}
	interface NextRouteForECashQuery {
		UnitID: number;
		SourceAreaID: number;
		SourceDate: Date;
	}
	interface TankerForSelectedRouteQuery extends CashTrack.Common.Request {
		SelectedDateTime: Date;
		AreaID: number;
		SelectedRouteID: number;
	}
	interface SuggestedItemsRouteQuery extends CashTrack.Common.Request {
		ItemIds: number[];
		AreaID: number;
		NextBusinessDayAM?: Date;
		NextBusinessDayPM?: Date;
		IsCommodityScanned?: boolean;
		OneRoutePerItem?: boolean;
		OutputItemAndRoute?: boolean;
		UnitDeliveryIdList?: number[];
	}
	interface SuggestedTargetRoutesForMoveItemsQuery extends CashTrack.Common.Request {
		AreaID: number;
		FromRouteID: number;
	}
	interface SuggestedRouteForDestinationQuery extends CashTrack.Common.Request {
		UnitID: number;
		SourceAreaID: number;
	}
	interface SuggestedRouteQuery extends CashTrack.Common.Request {
		UnitID: number;
		ShipperID?: number;
		CommodityID?: number;
		AreaID?: number;
		ExcludeRoutesWithoutTanker?: boolean;
		IsSameDay?: boolean;
		FilterIsDeposit: boolean;
		ManifestedDeliveryRouteID?: number;
		ManifestedDeliveryDate?: Date;
		ManifestedAreaDestination?: number;
	}
}
declare module Garda.CashTrackNG.Core.Contracts.Models.Common.Results {
	interface NextBusinessDayResult {
		NextBusinessDayAM: Date;
		NextBusinessDayPM: Date;
	}
	interface MissingItemBaseResult {
		ID: number;
		Barcode: string;
		Commodity: CashTrack.Common.CommodityResult;
		CommodityType: CashTrack.Common.CommodityTypeResult;
		Shipper: CashTrack.Common.ShipperResult;
		Destination: CashTrack.Common.UnitResult;
		Amount: number;
		MissingItemReasonCode: string;
		ItemStatusCode: string;
		Comment: string;
		CloseDateTime: Date;
		CloseStatus: string;
		Quantity: string;
		OpenedOn: Date;
	}
	interface TransactionResult {
		ID: number;
		ItemID: number;
		Item: CashTrack.Common.ItemResult;
	}
	interface RouteCategoryResult {
		ID: number;
		Code: string;
		Description: string;
	}
	interface WeekDayResult {
		Code: string;
		Description: string;
	}
	interface ContractLineServiceChangeLightResult {
		ID: number;
		EffectiveDate: Date;
		EndDate: Date;
		ServiceFrequencyCode: string;
		ServiceFrequencyDescription: string;
		ServiceDays: Garda.CashTrackNG.Core.Contracts.Models.Common.Results.ServiceDayResult[];
		AreaName: string;
		AreaSiteAdress: string;
		AreaZipPostalCode: string;
		AreaCity: string;
		AreaCountryCode: string;
	}
	interface ServiceDayResult {
		ID: number;
		ContractLineServiceChangeID: number;
		ServiceDayCode: string;
	}
	interface ProductCodeResult {
		ID: number;
		Code: string;
		PrefixAttributeCode: string;
		ProductPrefixCode: string;
		Description: string;
		ServiceType: string;
		IsActive: boolean;
		CITServiceID: number;
	}
	interface UnitWithShipmentsResult extends CashTrack.Common.UnitResult {
		ItemShipments: Garda.CashTrackNG.Core.Contracts.Models.Common.Results.ShippableItemResult[];
	}
	interface ShippableItemResult {
		ID: number;
		Barcode: string;
		OriginalBarcode: string;
		BarcodeChangedBy: number;
		BarcodeChangedOn: Date;
		DestinationID: number;
		ItemStatusCode: string;
		CurrentVehicleID: number;
		CurrentTankerID: number;
		RowVersion: number[];
		ShipperID: number;
		IsCancelled: boolean;
	}
	interface ServiceFrequencyCodeResult {
		Code: string;
		Description: string;
	}
	interface ServiceChangeTypeResult {
		Code: string;
		Description: string;
	}
	interface ContractLineServiceChangeJdeResult {
		ID: number;
		AreaID: number;
		ContractLineID: number;
		EffectiveDate: Date;
		OriginalEffectiveDate: Date;
		EndDate: Date;
		ServiceFrequencyCode: string;
		ServiceChangeTypeCode: string;
		ServiceStartDate: Date;
		Notes: string;
		CreatedOn: Date;
	}
}
declare module Garda.CashTrackNG.Core.Contracts.Models.ManageItems.Results {
	interface BaggedOutItemsResult {
		RouteTankerID: number;
		AMorPM: string;
		PlannedDate: Date;
		ShippableItem: Garda.CashTrackNG.Core.DataAccess.ShippableItem;
	}
}
declare module Garda.CashTrackNG.Core.DataAccess {
	interface ShippableItem {
		ID: number;
		Barcode: string;
		OriginalBarcode: string;
		BarcodeChangedBy: number;
		BarcodeChangedOn: Date;
		DestinationID: number;
		ItemStatusCode: string;
		CurrentVehicleID: number;
		CurrentTankerID: number;
		RowVersion: number[];
		CustomerShipmentID: number;
		DatedBankSourceTypeID: number;
		InputManifestSequenceNumber: number;
		CoinShipmentID: number;
		LabelPrintedOn: Date;
		StagingId: number;
		IsCancelled: boolean;
		HHTID: number;
		CreatedBy: number;
		CreatedOn: Date;
		BagSealNumber: string;
		Employee: Garda.CashTrackNG.Core.DataAccess.Employee;
		ItemShipments: Garda.CashTrackNG.Core.DataAccess.ItemShipment[];
		ItemStatus: Garda.CashTrackNG.Core.DataAccess.ItemStatus;
		ItemTransactions: Garda.CashTrackNG.Core.DataAccess.ItemTransaction[];
		MissingItems: Garda.CashTrackNG.Core.DataAccess.MissingItem[];
		Destination: Garda.CashTrackNG.Core.DataAccess.Unit;
		CurrentTanker: Garda.CashTrackNG.Core.DataAccess.Tanker;
		CurrentVehicle: Garda.CashTrackNG.Core.DataAccess.Vehicle;
		BagOut: Garda.CashTrackNG.Core.DataAccess.BagOut;
		CustomerShipment: Garda.CashTrackNG.Core.DataAccess.CustomerShipment;
		DatedBankSourceType: Garda.CashTrackNG.Core.DataAccess.DatedBankSourceType;
		CoinShipment: Garda.CashTrackNG.Core.DataAccess.CoinShipment;
		MultibankItem: Garda.CashTrackNG.Core.DataAccess.MultibankItem;
		ECashItem: Garda.CashTrackNG.Core.DataAccess.ECashItem;
	}
	interface Employee {
		ID: number;
		Barcode: string;
		FirstName: string;
		MiddleInitial: string;
		LastName: string;
		IsActive: boolean;
		RowVersion: number[];
		FullName: string;
		UserLevelCode: string;
		AreaID: number;
		CreatedOn: Date;
		LastUpdatedOn: Date;
		BarcodeChangedItems: Garda.CashTrackNG.Core.DataAccess.Item[];
		CreatedItemTransactions: Garda.CashTrackNG.Core.DataAccess.ItemTransaction[];
		ClosedMissingItems: Garda.CashTrackNG.Core.DataAccess.MissingItem[];
		OpenedMissingItems: Garda.CashTrackNG.Core.DataAccess.MissingItem[];
		RouteEmployees: Garda.CashTrackNG.Core.DataAccess.RouteEmployee[];
		LastUpdatedShipperDestinations: Garda.CashTrackNG.Core.DataAccess.ShipperDestination[];
		ShippableItems: Garda.CashTrackNG.Core.DataAccess.ShippableItem[];
		ContractLines: Garda.CashTrackNG.Core.DataAccess.ContractLine[];
		ContractLines1: Garda.CashTrackNG.Core.DataAccess.ContractLine[];
		StopServices: Garda.CashTrackNG.Core.DataAccess.StopService[];
		StopServices1: Garda.CashTrackNG.Core.DataAccess.StopService[];
		UserLevel: Garda.CashTrackNG.Core.DataAccess.UserLevel;
		MessengerRouteEvents: Garda.CashTrackNG.Core.DataAccess.RouteEvent[];
		DispatcherRouteEvents: Garda.CashTrackNG.Core.DataAccess.RouteEvent[];
		Area: Garda.CashTrackNG.Core.DataAccess.Area;
	}
	interface Item extends Garda.CashTrackNG.Core.DataAccess.ShippableItem {
		ShipperID: number;
		Quantity: number;
		Amount: number;
		CommodityID: number;
		IsReturnedToOrigin: boolean;
		RowVersion1: number[];
		ExportedDate: Date;
		BarcodeChangedByEmployee: Garda.CashTrackNG.Core.DataAccess.Employee;
		Shipper: Garda.CashTrackNG.Core.DataAccess.Unit;
		Commodity: Garda.CashTrackNG.Core.DataAccess.Commodity;
	}
	interface Unit {
		ID: number;
		UnitTypeCode: string;
		SiteLocationID: number;
		Barcode: string;
		IsActive: boolean;
		Name: string;
		RowVersion: number[];
		CustomerUnitNumber: string;
		Description: string;
		JDEServiceLocationNumber: number;
		SuspendedBeganOn: Date;
		SuspendedEndedOn: Date;
		Latitude: number;
		Longitude: number;
		MissedStopRequiredActionID: number;
		TimeZoneCode: string;
		TimeWindowStartedOn: Date;
		TimeWindowEndedOn: Date;
		ExceptTimeWindowStartedOn: Date;
		ExceptTimeWindowEndedOn: Date;
		OutsourcedReferenceNumber: string;
		MilesFromArea: number;
		CassetteQuantity: number;
		ContactName: string;
		ContactEmail: string;
		ContactPhoneNumber: string;
		IsMixedCommodityAllowed: boolean;
		CVSAreaID: number;
		AlternateDestinationID: number;
		BarcodeCode128b: string;
		CVSBankID: number;
		ItemsShippable: Garda.CashTrackNG.Core.DataAccess.Item[];
		ItemTransactionPickUps: Garda.CashTrackNG.Core.DataAccess.ItemTransactionPickUp[];
		Shippers: Garda.CashTrackNG.Core.DataAccess.ShipperDestination[];
		Destinations: Garda.CashTrackNG.Core.DataAccess.ShipperDestination[];
		SiteLocation: Garda.CashTrackNG.Core.DataAccess.SiteLocation;
		UnitType: Garda.CashTrackNG.Core.DataAccess.UnitType;
		ShippableItems: Garda.CashTrackNG.Core.DataAccess.ShippableItem[];
		ItemTransactionDeliveries: Garda.CashTrackNG.Core.DataAccess.ItemTransactionDelivery[];
		AreaCommodities: Garda.CashTrackNG.Core.DataAccess.AreaCommodity[];
		Stops: Garda.CashTrackNG.Core.DataAccess.Stop[];
		ContractLines: Garda.CashTrackNG.Core.DataAccess.ContractLine[];
		StopServices: Garda.CashTrackNG.Core.DataAccess.StopService[];
		NonRevenueUnits: Garda.CashTrackNG.Core.DataAccess.NonRevenueUnit[];
		MasterStops: Garda.CashTrackNG.Core.DataAccess.MasterStop[];
		BankSourceType: Garda.CashTrackNG.Core.DataAccess.BankSourceType[];
		CustomerShipment: Garda.CashTrackNG.Core.DataAccess.CustomerShipment[];
		Equipment: Garda.CashTrackNG.Core.DataAccess.Equipment[];
		HolidayServiceSolds: Garda.CashTrackNG.Core.DataAccess.HolidayServiceSold[];
		MissedStopRequiredAction: Garda.CashTrackNG.Core.DataAccess.MissedStopRequiredAction;
		Area: Garda.CashTrackNG.Core.DataAccess.Area[];
		TimeZone: Garda.CashTrackNG.Core.DataAccess.TimeZone;
		ItemTransactionCheckIn: Garda.CashTrackNG.Core.DataAccess.ItemTransactionCheckIn[];
		ItemTransactionCheckOut: Garda.CashTrackNG.Core.DataAccess.ItemTransactionCheckOut[];
		CVSArea: Garda.CashTrackNG.Core.DataAccess.CVSArea;
		RouteEvents: Garda.CashTrackNG.Core.DataAccess.RouteEvent[];
		ItemTransactionUnitVehicles: Garda.CashTrackNG.Core.DataAccess.ItemTransactionUnitVehicle[];
		CustomDestinations: Garda.CashTrackNG.Core.DataAccess.CustomDestination[];
		BagOuts: Garda.CashTrackNG.Core.DataAccess.BagOut[];
		SiteDeliveryArea: Garda.CashTrackNG.Core.DataAccess.SiteDeliveryArea[];
		CVSBank: Garda.CashTrackNG.Core.DataAccess.CVSBank;
	}
	interface ItemTransactionPickUp extends Garda.CashTrackNG.Core.DataAccess.ItemTransaction {
		From: number;
		To: number;
		StopID: number;
		IsVerifiedWhenReturningToTheTruck: boolean;
		RowVersion1: number[];
		FromUnit: Garda.CashTrackNG.Core.DataAccess.Unit;
		ToVehicle: Garda.CashTrackNG.Core.DataAccess.Vehicle;
		Stop: Garda.CashTrackNG.Core.DataAccess.Stop;
	}
	interface ItemTransaction {
		ID: number;
		RouteID: number;
		ItemID: number;
		CreatedBy: number;
		CreatedOn: Date;
		ItemTransactionTypeCode: string;
		Comments: string;
		RowVersion: number[];
		HHTID: number;
		CreatedByEmployee: Garda.CashTrackNG.Core.DataAccess.Employee;
		ItemTransactionType: Garda.CashTrackNG.Core.DataAccess.ItemTransactionType;
		Route: Garda.CashTrackNG.Core.DataAccess.Route;
		ItemShipments: Garda.CashTrackNG.Core.DataAccess.ItemShipment[];
		ShippableItem: Garda.CashTrackNG.Core.DataAccess.ShippableItem;
	}
	interface ItemTransactionType {
		Code: string;
		Description: string;
		LongDescription: string;
		IsCoin: boolean;
		IsManual: boolean;
		IsLoadTruck: boolean;
		IsPickup: boolean;
		IsDelivery: boolean;
		IsReturnToSender: boolean;
		IsRedelivery: boolean;
		ItemTransactions: Garda.CashTrackNG.Core.DataAccess.ItemTransaction[];
	}
	interface Route {
		ID: number;
		MasterRouteID: number;
		PlannedDate: Date;
		LoadStartedOn: Date;
		LoadEndedOn: Date;
		DepartedOn: Date;
		ArrivedOn: Date;
		UnloadStartedOn: Date;
		UnloadEndedOn: Date;
		RouteStatusCode: string;
		TankerID: number;
		CutOffDate: Date;
		RowVersion: number[];
		IsParcelCheckInCompleted: boolean;
		IsCoinCheckInCompleted: boolean;
		AreaHolidayDateID: number;
		IsSuspended: boolean;
		CreatedBy: number;
		CreatedOn: Date;
		HHTFileGeneratedOn: Date;
		IsCheckOutCompleted: boolean;
		IsTransferCheckInCompleted: boolean;
		LastUpdatedBy: number;
		LastUpdatedOn: Date;
		OverflowTankerID: number;
		StopDeletedOn: Date;
		MasterRouteDetailID: number;
		ActualUnloadEndDateTime: Date;
		ActualBalanceStartDateTime: Date;
		EndDate: Date;
		VaultBalanceEndDateTime: Date;
		VaultBalanceStartDateTime: Date;
		VaultBalanceItemCount: number;
		ItemShipments: Garda.CashTrackNG.Core.DataAccess.ItemShipment[];
		ItemTransactions: Garda.CashTrackNG.Core.DataAccess.ItemTransaction[];
		ItemTransactionCheckInVehicles: Garda.CashTrackNG.Core.DataAccess.ItemTransactionCheckInVehicle[];
		RouteStatus: Garda.CashTrackNG.Core.DataAccess.RouteStatus;
		Tanker: Garda.CashTrackNG.Core.DataAccess.Tanker;
		RouteEmployees: Garda.CashTrackNG.Core.DataAccess.RouteEmployee[];
		RouteVehicles: Garda.CashTrackNG.Core.DataAccess.RouteVehicle[];
		ItemTransactionCreateBagouts: Garda.CashTrackNG.Core.DataAccess.ItemTransactionCreateBagout[];
		Stops: Garda.CashTrackNG.Core.DataAccess.Stop[];
		AreaHolidayDate: Garda.CashTrackNG.Core.DataAccess.AreaHolidayDate;
		MasterRoute: Garda.CashTrackNG.Core.DataAccess.MasterRoute;
		Tanker1: Garda.CashTrackNG.Core.DataAccess.Tanker;
		MultibankItems: Garda.CashTrackNG.Core.DataAccess.MultibankItem[];
		MasterRouteDetail: Garda.CashTrackNG.Core.DataAccess.MasterRouteDetail;
		RouteEquipments: Garda.CashTrackNG.Core.DataAccess.RouteEquipment[];
		RouteEvents: Garda.CashTrackNG.Core.DataAccess.RouteEvent[];
		ItemTransactionFromBagouts: Garda.CashTrackNG.Core.DataAccess.ItemTransactionFromBagout[];
	}
	interface ItemShipment {
		ItemID: number;
		RouteID: number;
		ItemShipmentStatusCode: string;
		IsCheckedIn: boolean;
		LastTransactionID: number;
		IsTransferredToOtherArea: boolean;
		RowVersion: number[];
		AreaDestinationID: number;
		IsRedelivery: boolean;
		ScannedCode: string;
		ItemShipmentStatus: Garda.CashTrackNG.Core.DataAccess.ItemShipmentStatus;
		Route: Garda.CashTrackNG.Core.DataAccess.Route;
		LastTransaction: Garda.CashTrackNG.Core.DataAccess.ItemTransaction;
		Item: Garda.CashTrackNG.Core.DataAccess.ShippableItem;
		Area: Garda.CashTrackNG.Core.DataAccess.Area;
	}
	interface ItemShipmentStatus {
		Code: string;
		Description: string;
		ItemShipments: Garda.CashTrackNG.Core.DataAccess.ItemShipment[];
	}
	interface Area {
		ID: number;
		Name: string;
		MaxRouteMileage: number;
		RowVersion: number[];
		TimeZoneCode: string;
		IsManagingBranchTanker: boolean;
		IsPieceCountToGardaCVSAllowed: boolean;
		DefaultCSVMasterRouteID: number;
		CurrentSimulationEffectiveDateOn: Date;
		Number: number;
		EffectiveDate: Date;
		EndDate: Date;
		AreaUnitID: number;
		IsManagingOverride: boolean;
		CutOffAreaTime: System.TimeSpan;
		MMSTSiteNumber: string;
		Tankers: Garda.CashTrackNG.Core.DataAccess.Tanker[];
		AreaCommodities: Garda.CashTrackNG.Core.DataAccess.AreaCommodity[];
		VaultDoors: Garda.CashTrackNG.Core.DataAccess.VaultDoor[];
		AreaHolidayDates: Garda.CashTrackNG.Core.DataAccess.AreaHolidayDate[];
		JDEImportLogs: Garda.CashTrackNG.Core.DataAccess.JDEImportLog[];
		AreaBillingDates: Garda.CashTrackNG.Core.DataAccess.AreaBillingDate[];
		HHTImportLogs: Garda.CashTrackNG.Core.DataAccess.HHTImportLog[];
		ContractLineServiceChanges: Garda.CashTrackNG.Core.DataAccess.ContractLineServiceChange[];
		MMImportLogs: Garda.CashTrackNG.Core.DataAccess.MMImportLog[];
		NonRevenueUnits: Garda.CashTrackNG.Core.DataAccess.NonRevenueUnit[];
		MasterRoutes: Garda.CashTrackNG.Core.DataAccess.MasterRoute[];
		MasterRoute: Garda.CashTrackNG.Core.DataAccess.MasterRoute;
		TimeZone: Garda.CashTrackNG.Core.DataAccess.TimeZone;
		BankSourceType: Garda.CashTrackNG.Core.DataAccess.BankSourceType[];
		HHTAreaConfig: Garda.CashTrackNG.Core.DataAccess.HHTAreaConfig;
		StopService: Garda.CashTrackNG.Core.DataAccess.StopService[];
		CVSCarriers: Garda.CashTrackNG.Core.DataAccess.CVSCarrier[];
		ItemShipments: Garda.CashTrackNG.Core.DataAccess.ItemShipment[];
		Unit: Garda.CashTrackNG.Core.DataAccess.Unit;
		SiteDeliveryArea: Garda.CashTrackNG.Core.DataAccess.SiteDeliveryArea[];
		SiteDeliveryArea1: Garda.CashTrackNG.Core.DataAccess.SiteDeliveryArea[];
		BankSourceTypeDeliveryArea: Garda.CashTrackNG.Core.DataAccess.BankSourceTypeDeliveryArea[];
		ECashItem: Garda.CashTrackNG.Core.DataAccess.ECashItem[];
		ECashItem1: Garda.CashTrackNG.Core.DataAccess.ECashItem[];
		Employee: Garda.CashTrackNG.Core.DataAccess.Employee[];
	}
	interface Tanker {
		ID: number;
		Barcode: string;
		AreaID: number;
		Name: string;
		IsActive: boolean;
		TankerTypeID: number;
		RowVersion: number[];
		Area: Garda.CashTrackNG.Core.DataAccess.Area;
		ItemTransactionLoadVehicles: Garda.CashTrackNG.Core.DataAccess.ItemTransactionLoadVehicle[];
		Routes: Garda.CashTrackNG.Core.DataAccess.Route[];
		ShippableItems: Garda.CashTrackNG.Core.DataAccess.ShippableItem[];
		MasterRouteDetails: Garda.CashTrackNG.Core.DataAccess.MasterRouteDetail[];
		ItemTransactionCheckInVehicle: Garda.CashTrackNG.Core.DataAccess.ItemTransactionCheckInVehicle[];
		ItemTransactionCreateBagout: Garda.CashTrackNG.Core.DataAccess.ItemTransactionCreateBagout[];
		TankerType: Garda.CashTrackNG.Core.DataAccess.TankerType;
		BagOuts: Garda.CashTrackNG.Core.DataAccess.BagOut[];
		MasterRouteDetails1: Garda.CashTrackNG.Core.DataAccess.MasterRouteDetail[];
		Routes1: Garda.CashTrackNG.Core.DataAccess.Route[];
		ItemTransactionInternalMove: Garda.CashTrackNG.Core.DataAccess.ItemTransactionInternalMove[];
		ItemTransactionInternalMove1: Garda.CashTrackNG.Core.DataAccess.ItemTransactionInternalMove[];
		MultibankItems: Garda.CashTrackNG.Core.DataAccess.MultibankItem[];
		ItemTransactionCheckIn: Garda.CashTrackNG.Core.DataAccess.ItemTransactionCheckIn[];
		ItemTransactionCheckOut: Garda.CashTrackNG.Core.DataAccess.ItemTransactionCheckOut[];
		ItemTransactionToBagouts: Garda.CashTrackNG.Core.DataAccess.ItemTransactionToBagout[];
		ItemTransactionFromBagouts: Garda.CashTrackNG.Core.DataAccess.ItemTransactionFromBagout[];
	}
	interface ItemTransactionLoadVehicle extends Garda.CashTrackNG.Core.DataAccess.ItemTransaction {
		From: number;
		To: number;
		RowVersion1: number[];
		FromTanker: Garda.CashTrackNG.Core.DataAccess.Tanker;
		ToVehicle: Garda.CashTrackNG.Core.DataAccess.Vehicle;
	}
	interface Vehicle {
		ID: number;
		Barcode: string;
		IsActive: boolean;
		RowVersion: number[];
		InitialMileage: number;
		IsUnitOfMeasureByMile: boolean;
		Make: string;
		Model: string;
		NumberOfKeys: number;
		VehicleIdentificationNumber: string;
		Year: number;
		ItemTransactionCheckInVehicles: Garda.CashTrackNG.Core.DataAccess.ItemTransactionCheckInVehicle[];
		ItemTransactionLoadVehicles: Garda.CashTrackNG.Core.DataAccess.ItemTransactionLoadVehicle[];
		ItemTransactionPickUps: Garda.CashTrackNG.Core.DataAccess.ItemTransactionPickUp[];
		RouteVehicles: Garda.CashTrackNG.Core.DataAccess.RouteVehicle[];
		Equipments: Garda.CashTrackNG.Core.DataAccess.Equipment[];
		ShippableItems: Garda.CashTrackNG.Core.DataAccess.ShippableItem[];
		ItemTransactionDeliveries: Garda.CashTrackNG.Core.DataAccess.ItemTransactionDelivery[];
		Equipment: Garda.CashTrackNG.Core.DataAccess.Equipment[];
		ItemTransactionUnitVehicles: Garda.CashTrackNG.Core.DataAccess.ItemTransactionUnitVehicle[];
	}
	interface ItemTransactionCheckInVehicle extends Garda.CashTrackNG.Core.DataAccess.ItemTransaction {
		From: number;
		To: number;
		VaultDoorID: number;
		ToTankerID: number;
		RowVersion1: number[];
		FromVehicle: Garda.CashTrackNG.Core.DataAccess.Vehicle;
		ToRoute: Garda.CashTrackNG.Core.DataAccess.Route;
		VaultDoor: Garda.CashTrackNG.Core.DataAccess.VaultDoor;
		Tanker: Garda.CashTrackNG.Core.DataAccess.Tanker;
	}
	interface VaultDoor {
		ID: number;
		Barcode: string;
		AreaID: number;
		IsSameDayDoor: boolean;
		Description: string;
		IsActive: boolean;
		RowVersion: number[];
		Area: Garda.CashTrackNG.Core.DataAccess.Area;
		ItemTransactionCheckInVehicles: Garda.CashTrackNG.Core.DataAccess.ItemTransactionCheckInVehicle[];
		ItemTransactionCreateBagouts: Garda.CashTrackNG.Core.DataAccess.ItemTransactionCreateBagout[];
		ItemTransactionDeliveries: Garda.CashTrackNG.Core.DataAccess.ItemTransactionDelivery[];
		ItemTransactionToBagouts: Garda.CashTrackNG.Core.DataAccess.ItemTransactionToBagout[];
		ItemTransactionCheckIn: Garda.CashTrackNG.Core.DataAccess.ItemTransactionCheckIn[];
		MultibankItems: Garda.CashTrackNG.Core.DataAccess.MultibankItem[];
	}
	interface ItemTransactionCreateBagout extends Garda.CashTrackNG.Core.DataAccess.ItemTransaction {
		From: number;
		To: number;
		VaultDoorID: number;
		TankerID: number;
		RowVersion1: number[];
		BagOut: Garda.CashTrackNG.Core.DataAccess.BagOut;
		RouteTo: Garda.CashTrackNG.Core.DataAccess.Route;
		VaultDoor: Garda.CashTrackNG.Core.DataAccess.VaultDoor;
		Tanker: Garda.CashTrackNG.Core.DataAccess.Tanker;
	}
	interface BagOut extends Garda.CashTrackNG.Core.DataAccess.ShippableItem {
		CommodityID: number;
		BagoutOpenedOn: Date;
		ShuttleTankerID: number;
		RowVersion1: number[];
		ShipperID: number;
		BagoutTypeCode: string;
		Quantity: number;
		Amount: number;
		ItemTransactionToBagouts: Garda.CashTrackNG.Core.DataAccess.ItemTransactionToBagout[];
		ItemTransactionCreateBagouts: Garda.CashTrackNG.Core.DataAccess.ItemTransactionCreateBagout[];
		Commodity: Garda.CashTrackNG.Core.DataAccess.Commodity;
		Tanker: Garda.CashTrackNG.Core.DataAccess.Tanker;
		MultibankItems: Garda.CashTrackNG.Core.DataAccess.MultibankItem[];
		ItemTransactionFromBagouts: Garda.CashTrackNG.Core.DataAccess.ItemTransactionFromBagout[];
		ItemTransactionBagoutCancels: Garda.CashTrackNG.Core.DataAccess.ItemTransactionBagoutCancel[];
		BagoutType: Garda.CashTrackNG.Core.DataAccess.BagoutType;
		Shipper: Garda.CashTrackNG.Core.DataAccess.Unit;
	}
	interface ItemTransactionToBagout extends Garda.CashTrackNG.Core.DataAccess.ItemTransaction {
		From: number;
		To: number;
		VaultDoorID: number;
		RowVersion1: number[];
		Tanker: Garda.CashTrackNG.Core.DataAccess.Tanker;
		BagOut: Garda.CashTrackNG.Core.DataAccess.BagOut;
		VaultDoor: Garda.CashTrackNG.Core.DataAccess.VaultDoor;
	}
	interface Commodity {
		ID: number;
		CommodityTypeCode: string;
		Description: string;
		IsCheckInAllowed: boolean;
		IsEmergency: boolean;
		UnitAmount: number;
		Code: string;
		BillingLineComment: string;
		IsCheckItem: boolean;
		BarcodeSuffix: string;
		IsCommodityDisplayedOnHHT: boolean;
		IsVerified: boolean;
		QuickKey: number;
		ShortDescription: string;
		HHTSortOrder: number;
		DisplaySortOrder: number;
		CommodityCategoryID: number;
		AmountFlag: boolean;
		AreaCommodities: Garda.CashTrackNG.Core.DataAccess.AreaCommodity[];
		BagOuts: Garda.CashTrackNG.Core.DataAccess.BagOut[];
		CommodityType: Garda.CashTrackNG.Core.DataAccess.CommodityType;
		Items: Garda.CashTrackNG.Core.DataAccess.Item[];
		ShortAttributeCodes: Garda.CashTrackNG.Core.DataAccess.ShortAttributeCode[];
		BankSourceType: Garda.CashTrackNG.Core.DataAccess.BankSourceType[];
		ShipperDestination: Garda.CashTrackNG.Core.DataAccess.ShipperDestination[];
		CoinRelations: Garda.CashTrackNG.Core.DataAccess.CoinRelation[];
		CommodityCategory: Garda.CashTrackNG.Core.DataAccess.CommodityCategory;
	}
	interface AreaCommodity {
		AreaID: number;
		CommodityID: number;
		DefaultDestinationID: number;
		IsRouted: boolean;
		IsActive: boolean;
		IsCommodityScanned: boolean;
		RowVersion: number[];
		Area: Garda.CashTrackNG.Core.DataAccess.Area;
		Commodity: Garda.CashTrackNG.Core.DataAccess.Commodity;
		DefaultDestinationUnit: Garda.CashTrackNG.Core.DataAccess.Unit;
	}
	interface CommodityType {
		Code: string;
		Description: string;
		HHTCommodityTypeCode: string;
		Commodities: Garda.CashTrackNG.Core.DataAccess.Commodity[];
		CommodityType1: Garda.CashTrackNG.Core.DataAccess.CommodityType[];
		CommodityType2: Garda.CashTrackNG.Core.DataAccess.CommodityType;
	}
	interface ShortAttributeCode {
		ID: number;
		Code: string;
		Description: string;
		UnitOfMeasureCode: string;
		VariableBillingCode: string;
		IsSeasonalService: boolean;
		IsAppliedToConjunctiveService: boolean;
		CommodityIdToBill: number;
		BillingLineComment: string;
		ShortDescriptionBillingType: string;
		IsActive: boolean;
		ServiceOrExtra: string;
		CITAttributeDescription: string;
		IsFixedDayService: boolean;
		CITAttributeServices: Garda.CashTrackNG.Core.DataAccess.CITAttributeService[];
		Commodity: Garda.CashTrackNG.Core.DataAccess.Commodity;
		UnitOfMeasure: Garda.CashTrackNG.Core.DataAccess.UnitOfMeasure;
		VariableBillingCodes: Garda.CashTrackNG.Core.DataAccess.VariableBillingCode[];
		Attributes: Garda.CashTrackNG.Core.DataAccess.Attribute[];
		VariableBillingCode1: Garda.CashTrackNG.Core.DataAccess.VariableBillingCode;
		ProductCodes: Garda.CashTrackNG.Core.DataAccess.ProductCode[];
		RelationCITServiceExtra: Garda.CashTrackNG.Core.DataAccess.RelationCITServiceExtra[];
	}
	interface CITAttributeService {
		ID: number;
		AttributeId: number;
		ServiceTypeCode: string;
		IsSameDay: boolean;
		IsPickupService: boolean;
		IsDeliveryService: boolean;
		ProductPrefixCode: string;
		CITServiceID: number;
		PerTripOrPerHour: string;
		ServiceType: Garda.CashTrackNG.Core.DataAccess.ServiceType;
		ShortAttributeCode: Garda.CashTrackNG.Core.DataAccess.ShortAttributeCode;
		CITService: Garda.CashTrackNG.Core.DataAccess.CITService;
		StopVariableServices: Garda.CashTrackNG.Core.DataAccess.StopVariableService[];
	}
	interface ServiceType {
		Code: string;
		Description: string;
		CITAttributeServices: Garda.CashTrackNG.Core.DataAccess.CITAttributeService[];
		ProductCodes: Garda.CashTrackNG.Core.DataAccess.ProductCode[];
	}
	interface ProductCode {
		ID: number;
		Code: string;
		Description: string;
		PrefixAttributeCode: string;
		ProductPrefixCode: string;
		ServiceType: string;
		IsActive: boolean;
		DefaultShortAttributeCode: number;
		CITServiceID: number;
		ServiceType1: Garda.CashTrackNG.Core.DataAccess.ServiceType;
		ContractLines: Garda.CashTrackNG.Core.DataAccess.ContractLine[];
		ShortAttributeCode: Garda.CashTrackNG.Core.DataAccess.ShortAttributeCode;
		CITService: Garda.CashTrackNG.Core.DataAccess.CITService;
	}
	interface ContractLine {
		ID: number;
		ContractID: number;
		UnitID: number;
		ServiceFrequencyCode: string;
		ProductCodeID: number;
		ProductBillingCode: string;
		TriPartyCode: string;
		ContractLineNumber: number;
		EffectiveDate: Date;
		EndDate: Date;
		LiabilityTreshold: number;
		PremiseTimeThreshold: number;
		ItemThreshold: number;
		IsActive: boolean;
		CreatedBy: number;
		CreatedOn: Date;
		LastUpdatedBy: number;
		LastUpdatedOn: Date;
		RowVersion: number[];
		ServiceChangeStartedOn: Date;
		Description: string;
		IsCheckLiabilityBilled: boolean;
		Attributes: Garda.CashTrackNG.Core.DataAccess.Attribute[];
		Contract: Garda.CashTrackNG.Core.DataAccess.Contract;
		Employee: Garda.CashTrackNG.Core.DataAccess.Employee;
		Employee1: Garda.CashTrackNG.Core.DataAccess.Employee;
		ProductBillingCode1: Garda.CashTrackNG.Core.DataAccess.ProductBillingCode;
		TriPartyCode1: Garda.CashTrackNG.Core.DataAccess.TriPartyCode;
		ProductCode: Garda.CashTrackNG.Core.DataAccess.ProductCode;
		ServiceFrequencyCode1: Garda.CashTrackNG.Core.DataAccess.ServiceFrequencyCode;
		Unit: Garda.CashTrackNG.Core.DataAccess.Unit;
		JDEImportLogs: Garda.CashTrackNG.Core.DataAccess.JDEImportLog[];
		ContractLineServiceChanges: Garda.CashTrackNG.Core.DataAccess.ContractLineServiceChange[];
		ContractLineNextPlannedServiceDates: Garda.CashTrackNG.Core.DataAccess.ContractLineNextPlannedServiceDate[];
	}
	interface Attribute {
		ID: number;
		ContractLineID: number;
		AttributeBillingCode: string;
		ShortAttributeCodeID: number;
		IsActive: boolean;
		CreatedBy: number;
		CreatedOn: Date;
		LastUpdatedBy: number;
		LastUpdatedOn: Date;
		RowVersion: number[];
		ShortAttributeCode: Garda.CashTrackNG.Core.DataAccess.ShortAttributeCode;
		ContractLine: Garda.CashTrackNG.Core.DataAccess.ContractLine;
		VariableBillingLines: Garda.CashTrackNG.Core.DataAccess.VariableBillingLine[];
		StopVariableServices: Garda.CashTrackNG.Core.DataAccess.StopVariableService[];
		StopExtraFees: Garda.CashTrackNG.Core.DataAccess.StopExtraFee[];
	}
	interface VariableBillingLine {
		ID: number;
		StopID: number;
		BillingLineStatus: string;
		AttributeID: number;
		AttributeBillingCode: string;
		Quantity: number;
		RequestType: string;
		Comment: string;
		CustomerAuthorizationNumber: string;
		RowVersion: number[];
		Attribute: Garda.CashTrackNG.Core.DataAccess.Attribute;
		BillingLineStatu: Garda.CashTrackNG.Core.DataAccess.BillingLineStatus;
		Stop: Garda.CashTrackNG.Core.DataAccess.Stop;
	}
	interface BillingLineStatus {
		Code: string;
		Description: string;
		VariableBillingLines: Garda.CashTrackNG.Core.DataAccess.VariableBillingLine[];
	}
	interface Stop {
		ID: number;
		RouteID: number;
		UnitID: number;
		StartedOn: Date;
		EndedOn: Date;
		CustomerStartedOn: Date;
		CustomerEndedOn: Date;
		IsManual: boolean;
		IsCancelled: boolean;
		SequenceNumber: number;
		RowVersion: number[];
		MasterStopID: number;
		DeliverySignatureID: number;
		PickupSignatureID: number;
		DeliverySignature: string;
		PickupSignature: string;
		InternalSequenceNumber: number;
		CreatedBy: number;
		CreatedOn: Date;
		LastUpdatedBy: number;
		LastUpdatedOn: Date;
		HHTID: number;
		ItemTransactionDeliveries: Garda.CashTrackNG.Core.DataAccess.ItemTransactionDelivery[];
		ItemTransactionPickUps: Garda.CashTrackNG.Core.DataAccess.ItemTransactionPickUp[];
		MissedStop: Garda.CashTrackNG.Core.DataAccess.MissedStop;
		Route: Garda.CashTrackNG.Core.DataAccess.Route;
		Unit: Garda.CashTrackNG.Core.DataAccess.Unit;
		StopBillingInfo: Garda.CashTrackNG.Core.DataAccess.StopBillingInfo;
		NoPickup: Garda.CashTrackNG.Core.DataAccess.NoPickup;
		StopServices: Garda.CashTrackNG.Core.DataAccess.StopService[];
		VariableBillingLines: Garda.CashTrackNG.Core.DataAccess.VariableBillingLine[];
		StopExtraFees: Garda.CashTrackNG.Core.DataAccess.StopExtraFee[];
		MasterStop: Garda.CashTrackNG.Core.DataAccess.MasterStop;
		Signature2: Garda.CashTrackNG.Core.DataAccess.Signature;
		Signature3: Garda.CashTrackNG.Core.DataAccess.Signature;
		MissedStops: Garda.CashTrackNG.Core.DataAccess.MissedStop[];
	}
	interface ItemTransactionDelivery extends Garda.CashTrackNG.Core.DataAccess.ItemTransaction {
		From: number;
		To: number;
		VaultDoorID: number;
		StopID: number;
		RowVersion1: number[];
		Vehicle: Garda.CashTrackNG.Core.DataAccess.Vehicle;
		Unit: Garda.CashTrackNG.Core.DataAccess.Unit;
		VaultDoor: Garda.CashTrackNG.Core.DataAccess.VaultDoor;
		Stop: Garda.CashTrackNG.Core.DataAccess.Stop;
	}
	interface MissedStop {
		StopID: number;
		MissedStopReasonCode: string;
		Comments: string;
		CreatedOn: Date;
		RowVersion: number[];
		RescheduledStopID: number;
		MissedStopReason: Garda.CashTrackNG.Core.DataAccess.MissedStopReason;
		Stop: Garda.CashTrackNG.Core.DataAccess.Stop;
		Stop11: Garda.CashTrackNG.Core.DataAccess.Stop;
	}
	interface MissedStopReason {
		Code: string;
		IsGardaResponsibility: boolean;
		Description: string;
		HHTCommodityCode: string;
		HHTCommodityDescription: string;
		IsActive: boolean;
		IsHHTCommodity: boolean;
		HHTSortOrder: number;
		MissedStops: Garda.CashTrackNG.Core.DataAccess.MissedStop[];
		NoPickups: Garda.CashTrackNG.Core.DataAccess.NoPickup[];
	}
	interface NoPickup {
		StopID: number;
		NoPickupOn: Date;
		NoPickupBy: number;
		RowVersion: number[];
		ReasonCode: string;
		Comments: string;
		Stop: Garda.CashTrackNG.Core.DataAccess.Stop;
		MissedStopReason: Garda.CashTrackNG.Core.DataAccess.MissedStopReason;
	}
	interface StopBillingInfo {
		ID: number;
		StopBillingStatus: number;
		IsAllStopServiceReadyForBilling: boolean;
		IsBillable: boolean;
		IsManuallyBilled: boolean;
		RowVersion: number[];
		Stop: Garda.CashTrackNG.Core.DataAccess.Stop;
		StopBillingStatu: Garda.CashTrackNG.Core.DataAccess.StopBillingStatus;
	}
	interface StopBillingStatus {
		ID: number;
		Description: string;
		Code: string;
		StopBillingInfoes: Garda.CashTrackNG.Core.DataAccess.StopBillingInfo[];
	}
	interface StopService {
		ID: number;
		StopID: number;
		UnitID: number;
		CreatedBy: number;
		CreatedOn: Date;
		LastUpdatedBy: number;
		LastUpdatedOn: Date;
		StopServiceBillingTypeID: number;
		AreaID: number;
		CITServiceID: number;
		MasterStopID: number;
		IsCancelled: boolean;
		RowVersion: number[];
		Employee: Garda.CashTrackNG.Core.DataAccess.Employee;
		Employee1: Garda.CashTrackNG.Core.DataAccess.Employee;
		Stop: Garda.CashTrackNG.Core.DataAccess.Stop;
		Unit: Garda.CashTrackNG.Core.DataAccess.Unit;
		StopServiceBillingType: Garda.CashTrackNG.Core.DataAccess.StopServiceBillingType;
		StopVariableServices: Garda.CashTrackNG.Core.DataAccess.StopVariableService[];
		StopVariableServices1: Garda.CashTrackNG.Core.DataAccess.StopVariableService[];
		StopVariableService: Garda.CashTrackNG.Core.DataAccess.StopVariableService;
		Area: Garda.CashTrackNG.Core.DataAccess.Area;
		CITService: Garda.CashTrackNG.Core.DataAccess.CITService;
		MasterStop: Garda.CashTrackNG.Core.DataAccess.MasterStop;
	}
	interface StopServiceBillingType {
		ID: number;
		Description: string;
		Code: string;
		StopServices: Garda.CashTrackNG.Core.DataAccess.StopService[];
	}
	interface StopVariableService extends Garda.CashTrackNG.Core.DataAccess.StopService {
		CITAttributeServiceID: number;
		AttributeID: number;
		NonBillableReasonCode: number;
		RelatedStopServiceID1: number;
		RelatedStopServiceID2: number;
		QuantityToBill: System.TimeSpan;
		CustomerAuthorizationNumber: string;
		BillingComment: string;
		CaseNumber: string;
		OperationDescription: string;
		RequestedOn: Date;
		PlannedServiceDate: Date;
		PlannedServiceTimeStart: System.TimeSpan;
		PlannedServiceTimeEnd: System.TimeSpan;
		RowVersion1: number[];
		Attribute: Garda.CashTrackNG.Core.DataAccess.Attribute;
		CITAttributeService: Garda.CashTrackNG.Core.DataAccess.CITAttributeService;
		NonBillableReasonCode1: Garda.CashTrackNG.Core.DataAccess.NonBillableReasonCode;
		StopService: Garda.CashTrackNG.Core.DataAccess.StopService;
		StopService1: Garda.CashTrackNG.Core.DataAccess.StopService;
		StopService2: Garda.CashTrackNG.Core.DataAccess.StopService;
	}
	interface NonBillableReasonCode {
		ID: number;
		Description: string;
		IsSelectedInDashboard: boolean;
		IsVariableServiceRequired: boolean;
		Code: string;
		StopVariableServices: Garda.CashTrackNG.Core.DataAccess.StopVariableService[];
	}
	interface CITService {
		ID: number;
		ServiceCode: string;
		ServiceDescription: string;
		ServiceOrExtra: string;
		CITAttributeServices: Garda.CashTrackNG.Core.DataAccess.CITAttributeService[];
		NonRevenueUnit: Garda.CashTrackNG.Core.DataAccess.NonRevenueUnit[];
		ProductCode: Garda.CashTrackNG.Core.DataAccess.ProductCode[];
		RelationCITServiceExtra: Garda.CashTrackNG.Core.DataAccess.RelationCITServiceExtra[];
		StopService: Garda.CashTrackNG.Core.DataAccess.StopService[];
	}
	interface NonRevenueUnit {
		ID: number;
		AreaID: number;
		UnitID: number;
		EffectiveDate: Date;
		EndDate: Date;
		IsActive: boolean;
		IsAreaUnit: boolean;
		CreatedBy: number;
		CreatedOn: Date;
		LastUpdatedBy: number;
		LastUpdatedOn: Date;
		CITServiceID: number;
		RowVersion: number[];
		Area: Garda.CashTrackNG.Core.DataAccess.Area;
		Unit: Garda.CashTrackNG.Core.DataAccess.Unit;
		CITService: Garda.CashTrackNG.Core.DataAccess.CITService;
		NonRevenueServiceDays: Garda.CashTrackNG.Core.DataAccess.NonRevenueServiceDay[];
	}
	interface NonRevenueServiceDay {
		ID: number;
		NonRevenueUnitID: number;
		ServiceDayCode: string;
		RowVersion: number[];
		DayCode: Garda.CashTrackNG.Core.DataAccess.DayCode;
		NonRevenueUnit: Garda.CashTrackNG.Core.DataAccess.NonRevenueUnit;
	}
	interface DayCode {
		Code: string;
		Description: string;
		DayNumber: number;
		RouteCategoryCode: string;
		MasterRouteDetails: Garda.CashTrackNG.Core.DataAccess.MasterRouteDetail[];
		ServiceDays: Garda.CashTrackNG.Core.DataAccess.ServiceDay[];
		RouteCategory: Garda.CashTrackNG.Core.DataAccess.RouteCategory;
		NonRevenueServiceDays: Garda.CashTrackNG.Core.DataAccess.NonRevenueServiceDay[];
		CustomDestinations: Garda.CashTrackNG.Core.DataAccess.CustomDestination[];
		SiteDeliveryServiceDays: Garda.CashTrackNG.Core.DataAccess.SiteDeliveryServiceDay[];
	}
	interface MasterRouteDetail {
		ID: number;
		MasterRouteVersionID: number;
		ServiceDayCode: string;
		CutoffRouteTimeOn: System.TimeSpan;
		BeginRouteTimeOn: System.TimeSpan;
		EndRouteTimeOn: System.TimeSpan;
		DefaultTankerID: number;
		AreaHolidayDateID: number;
		RouteCategoryCode: string;
		IsActive: boolean;
		FirstStopRecordID: number;
		LastStopRecordID: number;
		CreatedBy: number;
		CreatedOn: Date;
		LastUpdatedBy: number;
		LastUpdatedOn: Date;
		SuspendedBegunOn: Date;
		SuspendedEndedOn: Date;
		Duration: number;
		OverflowTankerID: number;
		RowVersion: number[];
		AreaHolidayDate: Garda.CashTrackNG.Core.DataAccess.AreaHolidayDate;
		MasterRouteVersion: Garda.CashTrackNG.Core.DataAccess.MasterRouteVersion;
		MasterStop: Garda.CashTrackNG.Core.DataAccess.MasterStop;
		MasterStop1: Garda.CashTrackNG.Core.DataAccess.MasterStop;
		RouteCategory: Garda.CashTrackNG.Core.DataAccess.RouteCategory;
		Tanker: Garda.CashTrackNG.Core.DataAccess.Tanker;
		MasterStops: Garda.CashTrackNG.Core.DataAccess.MasterStop[];
		DayCode: Garda.CashTrackNG.Core.DataAccess.DayCode;
		Tanker1: Garda.CashTrackNG.Core.DataAccess.Tanker;
		Routes: Garda.CashTrackNG.Core.DataAccess.Route[];
	}
	interface AreaHolidayDate {
		ID: number;
		HolidayCode: string;
		AreaID: number;
		HolidayDate: Date;
		IsHolidayAM: boolean;
		IsHolidayPM: boolean;
		RowVersion: number[];
		Area: Garda.CashTrackNG.Core.DataAccess.Area;
		HolidayCode1: Garda.CashTrackNG.Core.DataAccess.HolidayCode;
		Routes: Garda.CashTrackNG.Core.DataAccess.Route[];
		MasterRouteDetails: Garda.CashTrackNG.Core.DataAccess.MasterRouteDetail[];
		MasterStops: Garda.CashTrackNG.Core.DataAccess.MasterStop[];
	}
	interface HolidayCode {
		Description: string;
		ShortDescriptionBillingType: string;
		Code: string;
		AreaHolidayDates1: Garda.CashTrackNG.Core.DataAccess.AreaHolidayDate[];
		HolidayServiceSolds: Garda.CashTrackNG.Core.DataAccess.HolidayServiceSold[];
	}
	interface HolidayServiceSold {
		ID: number;
		UnitID: number;
		HolidayCode: string;
		ReportCode: string;
		IsHolidayServiced: boolean;
		IsHolidayBilled: boolean;
		IsReportHolidayBilled: boolean;
		RowVersion: number[];
		HolidayCode1: Garda.CashTrackNG.Core.DataAccess.HolidayCode;
		HolidayReportCode: Garda.CashTrackNG.Core.DataAccess.HolidayReportCode;
		Unit: Garda.CashTrackNG.Core.DataAccess.Unit;
	}
	interface HolidayReportCode {
		Code: string;
		Description: string;
		HolidayServiceSolds: Garda.CashTrackNG.Core.DataAccess.HolidayServiceSold[];
	}
	interface MasterStop {
		ID: number;
		MasterRouteDetailID: number;
		UnitID: number;
		PreviousMasterStopID: number;
		NextMasterStopID: number;
		StopTypeCode: string;
		IsDepositStop: boolean;
		IsShipmentStop: boolean;
		ServiceDateBegunOn: Date;
		ServiceDateEndedOn: Date;
		EndedTemporaryBegunOn: Date;
		EndedTemporaryEndedOn: Date;
		IsKeyRequired: boolean;
		IsDeliveredOnSameRoute: boolean;
		IsBilled: boolean;
		CreatedBy: number;
		CreatedOn: Date;
		LastUpdatedBy: number;
		LastUpdatedOn: Date;
		OperationComment: string;
		IsDeliveryStop: boolean;
		InternalSequenceNumber: number;
		RowVersion: number[];
		AreaHolidayDateID: number;
		RelatedMasterStopID: number;
		MasterStop1: Garda.CashTrackNG.Core.DataAccess.MasterStop[];
		MasterStop2: Garda.CashTrackNG.Core.DataAccess.MasterStop;
		MasterStop11: Garda.CashTrackNG.Core.DataAccess.MasterStop[];
		MasterStop3: Garda.CashTrackNG.Core.DataAccess.MasterStop;
		StopType: Garda.CashTrackNG.Core.DataAccess.StopType;
		Unit: Garda.CashTrackNG.Core.DataAccess.Unit;
		Stops: Garda.CashTrackNG.Core.DataAccess.Stop[];
		MasterRouteDetails: Garda.CashTrackNG.Core.DataAccess.MasterRouteDetail[];
		MasterRouteDetails1: Garda.CashTrackNG.Core.DataAccess.MasterRouteDetail[];
		MasterRouteDetail: Garda.CashTrackNG.Core.DataAccess.MasterRouteDetail;
		StopService: Garda.CashTrackNG.Core.DataAccess.StopService[];
		MasterStopRescheduleService: Garda.CashTrackNG.Core.DataAccess.MasterStopRescheduleService[];
		MasterStopRescheduleService1: Garda.CashTrackNG.Core.DataAccess.MasterStopRescheduleService[];
		AreaHolidayDate: Garda.CashTrackNG.Core.DataAccess.AreaHolidayDate;
	}
	interface StopType {
		Code: string;
		Description: string;
		MasterStops: Garda.CashTrackNG.Core.DataAccess.MasterStop[];
		RouteTypeAndStopType: Garda.CashTrackNG.Core.DataAccess.RouteTypeAndStopType[];
	}
	interface RouteTypeAndStopType {
		ID: number;
		RouteTypeCode: string;
		StopTypeCode: string;
		RouteType: Garda.CashTrackNG.Core.DataAccess.RouteType;
		StopType: Garda.CashTrackNG.Core.DataAccess.StopType;
	}
	interface RouteType {
		Code: string;
		Description: string;
		IsServiceByTruck: boolean;
		IsDisplayedInMasterRouteDashboard: boolean;
		IsDailyGenerated: boolean;
		IsInternalRoute: boolean;
		IsSimulated: boolean;
		MasterRoutes: Garda.CashTrackNG.Core.DataAccess.MasterRoute[];
		RouteTypeAndStopTypes: Garda.CashTrackNG.Core.DataAccess.RouteTypeAndStopType[];
	}
	interface MasterRoute {
		ID: number;
		AreaID: number;
		RouteTypeCode: string;
		AMorPMCode: string;
		RouteNumber: string;
		RouteNumberCode128b: string;
		Name: string;
		IsHHTRoute: boolean;
		NumberOfCrew: number;
		IsHHTPrinterRequired: boolean;
		IsSpecialRoute: boolean;
		LastGeneratedDailyRouteDateOn: Date;
		RowVersion: number[];
		IsOverDaysRoute: boolean;
		ODRDaysQty: number;
		IsTransferIn: boolean;
		IsTransferOut: boolean;
		IsDefaultSource: boolean;
		AMorPMRoute: Garda.CashTrackNG.Core.DataAccess.AMorPMRoute;
		Area: Garda.CashTrackNG.Core.DataAccess.Area;
		MasterRouteVersions: Garda.CashTrackNG.Core.DataAccess.MasterRouteVersion[];
		Routes: Garda.CashTrackNG.Core.DataAccess.Route[];
		RouteType: Garda.CashTrackNG.Core.DataAccess.RouteType;
		Areas: Garda.CashTrackNG.Core.DataAccess.Area[];
		BankSourceType: Garda.CashTrackNG.Core.DataAccess.BankSourceType[];
	}
	interface AMorPMRoute {
		Code: string;
		Description: string;
		MasterRoutes: Garda.CashTrackNG.Core.DataAccess.MasterRoute[];
		HHTBeginDayProcessing: Garda.CashTrackNG.Core.DataAccess.HHTBeginDayProcessing[];
	}
	interface HHTBeginDayProcessing {
		ID: number;
		AMorPMCode: string;
		LastProcessingOn: Date;
		PlannedProcessingHour: System.TimeSpan;
		TimeZoneCode: string;
		AMorPMRoute: Garda.CashTrackNG.Core.DataAccess.AMorPMRoute;
		TimeZone: Garda.CashTrackNG.Core.DataAccess.TimeZone;
	}
	interface TimeZone {
		Code: string;
		Description: string;
		JDETimeZoneCode: string;
		Areas: Garda.CashTrackNG.Core.DataAccess.Area[];
		HHTBeginDayProcessing: Garda.CashTrackNG.Core.DataAccess.HHTBeginDayProcessing[];
		Unit: Garda.CashTrackNG.Core.DataAccess.Unit[];
	}
	interface MasterRouteVersion {
		ID: number;
		MasterRouteID: number;
		EffectiveDateOn: Date;
		EndDateOn: Date;
		IsSimulationRouteVersion: boolean;
		RowVersion: number[];
		MasterRoute: Garda.CashTrackNG.Core.DataAccess.MasterRoute;
		MasterRouteDetails: Garda.CashTrackNG.Core.DataAccess.MasterRouteDetail[];
	}
	interface BankSourceType {
		ID: number;
		AreaID: number;
		DefaultCommodityID: number;
		IsActive: boolean;
		IsCODPayment: boolean;
		LongDescription: string;
		Number: number;
		ShipperUnitID: number;
		ShortDescription: string;
		SourceMasterRouteID: number;
		SourceTypeID: number;
		Name: string;
		EffectiveDate: Date;
		EndDate: Date;
		IsBankSourcePrintedOnPOD: boolean;
		IsCoinInputByAmount: boolean;
		CoinPackageSizeID: number;
		ReportDescription: string;
		RowVersion: number[];
		UseCustomList: boolean;
		IsEcashSource: boolean;
		OddCoinID: number;
		ECashExpirationDays: number;
		Area: Garda.CashTrackNG.Core.DataAccess.Area;
		Commodity: Garda.CashTrackNG.Core.DataAccess.Commodity;
		MasterRoute: Garda.CashTrackNG.Core.DataAccess.MasterRoute;
		Unit: Garda.CashTrackNG.Core.DataAccess.Unit;
		CoinPackageSize: Garda.CashTrackNG.Core.DataAccess.CoinPackageSize;
		CoinInputSequences: Garda.CashTrackNG.Core.DataAccess.CoinInputSequence[];
		CVSBankSources: Garda.CashTrackNG.Core.DataAccess.CVSBankSource[];
		DatedBankSourceTypes: Garda.CashTrackNG.Core.DataAccess.DatedBankSourceType[];
		SourceType: Garda.CashTrackNG.Core.DataAccess.SourceType;
		CustomDestinations: Garda.CashTrackNG.Core.DataAccess.CustomDestination[];
		BankSourceTypeDeliveryArea: Garda.CashTrackNG.Core.DataAccess.BankSourceTypeDeliveryArea[];
	}
	interface CoinPackageSize {
		ID: number;
		Code: string;
		Description: string;
		DenominationGroupCode: string;
		BankSourceTypes: Garda.CashTrackNG.Core.DataAccess.BankSourceType[];
		CoinRelations: Garda.CashTrackNG.Core.DataAccess.CoinRelation[];
	}
	interface CoinRelation {
		ID: number;
		CoinPackageSizeID: number;
		CoinDenominationID: number;
		CommodityID: number;
		CoinDenomination: Garda.CashTrackNG.Core.DataAccess.CoinDenomination;
		CoinPackageSize: Garda.CashTrackNG.Core.DataAccess.CoinPackageSize;
		Commodity: Garda.CashTrackNG.Core.DataAccess.Commodity;
	}
	interface CoinDenomination {
		ID: number;
		Code: string;
		Description: string;
		CoinInputSequences: Garda.CashTrackNG.Core.DataAccess.CoinInputSequence[];
		CoinRelations: Garda.CashTrackNG.Core.DataAccess.CoinRelation[];
	}
	interface CoinInputSequence {
		ID: number;
		CoinDenominationID: number;
		BankSourceTypeID: number;
		SequenceNumber: number;
		RowVersion: number[];
		BankSourceType: Garda.CashTrackNG.Core.DataAccess.BankSourceType;
		CoinDenomination: Garda.CashTrackNG.Core.DataAccess.CoinDenomination;
	}
	interface CVSBankSource {
		ID: number;
		CVSAreaNumber: string;
		CVSBankNumber: string;
		BankSourceTypeID: number;
		RowVersion: number[];
		DenominationGroupCode: string;
		BankSourceType: Garda.CashTrackNG.Core.DataAccess.BankSourceType;
	}
	interface DatedBankSourceType {
		ID: number;
		CreatedBy: number;
		CreatedOn: Date;
		InputDateTime: Date;
		InputFileName: string;
		LastUpdatedBy: number;
		LastUpdatedOn: Date;
		BankSourceTypeID: number;
		RowVersion: number[];
		ShipmentSourceID: number;
		PreparedOn: Date;
		DeliveryOn: Date;
		StagingId: number;
		ShippableItem: Garda.CashTrackNG.Core.DataAccess.ShippableItem[];
		BankSourceType: Garda.CashTrackNG.Core.DataAccess.BankSourceType;
		ShipmentSource: Garda.CashTrackNG.Core.DataAccess.ShipmentSource;
	}
	interface ShipmentSource {
		ID: number;
		Code: string;
		Description: string;
		CustomerShipments: Garda.CashTrackNG.Core.DataAccess.CustomerShipment[];
		DatedBankSourceTypes: Garda.CashTrackNG.Core.DataAccess.DatedBankSourceType[];
	}
	interface CustomerShipment {
		ID: number;
		DeliveredOn: Date;
		DestinationUnitID: number;
		InitialDeliveryDate: Date;
		IsCODPayment: boolean;
		PreparedOn: Date;
		ShipmentSourceID: number;
		RowVersion: number[];
		StagingId: number;
		Unit: Garda.CashTrackNG.Core.DataAccess.Unit;
		ShippableItem: Garda.CashTrackNG.Core.DataAccess.ShippableItem[];
		ShipmentSource: Garda.CashTrackNG.Core.DataAccess.ShipmentSource;
	}
	interface SourceType {
		ID: number;
		Code: string;
		Description: string;
		BankSourceTypes: Garda.CashTrackNG.Core.DataAccess.BankSourceType[];
	}
	interface CustomDestination {
		Id: number;
		BankSourceTypeID: number;
		DestinationUnitID: number;
		DeliveryDayCode: string;
		BankSourceType: Garda.CashTrackNG.Core.DataAccess.BankSourceType;
		DayCode: Garda.CashTrackNG.Core.DataAccess.DayCode;
		Unit: Garda.CashTrackNG.Core.DataAccess.Unit;
	}
	interface BankSourceTypeDeliveryArea {
		ID: number;
		BankSourceTypeID: number;
		AreaID: number;
		Area: Garda.CashTrackNG.Core.DataAccess.Area;
		BankSourceType: Garda.CashTrackNG.Core.DataAccess.BankSourceType;
	}
	interface MasterStopRescheduleService {
		ID: number;
		NewMasterStopID: number;
		PreviousMasterStopID: number;
		PreviousPlannedDate: Date;
		MasterStop: Garda.CashTrackNG.Core.DataAccess.MasterStop;
		MasterStop1: Garda.CashTrackNG.Core.DataAccess.MasterStop;
	}
	interface RouteCategory {
		Code: string;
		Description: string;
		MasterRouteDetails: Garda.CashTrackNG.Core.DataAccess.MasterRouteDetail[];
		DayCode: Garda.CashTrackNG.Core.DataAccess.DayCode[];
	}
	interface ServiceDay {
		ID: number;
		ContractLineServiceChangeID: number;
		ServiceDayCode: string;
		ContractLineServiceChange: Garda.CashTrackNG.Core.DataAccess.ContractLineServiceChange;
		DayCode: Garda.CashTrackNG.Core.DataAccess.DayCode;
	}
	interface ContractLineServiceChange {
		ID: number;
		ContractLineID: number;
		EffectiveDate: Date;
		EndDate: Date;
		AreaID: number;
		ServiceFrequencyCode: string;
		ServiceChangeTypeCode: string;
		ServiceStartDate: Date;
		Notes: string;
		CreatedOn: Date;
		LastUpdatedOn: Date;
		RowVersion: number[];
		OriginalEffectiveDate: Date;
		Area: Garda.CashTrackNG.Core.DataAccess.Area;
		ContractLine: Garda.CashTrackNG.Core.DataAccess.ContractLine;
		ServiceChangeType: Garda.CashTrackNG.Core.DataAccess.ServiceChangeType;
		ServiceFrequencyCode1: Garda.CashTrackNG.Core.DataAccess.ServiceFrequencyCode;
		ServiceDays: Garda.CashTrackNG.Core.DataAccess.ServiceDay[];
	}
	interface ServiceChangeType {
		Code: string;
		Description: string;
		ContractLineServiceChanges: Garda.CashTrackNG.Core.DataAccess.ContractLineServiceChange[];
	}
	interface ServiceFrequencyCode {
		Description: string;
		Code: string;
		Frequency: number;
		ContractLines: Garda.CashTrackNG.Core.DataAccess.ContractLine[];
		ContractLineServiceChanges: Garda.CashTrackNG.Core.DataAccess.ContractLineServiceChange[];
	}
	interface SiteDeliveryServiceDay {
		ID: number;
		SiteDeliveryAreaID: number;
		ServiceDayCode: string;
		SiteDeliveryArea: Garda.CashTrackNG.Core.DataAccess.SiteDeliveryArea;
		DayCode: Garda.CashTrackNG.Core.DataAccess.DayCode;
	}
	interface SiteDeliveryArea {
		ID: number;
		SourceAreaID: number;
		UnitID: number;
		DeliveryAreaID: number;
		EffectiveDate: Date;
		EndDate: Date;
		Area: Garda.CashTrackNG.Core.DataAccess.Area;
		Area1: Garda.CashTrackNG.Core.DataAccess.Area;
		Unit: Garda.CashTrackNG.Core.DataAccess.Unit;
		SiteDeliveryServiceDay: Garda.CashTrackNG.Core.DataAccess.SiteDeliveryServiceDay[];
	}
	interface RelationCITServiceExtra {
		ID: number;
		CITServiceID: number;
		ShortAttributeCodeID: number;
		IsExtraServiceFee: boolean;
		IsTransportationFee: boolean;
		AttributeBillingCode: string;
		ConjunctiveServiceCode: string;
		CITService: Garda.CashTrackNG.Core.DataAccess.CITService;
		ShortAttributeCode: Garda.CashTrackNG.Core.DataAccess.ShortAttributeCode;
	}
	interface StopExtraFee {
		ID: number;
		StopID: number;
		AttributeID: number;
		VariableBillingCode: string;
		QuantityToBill: number;
		IsExcessBilled: boolean;
		Threshold: number;
		PickedSum: number;
		DeliverySum: number;
		RowVersion: number[];
		Attribute: Garda.CashTrackNG.Core.DataAccess.Attribute;
		Stop: Garda.CashTrackNG.Core.DataAccess.Stop;
		VariableBillingCode1: Garda.CashTrackNG.Core.DataAccess.VariableBillingCode;
	}
	interface VariableBillingCode {
		Code: string;
		Description: string;
		BillingType: string;
		DefaultAttributeCode: number;
		ShortAttributeCode: Garda.CashTrackNG.Core.DataAccess.ShortAttributeCode;
		ShortAttributeCodes: Garda.CashTrackNG.Core.DataAccess.ShortAttributeCode[];
		StopExtraFees: Garda.CashTrackNG.Core.DataAccess.StopExtraFee[];
	}
	interface Signature {
		ID: number;
		Image: number[];
		RowVersion: number[];
		FileName: string;
		Stop2: Garda.CashTrackNG.Core.DataAccess.Stop[];
		Stop3: Garda.CashTrackNG.Core.DataAccess.Stop[];
	}
	interface Contract {
		ID: number;
		CustomerID: number;
		Number: number;
		Description: string;
		RowVersion: number[];
		Version: number;
		Type: string;
		IsActive: boolean;
		CreatedBy: number;
		CreatedOn: Date;
		LastUpdatedBy: number;
		LastUpdatedOn: Date;
		ConjunctiveServiceCode: string;
		IsAuthorizationNumberRequired: boolean;
		Customer: Garda.CashTrackNG.Core.DataAccess.Customer;
		ExtranetUsers: Garda.CashTrackNG.Core.DataAccess.ExtranetUser[];
		ContractLines: Garda.CashTrackNG.Core.DataAccess.ContractLine[];
		ConjunctiveServiceCode1: Garda.CashTrackNG.Core.DataAccess.ConjunctiveServiceCode;
	}
	interface Customer {
		ID: number;
		Name: string;
		Number: number;
		IsSuspended: boolean;
		ParentID: number;
		RowVersion: number[];
		Contracts: Garda.CashTrackNG.Core.DataAccess.Contract[];
		ChildCustomers: Garda.CashTrackNG.Core.DataAccess.Customer[];
		ParentCustomer: Garda.CashTrackNG.Core.DataAccess.Customer;
		ExtranetUsers: Garda.CashTrackNG.Core.DataAccess.ExtranetUser[];
	}
	interface ExtranetUser {
		ID: number;
		FirstName: string;
		LastName: string;
		Email: string;
		IsActive: boolean;
		Username: string;
		LastLogin: Date;
		IsLocked: boolean;
		PhoneNumber: string;
		PhoneNumberExtension: string;
		RowVersion: number[];
		CreatedBy: number;
		CreatedOn: Date;
		LastUpdatedBy: number;
		LastUpdatedOn: Date;
		Contracts: Garda.CashTrackNG.Core.DataAccess.Contract[];
		Customers: Garda.CashTrackNG.Core.DataAccess.Customer[];
	}
	interface ConjunctiveServiceCode {
		Code: string;
		Description: string;
		IsConjunctiveService: boolean;
		Contracts: Garda.CashTrackNG.Core.DataAccess.Contract[];
	}
	interface ProductBillingCode {
		Code: string;
		AlternateCode: string;
		Description: string;
		ContractLines: Garda.CashTrackNG.Core.DataAccess.ContractLine[];
	}
	interface TriPartyCode {
		Code: string;
		PartnerCode: string;
		Customer: string;
		Description: string;
		ContractLines: Garda.CashTrackNG.Core.DataAccess.ContractLine[];
	}
	interface JDEImportLog {
		ID: number;
		CreatedOn: Date;
		AreaID: number;
		JDEImportStatusCode: string;
		ContractNumber: number;
		ContractVersion: number;
		ContractType: string;
		ContractLineNumber: number;
		AttributeBillingCode: string;
		ContractLineID: number;
		Area: Garda.CashTrackNG.Core.DataAccess.Area;
		JDEErrorEvents: Garda.CashTrackNG.Core.DataAccess.JDEErrorEvent[];
		JDEImportStatu: Garda.CashTrackNG.Core.DataAccess.JDEImportStatu;
		JDESuccessEvents: Garda.CashTrackNG.Core.DataAccess.JDESuccessEvent[];
		ContractLine: Garda.CashTrackNG.Core.DataAccess.ContractLine;
	}
	interface JDEErrorEvent {
		ID: number;
		ImportLogID: number;
		RecordTypeCode: string;
		InvalidValue: string;
		RecordInfo: string;
		ErrorEventTypeCode: number;
		JDEImportLog: Garda.CashTrackNG.Core.DataAccess.JDEImportLog;
		RecordType: Garda.CashTrackNG.Core.DataAccess.RecordType;
		ErrorEventType: Garda.CashTrackNG.Core.DataAccess.ErrorEventType;
	}
	interface RecordType {
		Code: string;
		Description: string;
		JDEErrorEvents: Garda.CashTrackNG.Core.DataAccess.JDEErrorEvent[];
		HHTErrorEvents: Garda.CashTrackNG.Core.DataAccess.HHTErrorEvent[];
		MMErrorEvents: Garda.CashTrackNG.Core.DataAccess.MMErrorEvent[];
		HHTImportLog: Garda.CashTrackNG.Core.DataAccess.HHTImportLog[];
		MMImportLog: Garda.CashTrackNG.Core.DataAccess.MMImportLog[];
	}
	interface HHTErrorEvent {
		ID: number;
		ImportLogID: number;
		RecordTypeCode: string;
		InvalidValue: string;
		RecordInfo: string;
		ErrorEventTypeCode: number;
		HHTImportLog: Garda.CashTrackNG.Core.DataAccess.HHTImportLog;
		RecordType: Garda.CashTrackNG.Core.DataAccess.RecordType;
		ErrorEventType: Garda.CashTrackNG.Core.DataAccess.ErrorEventType;
	}
	interface HHTImportLog {
		ID: number;
		CreatedOn: Date;
		AreaID: number;
		RouteNumber: string;
		StopNumber: string;
		ItemBarcode: string;
		VehicleNumber: string;
		ErrorEventTypeCode: number;
		InvalidValue: string;
		RecordInfo: string;
		RecordTypeCode: string;
		Area: Garda.CashTrackNG.Core.DataAccess.Area;
		HHTErrorEvents: Garda.CashTrackNG.Core.DataAccess.HHTErrorEvent[];
		ErrorEventType: Garda.CashTrackNG.Core.DataAccess.ErrorEventType;
		RecordType: Garda.CashTrackNG.Core.DataAccess.RecordType;
	}
	interface ErrorEventType {
		Code: number;
		Description: string;
		MessageType: string;
		ErrorField: string;
		HHTErrorEvents: Garda.CashTrackNG.Core.DataAccess.HHTErrorEvent[];
		JDEErrorEvents: Garda.CashTrackNG.Core.DataAccess.JDEErrorEvent[];
		MMErrorEvents: Garda.CashTrackNG.Core.DataAccess.MMErrorEvent[];
		HHTImportLog: Garda.CashTrackNG.Core.DataAccess.HHTImportLog[];
		MMImportLog: Garda.CashTrackNG.Core.DataAccess.MMImportLog[];
	}
	interface MMErrorEvent {
		ID: number;
		ImportLogID: number;
		RecordTypeCode: string;
		InvalidValue: string;
		RecordInfo: string;
		ErrorEventTypeCode: number;
		MMImportLog: Garda.CashTrackNG.Core.DataAccess.MMImportLog;
		RecordType: Garda.CashTrackNG.Core.DataAccess.RecordType;
		ErrorEventType: Garda.CashTrackNG.Core.DataAccess.ErrorEventType;
	}
	interface MMImportLog {
		ID: number;
		CreatedOn: Date;
		AreaID: number;
		CSVAreaNumber: string;
		CSVBankNumber: string;
		CSVCustomerNumber: string;
		DeliveryDate: Date;
		ErrorEventTypeCode: number;
		InvalidValue: string;
		RecordInfo: string;
		RecordTypeCode: string;
		Area: Garda.CashTrackNG.Core.DataAccess.Area;
		MMErrorEvents: Garda.CashTrackNG.Core.DataAccess.MMErrorEvent[];
		ErrorEventType: Garda.CashTrackNG.Core.DataAccess.ErrorEventType;
		RecordType: Garda.CashTrackNG.Core.DataAccess.RecordType;
	}
	interface JDEImportStatu {
		Code: string;
		Description: string;
		JDEImportLogs: Garda.CashTrackNG.Core.DataAccess.JDEImportLog[];
	}
	interface JDESuccessEvent {
		ID: number;
		ImportLogID: number;
		PreviousValue: string;
		NewValue: string;
		JDESuccessEventTypeCode: string;
		ServiceChangeStartedOn: Date;
		ServiceChangeEndedOn: Date;
		JDEImportLog: Garda.CashTrackNG.Core.DataAccess.JDEImportLog;
		JDESuccessEventType: Garda.CashTrackNG.Core.DataAccess.JDESuccessEventType;
	}
	interface JDESuccessEventType {
		Code: string;
		Description: string;
		JDESuccessEvents: Garda.CashTrackNG.Core.DataAccess.JDESuccessEvent[];
	}
	interface ContractLineNextPlannedServiceDate {
		ID: number;
		NextPlannedServiceDate: Date;
		RowVersion: number[];
		ContractLine: Garda.CashTrackNG.Core.DataAccess.ContractLine;
	}
	interface UnitOfMeasure {
		Code: string;
		Description: string;
		ShortAttributeCodes: Garda.CashTrackNG.Core.DataAccess.ShortAttributeCode[];
	}
	interface ShipperDestination {
		ShipperID: number;
		DestinationID: number;
		CommodityID: number;
		LastUpdatedBy: number;
		LastUpdatedOn: Date;
		EffectiveDate: Date;
		EndDate: Date;
		IsSameDay: boolean;
		RowVersion: number[];
		LastUpdatedByEmployee: Garda.CashTrackNG.Core.DataAccess.Employee;
		DestinationUnit: Garda.CashTrackNG.Core.DataAccess.Unit;
		ShipperUnit: Garda.CashTrackNG.Core.DataAccess.Unit;
		Commodity: Garda.CashTrackNG.Core.DataAccess.Commodity;
	}
	interface CommodityCategory {
		ID: number;
		Code: string;
		Description: string;
		Commodities: Garda.CashTrackNG.Core.DataAccess.Commodity[];
	}
	interface MultibankItem {
		ItemID: number;
		BagOutID: number;
		VaultDoorID: number;
		TankerID: number;
		RouteID: number;
		BagOut: Garda.CashTrackNG.Core.DataAccess.BagOut;
		Route: Garda.CashTrackNG.Core.DataAccess.Route;
		Tanker: Garda.CashTrackNG.Core.DataAccess.Tanker;
		VaultDoor: Garda.CashTrackNG.Core.DataAccess.VaultDoor;
		ShippableItem: Garda.CashTrackNG.Core.DataAccess.ShippableItem;
	}
	interface ItemTransactionFromBagout extends Garda.CashTrackNG.Core.DataAccess.ItemTransaction {
		From: number;
		To: number;
		ToTankerID: number;
		RowVersion1: number[];
		BagOut: Garda.CashTrackNG.Core.DataAccess.BagOut;
		RouteTo: Garda.CashTrackNG.Core.DataAccess.Route;
		Tanker: Garda.CashTrackNG.Core.DataAccess.Tanker;
	}
	interface ItemTransactionBagoutCancel extends Garda.CashTrackNG.Core.DataAccess.ItemTransaction {
		From: number;
		To: number;
		RowVersion1: number[];
		BagOut: Garda.CashTrackNG.Core.DataAccess.BagOut;
	}
	interface BagoutType {
		Code: string;
		Description: string;
		BagOuts: Garda.CashTrackNG.Core.DataAccess.BagOut[];
	}
	interface ItemTransactionCheckIn extends Garda.CashTrackNG.Core.DataAccess.ItemTransaction {
		From: number;
		To: number;
		VaultDoorID: number;
		RowVersion1: number[];
		VaultDoor: Garda.CashTrackNG.Core.DataAccess.VaultDoor;
		Unit: Garda.CashTrackNG.Core.DataAccess.Unit;
		Tanker: Garda.CashTrackNG.Core.DataAccess.Tanker;
	}
	interface RouteVehicle {
		ID: number;
		RouteID: number;
		VehicleID: number;
		StartMileage: number;
		EndMileage: number;
		RowVersion: number[];
		Route: Garda.CashTrackNG.Core.DataAccess.Route;
		Vehicle: Garda.CashTrackNG.Core.DataAccess.Vehicle;
	}
	interface Equipment {
		ID: number;
		Barcode: string;
		EquipmentTypeCode: string;
		IsActive: boolean;
		SiteLocationID: number;
		UnitID: number;
		VehicleID: number;
		RowVersion: number[];
		EquipmentType: Garda.CashTrackNG.Core.DataAccess.EquipmentType;
		Vehicles: Garda.CashTrackNG.Core.DataAccess.Vehicle[];
		SiteLocation: Garda.CashTrackNG.Core.DataAccess.SiteLocation;
		Unit: Garda.CashTrackNG.Core.DataAccess.Unit;
		Vehicle: Garda.CashTrackNG.Core.DataAccess.Vehicle;
		RouteEquipments: Garda.CashTrackNG.Core.DataAccess.RouteEquipment[];
	}
	interface EquipmentType {
		Code: string;
		Description: string;
		Equipments: Garda.CashTrackNG.Core.DataAccess.Equipment[];
	}
	interface SiteLocation {
		ID: number;
		CityID: number;
		Address1: string;
		Address2: string;
		Address3: string;
		SearchAddress: string;
		ZipPostalCode: string;
		RowVersion: number[];
		Number: number;
		City: Garda.CashTrackNG.Core.DataAccess.City;
		Units: Garda.CashTrackNG.Core.DataAccess.Unit[];
		Equipment: Garda.CashTrackNG.Core.DataAccess.Equipment[];
	}
	interface City {
		ID: number;
		SubdivisionCountryCode: string;
		Name: string;
		RowVersion: number[];
		SubdivisionCountry: Garda.CashTrackNG.Core.DataAccess.SubdivisionCountry;
		SiteLocations: Garda.CashTrackNG.Core.DataAccess.SiteLocation[];
	}
	interface SubdivisionCountry {
		Code: string;
		CountryCode: string;
		RowVersion: number[];
		Cities: Garda.CashTrackNG.Core.DataAccess.City[];
		Country: Garda.CashTrackNG.Core.DataAccess.Country;
	}
	interface Country {
		Code: string;
		RowVersion: number[];
		SubdivisionCountries: Garda.CashTrackNG.Core.DataAccess.SubdivisionCountry[];
	}
	interface RouteEquipment {
		RouteID: number;
		EquipmentID: number;
		RowVersion: number[];
		CheckedOutOn: Date;
		CheckedOutBy: number;
		Equipment: Garda.CashTrackNG.Core.DataAccess.Equipment;
		Route: Garda.CashTrackNG.Core.DataAccess.Route;
	}
	interface ItemTransactionUnitVehicle extends Garda.CashTrackNG.Core.DataAccess.ItemTransaction {
		From: number;
		To: number;
		RowVersion1: number[];
		Unit: Garda.CashTrackNG.Core.DataAccess.Unit;
		Vehicle: Garda.CashTrackNG.Core.DataAccess.Vehicle;
	}
	interface TankerType {
		Code: string;
		Description: string;
		ID: number;
		IsUpdatedByTankerFunction: boolean;
		IsItemWithinTanker: boolean;
		Tanker: Garda.CashTrackNG.Core.DataAccess.Tanker[];
	}
	interface ItemTransactionInternalMove extends Garda.CashTrackNG.Core.DataAccess.ItemTransaction {
		From: number;
		To: number;
		RowVersion1: number[];
		FromTanker: Garda.CashTrackNG.Core.DataAccess.Tanker;
		ToTanker: Garda.CashTrackNG.Core.DataAccess.Tanker;
	}
	interface ItemTransactionCheckOut extends Garda.CashTrackNG.Core.DataAccess.ItemTransaction {
		From: number;
		To: number;
		RowVersion1: number[];
		Tanker: Garda.CashTrackNG.Core.DataAccess.Tanker;
		Unit: Garda.CashTrackNG.Core.DataAccess.Unit;
	}
	interface AreaBillingDate {
		ID: number;
		AreaID: number;
		BillingConfirmedOn: Date;
		BillingAuthorizedOn: Date;
		BillingAuthorizedBy: number;
		RowVersion: number[];
		Area: Garda.CashTrackNG.Core.DataAccess.Area;
	}
	interface HHTAreaConfig {
		AreaID: number;
		RouteDBPath: string;
		RouteDBPathUNC: string;
		ComboPath: string;
		ComboPathUNC: string;
		MasPath: string;
		NextLogNo: number;
		Bagout: string;
		NextFileNo: number;
		CurrentCustomer: string;
		RouteMiles: number;
		RadioType: string;
		ExportDirectory: string;
		ExportDirectoryUNC: string;
		CurrentRoute: string;
		CurrentDateTime: Date;
		ActiveHours: number;
		ServerLocalIPAddress: string;
		ServerRemoteIPAddress: string;
		FTPLogin: string;
		FTPPassword: string;
		MinAmount: number;
		MaxAmount: number;
		MinutesOffset: number;
		BackupPath: string;
		PhoneNumber: string;
		PhoneLogin: string;
		PhonePassword: string;
		PhonePDPContext: string;
		PhoneBaudRate: number;
		PhoneTimeout: number;
		TimeAdjust: number;
		MaxNumFiles: number;
		Name: string;
		Address: string;
		City: string;
		Postcode: string;
		CoinExtract: string;
		Currency: string;
		SuperPwd: string;
		ExitPwd: string;
		AltOcr: string;
		PrintingActive: string;
		DisplayReceipt: string;
		ScreenSigLT: string;
		ScreenSigPD: string;
		RefNo: string;
		DummyShipper: string;
		DriverVerify: string;
		MultiStop: string;
		PreTrip: string;
		AskInjury: string;
		ReceiptSigDesc: string;
		ReceiptNumPrint: number;
		ItemIdMaxSize: number;
		UsesWIFI: string;
		CustDataSrc: number;
		Area: Garda.CashTrackNG.Core.DataAccess.Area;
	}
	interface CVSCarrier {
		ID: number;
		AreaID: number;
		CVSAreaNumber: string;
		CVSCarrierName: string;
		Name: string;
		Number: number;
		RowVersion: number[];
		Area: Garda.CashTrackNG.Core.DataAccess.Area;
	}
	interface ECashItem {
		ItemID: number;
		ExpirationDate: Date;
		ECashStatusID: number;
		SourceAreaID: number;
		DestinationAreaID: number;
		CVSBankNumber: string;
		Area: Garda.CashTrackNG.Core.DataAccess.Area;
		Area1: Garda.CashTrackNG.Core.DataAccess.Area;
		ShippableItem: Garda.CashTrackNG.Core.DataAccess.ShippableItem;
		ECashStatus: Garda.CashTrackNG.Core.DataAccess.ECashStatus;
	}
	interface ECashStatus {
		ID: number;
		Code: string;
		Description: string;
		ECashItem: Garda.CashTrackNG.Core.DataAccess.ECashItem[];
	}
	interface RouteStatus {
		Code: string;
		Description: string;
		Routes: Garda.CashTrackNG.Core.DataAccess.Route[];
	}
	interface RouteEmployee {
		RouteID: number;
		EmployeeID: number;
		EmployeeRoleCode: string;
		RowVersion: number[];
		Employee: Garda.CashTrackNG.Core.DataAccess.Employee;
		EmployeeRole: Garda.CashTrackNG.Core.DataAccess.EmployeeRole;
		Route: Garda.CashTrackNG.Core.DataAccess.Route;
	}
	interface EmployeeRole {
		Code: string;
		Description: string;
		RouteEmployees: Garda.CashTrackNG.Core.DataAccess.RouteEmployee[];
	}
	interface RouteEvent {
		ID: number;
		RouteEventTypeNumber: number;
		MessengerID: number;
		UnitID: number;
		RouteID: number;
		CreatedOn: Date;
		Description: string;
		ItemBarcodes: string;
		CreatedBy: number;
		RowVersion: number[];
		MessengerEmployee: Garda.CashTrackNG.Core.DataAccess.Employee;
		DispatcherEmployee: Garda.CashTrackNG.Core.DataAccess.Employee;
		Route: Garda.CashTrackNG.Core.DataAccess.Route;
		RouteEventType: Garda.CashTrackNG.Core.DataAccess.RouteEventType;
		Unit: Garda.CashTrackNG.Core.DataAccess.Unit;
	}
	interface RouteEventType {
		Number: number;
		Description: string;
		RouteEvents: Garda.CashTrackNG.Core.DataAccess.RouteEvent[];
	}
	interface UnitType {
		Code: string;
		Description: string;
		IsNonBillableUnit: boolean;
		IsCustomerUnit: boolean;
		Units: Garda.CashTrackNG.Core.DataAccess.Unit[];
	}
	interface MissedStopRequiredAction {
		ID: number;
		Code: string;
		Description: string;
		Units: Garda.CashTrackNG.Core.DataAccess.Unit[];
	}
	interface CVSArea {
		ID: number;
		Number: string;
		Description: string;
		RowVersion: number[];
		OddCoinIncludedInCash: boolean;
		Units: Garda.CashTrackNG.Core.DataAccess.Unit[];
	}
	interface CVSBank {
		ID: number;
		CVSBankNumber: string;
		CVSBankName: string;
		IsEcashInvManagedInMM: boolean;
		Unit: Garda.CashTrackNG.Core.DataAccess.Unit[];
	}
	interface MissingItem {
		ID: number;
		ItemID: number;
		OpenedOn: Date;
		ClosedOn: Date;
		OpenedBy: number;
		ClosedBy: number;
		MissingItemTypeCode: string;
		MissingItemReasonCode: string;
		Comment: string;
		RowVersion: number[];
		ClosedByEmployee: Garda.CashTrackNG.Core.DataAccess.Employee;
		OpenedByEmployee: Garda.CashTrackNG.Core.DataAccess.Employee;
		MissingItemType: Garda.CashTrackNG.Core.DataAccess.MissingItemType;
		MissingItemReason: Garda.CashTrackNG.Core.DataAccess.MissingItemReason;
		Item: Garda.CashTrackNG.Core.DataAccess.ShippableItem;
	}
	interface MissingItemType {
		Code: string;
		Description: string;
		MissingItems: Garda.CashTrackNG.Core.DataAccess.MissingItem[];
	}
	interface MissingItemReason {
		Code: string;
		Description: string;
		IsDisplayedAtCheckInOnly: boolean;
		IsDisplayedAtCVSCarrierCheckInOnly: boolean;
		IsDisplayedAtEditMissingItemOnly: boolean;
		MissingItems: Garda.CashTrackNG.Core.DataAccess.MissingItem[];
	}
	interface UserLevel {
		Code: string;
		Description: string;
		Employee: Garda.CashTrackNG.Core.DataAccess.Employee[];
	}
	interface ItemStatus {
		Code: string;
		Description: string;
		ShippableItems: Garda.CashTrackNG.Core.DataAccess.ShippableItem[];
	}
	interface CoinShipment {
		ID: number;
		BarCode: string;
		RowVersion: number[];
		StagingId: number;
		ShippableItems: Garda.CashTrackNG.Core.DataAccess.ShippableItem[];
	}
}
declare module CashTrack.Administration {
	interface HhtExportRoutesQuery {
		Date: Date;
		AreaID: number;
		Routes: string[];
		AllRoutes: boolean;
		AmOrPmRoute: string;
	}
	interface CustomerResult {
		ID: number;
		Name: string;
		Number: number;
		IsSuspended: boolean;
	}
	interface SetLastLoginDateCommand {
		Username: string;
	}
	interface PrintLabelValueDropdownResult {
		Barcode: string;
		Spacer: string;
		Description: string;
	}
	interface VaultAuditCategoryResult {
		Category: string;
		Sums: CashTrack.Administration.VaultAuditSumResult[];
	}
	interface VaultAuditSumResult {
		ID: number;
		Category: string;
		Date: Date;
		Sum: number;
	}
	interface CustomerLookUpResult {
		ID: number;
		Barcode: string;
		AreaID: number;
		AreaName: string;
		Active: boolean;
		Routed: boolean;
		CustomerNumber: string;
		CustomerName: string;
		UnitTypeCode: string;
		SiteLocation: CashTrack.Common.SiteLocationResult;
		ProductCode: string;
		Frequency: string;
		JDELocationNumber: string;
		JDEContractNumber: string;
		ContractLineNumber: number;
		JDEBillTo: string;
		ServiceEffectiveDate: Date;
		SundayService: boolean;
		SundayRouted: boolean;
		MondayService: boolean;
		MondayRouted: boolean;
		TuesdayService: boolean;
		TuesdayRouted: boolean;
		WednesdayService: boolean;
		WednesdayRouted: boolean;
		ThursdayService: boolean;
		ThursdayRouted: boolean;
		FridayService: boolean;
		FridayRouted: boolean;
		SaturdayService: boolean;
		SaturdayRouted: boolean;
		TotalCount: number;
	}
	interface LoadTruckResult {
		RouteNumber: string;
		Time: Date;
		Barcode: string;
		Shipper: string;
		Commodity: string;
		UnitDestination: string;
		DestinationName: string;
		DestinationAddress: string;
		Qty: number;
		Amount: number;
	}
	interface PrintLabelResult {
		Scancode: string;
		Barcode: string;
		Name: string;
		Address: CashTrack.Common.AddressResult;
	}
	interface CustomerByNumberQuery {
		Number: string;
	}
	interface PrintLabelValueDropdownQuery {
		LabelType: CashTrack.Consts.LabelTypeEnum;
		SearchText: string;
		AreaID: number;
	}
	interface PrintLabelQuery {
		StartAt: number;
	}
	interface VaultAuditQuery {
		AreaId: number;
		DateRef: Date;
	}
	interface CustomerLookUpQuery extends CashTrack.Common.SearchTableQuery {
		Inactive: boolean;
		BillableType: CashTrack.Consts.BillableTypeEnum;
		AreaID: string;
		Barcode: string;
		CustomerName: string;
		CustomerNumber: string;
		Address: string;
		City: string;
		State: string;
		ZipCode: string;
		JDELocationNumber: string;
		JDEContractNumber: string;
		JDEBillTo: string;
		ContractLineNumber: string;
	}
	interface PrintLabelByValueQuery {
		StartAt: number;
		LabelType: CashTrack.Consts.LabelTypeEnum;
		Barcodes: string[];
	}
	interface PrintLabelByRouteQuery extends CashTrack.Common.Request {
		StartAt: number;
		AreaID: number;
		RouteID: number;
		ServiceDayCode: string[];
	}
	interface LoadTruckByAreaRouteAndRouteDateQuery extends CashTrack.Common.SearchTableQuery {
		AreaID: number;
		MasterRouteID: number;
		RouteDate: Date;
	}
}
declare module CashTrack.CITService {
	interface AttributeResult {
		VariableBillingCode: string;
		AttributeID: number;
		ContractLineID: number;
		CITAttributeServiceID: number;
		AreaID: number;
		UnitOfMesure: string;
		CITAttributeDescription: string;
		ServiceCode: string;
	}
	interface SaveOnDemandCommand extends CashTrack.Common.Request {
		CaseWorkOrder: string;
		CustomerAuthorizationNumber: string;
		BillingComment: string;
		OperationDescription: string;
		RequestDate: Date;
		ServiceDate: Date;
		WindowStart: Date;
		WindowEnd: Date;
		ServiceID: number;
		UnitID: number;
		FirstStopUnitID: number;
		SecondStopUnitID: number;
		Attribute: CashTrack.CITService.AttributeResult;
		StopID: number;
		AreaID: number;
		IsCancelled: boolean;
		QuantityToBill: Date;
		CITServiceID: number;
	}
	interface BillingAreaResult {
		ID: number;
		Number: number;
		Name: string;
		LatestBillingConfirmationDate: Date;
	}
	interface CITServiceResult {
		ID: number;
		ServiceCode: string;
		ServiceDescription: string;
	}
	interface CITAttributeServiceResult {
		CITServiceID: number;
		ServiceDescription: string;
		PerTripOrPerHour: CashTrack.CITService.AttributeResult[];
		SelectedPerTripOrPerHour: CashTrack.CITService.AttributeResult;
		IsSameDay: boolean;
	}
	interface OnDemandResult {
		ServiceID: number;
		ServiceName: string;
		Attribute: CashTrack.CITService.AttributeResult;
		OnDemandNumber: number;
		BillingComment: string;
		CaseNumber: string;
		CustomerAuthorizationNumber: string;
		OperationComment: string;
		FirstStop: CashTrack.Common.StopServiceResult;
		SecondStop: CashTrack.Common.StopServiceResult;
		CitAtmUnit: CashTrack.Common.StopServiceResult;
		TimeWindowFrom: Date;
		TimeWindowTo: Date;
		RequestedOn: Date;
		PlannedServiceDate: Date;
		IsRouted: boolean;
		IsCheckInStarted: boolean;
		ReadOnly: boolean;
		IsCancelled: boolean;
		AllowCancellation: boolean;
		IsRouteOpen: boolean;
		IsDeliveryService: boolean;
		IsDeliveryServiceWithItem: boolean;
		ErrorCode: CashTrack.Consts.OnDemandErrorEnum;
	}
	interface AlternateDestinationSiteQuery extends CashTrack.Common.Request {
		SearchText: string;
		CurrentUnitID: number;
		AreaIDs: number[];
	}
	interface CitServiceListQuery extends CashTrack.Common.Request {
		UnitID: number;
	}
	interface CITServiceQuery {
		ServiceCode: string;
	}
	interface FindOnDemandQuery {
		UnitID: number;
		ServiceDate: Date;
	}
	interface GetAttributeQuery {
		AttributeID: number;
	}
	interface IsAuthorizationNumberRequiredQuery {
		AttributeID: number;
	}
	interface IsUnitNotServicedOnHolidayQuery {
		SelectedServiceDate: Date;
		UnitID: number;
		AreaID: number;
	}
	interface LoadOnDemandQuery {
		OnDemandID: number;
	}
	interface OnDemandByAreaDatesAndUnitQuery extends CashTrack.Common.SearchTableQuery {
		AreaID: number;
		UnitID: number;
		FromDate: Date;
		ToDate: Date;
		IncludeCancelled: boolean;
	}
}
declare module Garda.CashTrackNG.Core.Contracts.Models.Common.Queries {
	interface ByUnitAndDateQuery {
		UnitID: number;
		Date: Date;
	}
}
declare module CashTrack.Core {
	interface CTError {
		ErrorCode: string;
		ErrorMessage: string;
		Exception: System.Exception;
	}
}
declare module System.Reflection {
	interface MethodBase extends System.Reflection.MemberInfo {
		MethodImplementationFlags: System.Reflection.MethodImplAttributes;
		MethodHandle: System.RuntimeMethodHandle;
		Attributes: System.Reflection.MethodAttributes;
		CallingConvention: System.Reflection.CallingConventions;
		IsGenericMethodDefinition: boolean;
		ContainsGenericParameters: boolean;
		IsGenericMethod: boolean;
		IsSecurityCritical: boolean;
		IsSecuritySafeCritical: boolean;
		IsSecurityTransparent: boolean;
		IsPublic: boolean;
		IsPrivate: boolean;
		IsFamily: boolean;
		IsAssembly: boolean;
		IsFamilyAndAssembly: boolean;
		IsFamilyOrAssembly: boolean;
		IsStatic: boolean;
		IsFinal: boolean;
		IsVirtual: boolean;
		IsHideBySig: boolean;
		IsAbstract: boolean;
		IsSpecialName: boolean;
		IsConstructor: boolean;
	}
	interface MemberInfo {
		MemberType: System.Reflection.MemberTypes;
		Name: string;
		DeclaringType: System.Type;
		ReflectedType: System.Type;
		CustomAttributes: System.Reflection.CustomAttributeData[];
		MetadataToken: number;
		Module: System.Reflection.Module;
	}
	interface Binder {
	}
	interface Module {
		CustomAttributes: System.Reflection.CustomAttributeData[];
		MDStreamVersion: number;
		FullyQualifiedName: string;
		ModuleVersionId: System.Guid;
		MetadataToken: number;
		ScopeName: string;
		Name: string;
		Assembly: System.Reflection.Assembly;
		ModuleHandle: System.ModuleHandle;
	}
	interface CustomAttributeData {
		AttributeType: System.Type;
		Constructor: System.Reflection.ConstructorInfo;
		ConstructorArguments: System.Reflection.CustomAttributeTypedArgument[];
		NamedArguments: System.Reflection.CustomAttributeNamedArgument[];
	}
	interface ConstructorInfo extends System.Reflection.MethodBase {
		MemberType: System.Reflection.MemberTypes;
	}
	interface CustomAttributeTypedArgument {
		ArgumentType: System.Type;
		Value: any;
	}
	interface CustomAttributeNamedArgument {
		MemberInfo: System.Reflection.MemberInfo;
		TypedValue: System.Reflection.CustomAttributeTypedArgument;
		MemberName: string;
		IsField: boolean;
	}
	interface Assembly {
		CodeBase: string;
		EscapedCodeBase: string;
		FullName: string;
		EntryPoint: System.Reflection.MethodInfo;
		ExportedTypes: System.Type[];
		DefinedTypes: System.Reflection.TypeInfo[];
		Evidence: any;
		PermissionSet: any;
		IsFullyTrusted: boolean;
		SecurityRuleSet: System.Security.SecurityRuleSet;
		ManifestModule: System.Reflection.Module;
		CustomAttributes: System.Reflection.CustomAttributeData[];
		ReflectionOnly: boolean;
		Modules: System.Reflection.Module[];
		Location: string;
		ImageRuntimeVersion: string;
		GlobalAssemblyCache: boolean;
		HostContext: number;
		IsDynamic: boolean;
	}
	interface MethodInfo extends System.Reflection.MethodBase {
		MemberType: System.Reflection.MemberTypes;
		ReturnType: System.Type;
		ReturnParameter: System.Reflection.ParameterInfo;
		ReturnTypeCustomAttributes: System.Reflection.ICustomAttributeProvider;
	}
	interface ParameterInfo {
		ParameterType: System.Type;
		Name: string;
		HasDefaultValue: boolean;
		DefaultValue: any;
		RawDefaultValue: any;
		Position: number;
		Attributes: System.Reflection.ParameterAttributes;
		Member: System.Reflection.MemberInfo;
		IsIn: boolean;
		IsOut: boolean;
		IsLcid: boolean;
		IsRetval: boolean;
		IsOptional: boolean;
		MetadataToken: number;
		CustomAttributes: System.Reflection.CustomAttributeData[];
	}
	interface ICustomAttributeProvider {
	}
	interface TypeInfo extends System.Type {
		GenericTypeParameters: System.Type[];
		DeclaredConstructors: System.Reflection.ConstructorInfo[];
		DeclaredEvents: System.Reflection.EventInfo[];
		DeclaredFields: System.Reflection.FieldInfo[];
		DeclaredMembers: System.Reflection.MemberInfo[];
		DeclaredMethods: System.Reflection.MethodInfo[];
		DeclaredNestedTypes: System.Reflection.TypeInfo[];
		DeclaredProperties: System.Reflection.PropertyInfo[];
		ImplementedInterfaces: System.Type[];
	}
	interface EventInfo extends System.Reflection.MemberInfo {
		MemberType: System.Reflection.MemberTypes;
		Attributes: System.Reflection.EventAttributes;
		AddMethod: System.Reflection.MethodInfo;
		RemoveMethod: System.Reflection.MethodInfo;
		RaiseMethod: System.Reflection.MethodInfo;
		EventHandlerType: System.Type;
		IsSpecialName: boolean;
		IsMulticast: boolean;
	}
	interface FieldInfo extends System.Reflection.MemberInfo {
		MemberType: System.Reflection.MemberTypes;
		FieldHandle: System.RuntimeFieldHandle;
		FieldType: System.Type;
		Attributes: System.Reflection.FieldAttributes;
		IsPublic: boolean;
		IsPrivate: boolean;
		IsFamily: boolean;
		IsAssembly: boolean;
		IsFamilyAndAssembly: boolean;
		IsFamilyOrAssembly: boolean;
		IsStatic: boolean;
		IsInitOnly: boolean;
		IsLiteral: boolean;
		IsNotSerialized: boolean;
		IsSpecialName: boolean;
		IsPinvokeImpl: boolean;
		IsSecurityCritical: boolean;
		IsSecuritySafeCritical: boolean;
		IsSecurityTransparent: boolean;
	}
	interface PropertyInfo extends System.Reflection.MemberInfo {
		MemberType: System.Reflection.MemberTypes;
		PropertyType: System.Type;
		Attributes: System.Reflection.PropertyAttributes;
		CanRead: boolean;
		CanWrite: boolean;
		GetMethod: System.Reflection.MethodInfo;
		SetMethod: System.Reflection.MethodInfo;
		IsSpecialName: boolean;
	}
}
declare module System.Runtime.InteropServices {
	interface StructLayoutAttribute extends System.Attribute {
		Value: System.Runtime.InteropServices.LayoutKind;
	}
}


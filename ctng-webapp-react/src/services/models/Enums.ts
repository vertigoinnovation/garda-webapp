﻿
// //module CashTrack.Core {
export enum BusinessRuleValidationCodeEnum {
	MoveStopOnMasterRoute_UnitExists = 0,
	MoveStopOnMasterRouteStopTypeNotSupported = 1,
	ItemsOrOnDemandAreAttachedToStop = 2,
	IsItemAttachedToRelatedStop = 3,
	ItemsAreAttachedToStop = 4,
	OnDemandIsAttachedToStop = 5,
	ItemsAreNotBalanced = 6,
	IsAnyStopRouteAlreadyStarted = 7,
	IsStopMissed = 8,
	ChangeServiceDay_SourceStopNotFound = 9,
	ChangeServiceDay_SourceRouteNotIsOpened = 10,
	ChangeServiceDay_SourceRouteIsInternal = 11,
	ChangeServiceDay_TargetRouteDoesNotExist = 12,
	ChangeServiceDay_TargetRouteIsNotOpened = 13,
	ChangeServiceDay_TargetRouteIsInternal = 14,
	ChangeServiceDay_UnitAlreadyServicedOnTargetRoute = 15,
	ChangeServiceDay_BeforeMasterStopDoesNotExist = 16,
	ChangeServiceDay_AfterMasterStopDoesNotExist = 17,
	ChangeServiceDay_SourceRouteIsTargetRoute = 18,
	ChangeServiceDay_SourceRouteIsInThePast = 19,
	ChangeServiceDay_TargetRouteIsInThePast = 20,
	AddStopOnMasterRoute_UnitIsSuspended = 21,
	AddStopOnMasterRoute_UnitHasNoContractOnRecurrentStop = 22,
	AddStopOnMasterRoute_UnitHasNoContractOnOneDayTemporaryStop = 23,
	AddStopOnMasterRoute_OnlyRegularOrChangeOrHolidayStopTypesSupported = 24,
	AddStopOnMasterRoute_IsBillableOrStopTypeNotSupported = 25,
	AddStopOnMasterRoute_IsNotBillableOrStopTypeNotSupported = 26,
	AddStopOnMasterRoute_RecurrentStopNotSupportedOnHolidayRoute = 27,
	AddStopOnMasterRoute_RecurrentStopNotSupportedOnSpecialRoute = 28,
	AddStopOnMasterRoute_OnlyInternalStopIsSupportedOnInternalRoute = 29,
	AddStopOnMasterRoute_OnDemandUpdateOrChangeDayOfRoute = 30,
	AddStopOnMasterRoute_OnDemandRequiresAuthorization = 31,
	AddStopOnMasterRoute_OnDemandServicesMandatory = 32,
	AddStopOnMasterRoute_EffectiveEndDatesEqualForTemporaryStop = 33,
	AddStopOnMasterRoute_UnitIsNotAllowedOffDayServicing = 34,
	AddStopOnMasterRoute_CannotEnterTemporaryStopCustomerRequiresAuthorization = 35,
	AddStopOnMasterRoute_DailyRouteNotPresentOnMissedStop = 36,
	AddStopOnMasterRoute_MissedStopNotFound = 37,
	AddStopOnMasterRoute_RegularStopMustExistOnDayOfHoliday = 38,
	AddStopOnMasterRoute_HolidayServiceNotSold = 39,
	AddStopOnMasterRoute_CVSRoutesOnlyContainProcessorUnitsOrRouteUnits = 40,
	AddStopOnMasterRoute_StopTypeNotSupported = 41,
	AddStopOnMasterRoute_ReportHolidayHolidayNotReported = 42,
	AddStopOnMasterRoute_ReportHolidayHolidaySubmittedDoesNotMatchBusinessRules = 43,
	AddStopOnMasterRoute_ReportHolidayServiceCanOnlyBeSetForOneDay = 44,
	AddStopOnMasterRoute_ReportHolidayServiceCanNotBeSetTheDayOfTheHoliday = 45,
	AddStopOnMasterRoute_MasterStopsNotPresentToAddStopBeforeOrAfter = 46,
	GenerateAutomaticOnDemand_IncorrectOrMissingInformation = 47,
	CreateOrUpdateAreaSourceRoute_RouteNumberNotAvailable = 48,
	DeleteAreaSourceRoute_CannotDeleteUsedSourceRoute = 49,
	CreateOrUpdateAreaVaultDoor_VaultDoorBarcodeNotAvailable = 50,
	CreateOrUpdateAreaHoliday_HolidayDoesAlreadyExist = 51,
	CreateOrUpdateAreaHoliday_DailyRouteForHolidyDateIsAlreadyGenerated = 52,
	CreateOrUpdateAreaCommodity_AreaCommodityDoesAlreadyExist = 53,
	MoveStopOnMasterRoute_TargetRouteIsNotOpen = 54,
	AddStopOnMasterRoute_MissingSeasonalService = 55
}
// }
//module System.Reflection {
export enum MemberTypes {
	Constructor = 1,
	Event = 2,
	Field = 4,
	Method = 8,
	Property = 16,
	TypeInfo = 32,
	Custom = 64,
	NestedType = 128,
	All = 191
}
export enum ParameterAttributes {
	None = 0,
	In = 1,
	Out = 2,
	Lcid = 4,
	Retval = 8,
	Optional = 16,
	ReservedMask = 61440,
	HasDefault = 4096,
	HasFieldMarshal = 8192,
	Reserved3 = 16384,
	Reserved4 = 32768
}
export enum EventAttributes {
	None = 0,
	SpecialName = 512,
	ReservedMask = 1024,
	RTSpecialName = 1024
}
export enum FieldAttributes {
	FieldAccessMask = 7,
	PrivateScope = 0,
	Private = 1,
	FamANDAssem = 2,
	Assembly = 3,
	Family = 4,
	FamORAssem = 5,
	Public = 6,
	Static = 16,
	InitOnly = 32,
	Literal = 64,
	NotSerialized = 128,
	SpecialName = 512,
	PinvokeImpl = 8192,
	ReservedMask = 38144,
	RTSpecialName = 1024,
	HasFieldMarshal = 4096,
	HasDefault = 32768,
	HasFieldRVA = 256
}
export enum PropertyAttributes {
	None = 0,
	SpecialName = 512,
	ReservedMask = 62464,
	RTSpecialName = 1024,
	HasDefault = 4096,
	Reserved2 = 8192,
	Reserved3 = 16384,
	Reserved4 = 32768
}
export enum TypeAttributes {
	VisibilityMask = 7,
	NotPublic = 0,
	Public = 1,
	NestedPublic = 2,
	NestedPrivate = 3,
	NestedFamily = 4,
	NestedAssembly = 5,
	NestedFamANDAssem = 6,
	NestedFamORAssem = 7,
	LayoutMask = 24,
	AutoLayout = 0,
	SequentialLayout = 8,
	ExplicitLayout = 16,
	ClassSemanticsMask = 32,
	Class = 0,
	Interface = 32,
	Abstract = 128,
	Sealed = 256,
	SpecialName = 1024,
	Import = 4096,
	Serializable = 8192,
	WindowsRuntime = 16384,
	StringFormatMask = 196608,
	AnsiClass = 0,
	UnicodeClass = 65536,
	AutoClass = 131072,
	CustomFormatClass = 196608,
	CustomFormatMask = 12582912,
	BeforeFieldInit = 1048576,
	ReservedMask = 264192,
	RTSpecialName = 2048,
	HasSecurity = 262144
}
export enum GenericParameterAttributes {
	None = 0,
	VarianceMask = 3,
	Covariant = 1,
	Contravariant = 2,
	SpecialConstraintMask = 28,
	ReferenceTypeConstraint = 4,
	NotNullableValueTypeConstraint = 8,
	DefaultConstructorConstraint = 16
}
export enum MethodImplAttributes {
	CodeTypeMask = 3,
	IL = 0,
	Native = 1,
	OPTIL = 2,
	Runtime = 3,
	ManagedMask = 4,
	Unmanaged = 4,
	Managed = 0,
	ForwardRef = 16,
	PreserveSig = 128,
	InternalCall = 4096,
	Synchronized = 32,
	NoInlining = 8,
	AggressiveInlining = 256,
	NoOptimization = 64,
	SecurityMitigations = 1024,
	MaxMethodImplVal = 65535
}
export enum MethodAttributes {
	MemberAccessMask = 7,
	PrivateScope = 0,
	Private = 1,
	FamANDAssem = 2,
	Assembly = 3,
	Family = 4,
	FamORAssem = 5,
	Public = 6,
	Static = 16,
	Final = 32,
	Virtual = 64,
	HideBySig = 128,
	CheckAccessOnOverride = 512,
	VtableLayoutMask = 256,
	ReuseSlot = 0,
	NewSlot = 256,
	Abstract = 1024,
	SpecialName = 2048,
	PinvokeImpl = 8192,
	UnmanagedExport = 8,
	RTSpecialName = 4096,
	ReservedMask = 53248,
	HasSecurity = 16384,
	RequireSecObject = 32768
}
export enum CallingConventions {
	Standard = 1,
	VarArgs = 2,
	Any = 3,
	HasThis = 32,
	ExplicitThis = 64
}
// }
//module System.Runtime.InteropServices {
export enum LayoutKind {
	Sequential = 0,
	Explicit = 2,
	Auto = 3
}
// }
//module System.Data.SqlClient {
export enum SortOrder {
	Unspecified = -1,
	Ascending = 0,
	Descending = 1
}
// }
//module CashTrack.Consts {
export enum VariableBillingCodeEnum {
	EU = 0,
	EP = 1,
	EL = 2,
	EI = 3,
	ST = 4,
	SH = 5,
	NB = 6,
	EA = 7,
	EC = 8,
	EH = 9
}
export enum PerTripPerHourEnum {
	BOTH = 0,
	HR = 1,
	TR = 2
}
export enum RouteSheetReportTypeEnum {
	Driver = 1,
	Messenger = 2,
	Both = 3
}
export enum AssignOnDemandRouteStatusEnum {
	ASSIGNED = 0,
	UNASSIGNED = 1
}
export enum DayOfWeekEnum {
	SUN = 0,
	MON = 1,
	TUE = 2,
	WED = 3,
	THU = 4,
	FRI = 5,
	SAT = 6
}
export enum TankerManifestReportEnum {
	ItemsInTanker = 1,
	Acquired = 2,
	Released = 3
}
export enum ItemShipmentStatusEnum {
	Planned = 0,
	PickedUp = 1,
	CheckedIn = 2,
	Delivered = 3,
	BaggedOut = 4,
	Cancelled = 5,
	CheckedOut = 6,
	Tankered = 7,
	Transferred = 8
}
export enum ItemTransactionTypeEnum {
	BC = 0,
	BI = 1,
	BO = 2,
	CB = 3,
	CC = 4,
	CD = 5,
	CM = 6,
	CO = 7,
	CR = 8,
	CT = 9,
	CV = 10,
	DC = 11,
	DR = 12,
	FB = 13,
	FT = 14,
	OB = 15,
	OC = 16,
	OH = 17,
	OL = 18,
	OM = 19,
	ON = 20,
	OW = 21,
	OV = 22,
	PM = 23,
	PU = 24,
	RD = 25,
	TC = 26,
	TF = 27,
	TO = 28,
	TR = 29,
	TT = 30,
	VR = 31
}
export enum TankerCheckInMode {
	ThrowToTanker = 0,
	Holdover = 1,
	Unrouted = 2,
	MoveItem = 3,
	MoveAndBalance = 4,
	CreateBagoutForArea = 5,
	CreateBagout = 6,
	EditParcel = 7,
	OtherArea = 8,
	CVS = 9,
	CancelShipmentRoute = 10,
	CreateTankerForOtherDestination = 11,
	MoveAndBalanceECash = 12
}
export enum TankerTypeEnum {
	Branch = 1,
	Holdover = 2,
	Unrouted = 3,
	Route = 4,
	Shuttle = 5,
	InternalBranch = 6
}
export enum ItemShipmentScannedCode {
	B = 0,
	E = 1,
	T = 2,
	V = 3,
	A = 4
}
export enum ItemStatusEnum {
	Expected = 0,
	InProgress = 1,
	Missing = 2,
	Done = 3,
	Transferred = 4
}
export enum RouteTypeEnum {
	ATM = 0,
	CIT = 1,
	CVS = 2,
	OtherAreaShuttle = 3,
	Shuttle = 4,
	SourceRoute = 5,
	ExternalArea = 6
}
export enum ErrorLevelEnum {
	Error = 0,
	Warning = 1
}
export enum RouteCategoryEnum {
	DA = 1,
	ST = 2,
	SN = 3,
	SPECIAL = 4,
	HO = 5,
	SIM = 6
}
export enum TimeOfTheDayEnum {
	AM = 1,
	PM = 2
}
export enum MissingItemTypeEnum {
	LoadTruck = 0,
	Delivery = 1,
	PickUp = 2,
	CheckIn = 3,
	Branch = 4,
	CVS = 5,
	Manifesting = 6
}
export enum MissingItemReasonEnum {
	DEFINITELY_MISSING = 0,
	DELIVERED_MANUALLY = 1,
	DELIVERED_MANUALLY_CONFIRMED = 2,
	INVESTIGAITON_NEEDED = 3,
	IS_CASH_COMMODITY = 4,
	IS_COIN_COMMODITY = 5,
	MANIFESTED_AND_NEVER_RECEIVED = 6,
	MISPLACED = 7,
	SEE_SUPERVISOR = 8,
	TRANSFERED_TO_ROUTE = 9
}
export enum OnDemandErrorEnum {
	NoError = 0,
	RequestDateCannotBeInTheFuture = 1,
	CannotAssignServiceDateSmallerThanTheAreaConfirmationDate = 2,
	RequestDateIsMandatory = 3,
	UnitWithNoActiveProducts = 4,
	ServiceWindowEndTimeCannotBeAfterServiceWindowStart = 5,
	CannotChangePlannedServiceDateWhenCheckInHasStartedOnARoute = 6,
	CannotChangeMainUnitWhenCheckInHasStartedOnARoute = 7,
	CannotChangeServiceWindowEndTimeWhenCheckInHasStartedOnARoute = 8,
	CannotChangeServiceWindowStartTimeWhenCheckInHasStartedOnARoute = 9,
	CannotEditOnDemandWithoutSpecifyingItsNumber = 10
}
export enum LabelTypeEnum {
	ArriveDepart = 0,
	Customer = 1,
	Tanker = 2,
	Truck = 3,
	TruckKey = 4,
	VaultDoor = 5,
	CustomerKey = 6
}
export enum BillableTypeEnum {
	All = 1,
	Billable = 2,
	NonBillable = 3
}
export enum AMorPMRouteEnum {
	AM = 0,
	PM = 1
}
export enum BagoutTypeEnum {
	BO = 0,
	BB = 1,
	BC = 2,
	BT = 3
}
export enum ByTypeSourceEnum {
	ByBankSourceType = 1,
	BySourceRoute = 2
}
export enum CommodityCodeEnum {
	BT = 0,
	BG = 1
}
export enum ECashStatusEnum {
	Expected = 1,
	InVault = 2,
	InTransit = 3,
	OnRoute = 4,
	Delivered = 5,
	Returned = 6
}
export enum EquipmentTypeCodeEnum {
	C = 0,
	K = 1,
	KEYRING = 2,
	WEAPON = 3,
	V = 4,
	RADIO = 5
}
export enum ChangeDescriptionEnum {
	NU = 0,
	DU = 1,
	AU = 2,
	UN = 3,
	SL = 4,
	NP = 5,
	CS = 6,
	CE = 7,
	TF = 8,
	TT = 9,
	AS = 10,
	RS = 11,
	CF = 12,
	AH = 13,
	RH = 14,
	SU = 15,
	UU = 16,
	TC = 17,
	TE = 18,
	TD = 19
}
export enum ExtranetUserStateEnum {
	Added = 0,
	Removed = 1,
	Unchanged = 2
}
export enum MissedStopCommentEnum {
	MiniumMissedStopCommentLength = 5,
	MaxiumMissedStopCommentLength = 120
}
export enum PackagesDisplaySearchTypeEnum {
	Unit = 0,
	Address = 1,
	City = 2,
	Zip = 3,
	JdeContract = 4,
	Area = 5,
	Route = 6,
	Vehicle = 7,
	ItemBarcode = 8
}
export enum LocationDisplayEnums {
	MaximumDateRange = 45
}
export enum LocationDisplayShipmentDirectionEnum {
	From = 0,
	To = 1
}
export enum LocationDisplaySearchTypeEnum {
	Unit = 0,
	Address = 1,
	City = 2,
	Zip = 3,
	JdeContract = 4,
	Area = 5,
	Route = 6,
	Vehicle = 7
}
export enum LocationDisplayTabEnum {
	RouteItem = 0,
	Routing = 1,
	Destinations = 2,
	Shipments = 3
}
export enum PrintUntankeredTypeEnum {
	Current = 0,
	Historic = 1
}
export enum SelectEnum {
	All = 0
}
export enum ServiceOrExtraEnum {
	S = 0,
	E = 1
}
export enum SourceTypeCode {
	Cash = 0,
	Coin = 1
}
export enum SourceTypeShortCode {
	C = 0,
	B = 1
}
export enum SourceTypeEnum {
	C = 1,
	B = 2
}
export enum CommodityTypeCodeEnum {
	BA = 0,
	BO = 1,
	CO = 2
}
export enum EmployeeUserLevelCodeEnum {
	NoAccess = 0,
	Supervisor = 1,
	User = 2
}
export enum PrintTypeEnum {
	ByValues = 0,
	ByRoute = 1
}
export enum StopStatusEnum {
	STOP_STATUS_COMPLETE_SERVICE = 1,
	STOP_STATUS_MISSED_BY_CLIENT = 2,
	STOP_STATUS_MISSED_BY_GARDA = 3,
	STOP_STATUS_NO_INFORMATION = 4,
	STOP_STATUS_NOT_STARTED = 5
}
export enum ReportCodeEnum {
	N = 0,
	B = 1,
	A = 2
}
export enum RouteKindEnum {
	Effective = 1,
	Simulation = 2
}
export enum ServiceChangeTypeEnum {
	PSC = 0,
	TSC = 1
}
export enum ServiceFrequencyCodeEnum {
	M1 = 1,
	M2 = 2,
	M3 = 3,
	M4 = 4,
	ML = 5,
	W = 6,
	W2 = 14,
	W3 = 21,
	W4 = 28
}
export enum StandardErrors {
	UnknowError = 0,
	NotBeEmpty = 1,
	InvalidValue = 2,
	MustBeUnique = 3
}
export enum StopTypeEnum {
	C = 0,
	H = 1,
	I = 2,
	R = 3,
	S = 4,
	T = 5,
	N = 6
}
export enum StringTypeSizeEnum {
	Code = 40,
	Email = 256,
	LongDescription = 240,
	Name = 100,
	PhoneNumber = 15,
	ShortDescription = 120,
	UserName = 256
}
export enum BarcodePrefixEnum {
	D = 0,
	L = 1
}
export enum ConjunctiveServiceCodeEnum {
	D = 0,
	Y = 1,
	N = 2
}
export enum EmployeeRoleEnum {
	Driver = 0,
	Messenger = 1,
	Guard = 2
}
export enum EquipmentTypeEnum {
	KEYRING = 0,
	WEAPON = 1,
	C = 2,
	V = 3,
	RADIO = 4
}
export enum MissingItemCloseStatusEnum {
	Retrieved = 0,
	Missing = 1
}
export enum RouteStatusEnum {
	OPEN = 0,
	INSERVICE = 1,
	ARRIVED = 2,
	UNLOADING = 3,
	CLOSED = 4,
	HHTLOADING = 5
}
export enum MissedStopReasonCodeEnum {
	ForceMajeure = 94,
	GivenToAnotherRoute = 95,
	NoServiceRequired = 85,
	ClientSiteProblem = 86,
	TimeExpired = 87,
	LaborIssue = 92,
	TruckIssue = 91,
	VaultIssue = 93,
	Routing = 88,
	GardaSiteProblem = 89,
	Window = 90
}
export enum MissedStopCommodityTypeCodeEnum {
	NP = 0,
	MS = 1
}
export enum NonBillableReasonCodeEnum {
	CS = 1,
	ND = 2,
	OS = 3,
	FS = 4,
	AP = 5
}
export enum StopBillingStatusEnum {
	CP = 1,
	CF = 2,
	AD = 3,
	BD = 4,
	NB = 5
}
export enum StopServiceBillingTypeEnum {
	FixedService = 1,
	VariableService = 2,
	NonBillable = 3
}
export enum UnitTypeCodeEnum {
	ATM = 0,
	BankBranch = 1,
	Commercial = 2,
	ProcessCenter = 3,
	Area = 4,
	RouteUnit = 5
}
// }
//module Garda.CashTrackNG.Core.Tooling {
export enum AutomaticOnDemandSourceType {
	MoneyManager = 0,
	DailyRoutes = 1,
	General = 2
}
// }
//module System.Security {
export enum SecurityRuleSet {
	None = 0,
	Level1 = 1,
	Level2 = 2
}
// }


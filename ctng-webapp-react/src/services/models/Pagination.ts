
export interface PagingQueryOptions {
    PageSize: number;
    PageIndex: number;
    IsPaging: boolean;
    Filter: any;
    FilterOptions: Array<PagingFilterOptions>;
    SortExpressionOptions: Array<PagingSortOptions>;
}

export interface SearchTableQuery {
    EnablePaging: boolean;
    CurrentPage: number;
    ItemsToShow: number;
    OrderBy: string;
    SortOrder: SortOrder;
}

export interface ExtranetUsersSearchQueryModel extends SearchTableQuery {
    FirstName: string;
    LastName: string;
    Username: string;
    ContractId: string;
    ContractDescription: string;
    CustomerId: string;
    CustomerDescription: string;
}

export interface ContractsSearchQueryModel extends SearchTableQuery {
    ContractId: string;
    ContractDescription: string;
    CustomerId: string;
    CustomerDescription: string;
}

export interface ContractExtranetUsersSearchQueryModel extends SearchTableQuery {
    ExtranetUserId: number;
}

export interface ExtranetUsersUnicityQueryModel {
    ExtranetUserId: number;
    FirstName: string;
    LastName: string;
}

export enum SortOrder {
    Unspecified = -1,
    Ascending = 0,
    Descending = 1,
}

export interface PagingSortOptions {
    SortExpression: string;
    SortDescending: boolean;
}

export interface PagingFilterOptions {
    Name: string;
    Operator: string;
    Value: any;
    DataType: string;
}
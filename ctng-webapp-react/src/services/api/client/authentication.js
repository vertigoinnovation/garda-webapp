// import _ from 'lodash';

// import authStorage from './auth-storage';
// import Amplify, {Auth, Hub} from 'aws-amplify';

// import {ROUTE_URLS} from '../../constants/routes';

// const AMPLIFY_CONFIG = {
//   aws_cognito_region: process.env.REACT_APP_REGION,
//   aws_user_pools_id: process.env.REACT_APP_USER_POOL_ID,
//   aws_user_pools_web_client_id: process.env.REACT_APP_CLIENT_ID,
//   oauth: {
//     domain: process.env.REACT_APP_AUTH_DOMAIN,
//     scope: [
//       'aws.cognito.signin.user.admin',
//       'email',
//       'openid',
//       'phone',
//       'profile'
//     ],
//     redirectSignIn: process.env.REACT_APP_AUTH_REDIRECT,
//     redirectSignOut: process.env.REACT_APP_AUTH_REDIRECT,
//     responseType: 'code'
//   }
// };

// /**
//  * Applies Amplify.configure and adds Hub listener to watch customOAuthState
//  * events on auth channel. Upon event triggers redirectToEncodedUrl, used for
//  * forwarding deep linkage
//  */
// function initialize() {
//   Amplify.configure(AMPLIFY_CONFIG);

//   Hub.listen('auth', ({payload: {event, data}}) => {
//     if (event === 'customOAuthState') {
//       authStorage.redirectToEncodedUrl(data);
//     }
//   });
// }

// /**
//  * Returns Auth.currentSession true if resolves else false
//  * @returns {Promise<boolean>}
//  */
// function hasCurrentSession() {
//   return Auth.currentSession()
//     .then(() => true)
//     .catch(() => false);
// }

// /**
//  * Triggers Auth.signOut
//  */
// function logout() {
//   authStorage.clearSession();

//   return Auth.signOut()
//     .then(() => null)
//     .catch(() => null)
//     .finally(() => {
//       window.location.assign(`${window.location.origin}${ROUTE_URLS.LOGIN}`);
//     });
// }

// /**
//  * Clears local session information
//  * Directs application to federatedSignIn
//  *
//  * Adds object with customState property set to encoded window location href
//  * to facilitate deep link return upon login return
//  */
// function redirectToLogin() {
//   authStorage.clearSession();

//   const encodedUrl = authStorage.encodeUrl(window.location.href);

//   window.location.assign(`${window.location.origin}${ROUTE_URLS.LOGIN}?redirectUrl=${encodedUrl}`);
// }

// /**
//  * Returns number of millis a user can be idle before session should timeout.
//  *
//  * This is based off the sessionStorage extended session values or defaults to 2 hours.
//  */
// function getIdleTimeout() {
//   const hourMillis = 60 * 60 * 1000; // 1 hour

//   const extendedSession = authStorage.getExtendedSessionConfiguration();

//   const expireTime = _.get(extendedSession, 'expireTime', 0);
//   const sessionTimeout = _.get(extendedSession, 'sessionTimeout', 2);
//   const sessionTimeoutUnits = _.get(extendedSession, 'sessionTimeoutUnits', 'hours');

//   // Only supports hours currently
//   if (sessionTimeout > 0 && sessionTimeoutUnits === 'hours' && expireTime > Date.now()) {
//     return sessionTimeout * hourMillis;
//   }

//   return 2 * hourMillis;
// }

// function signIn(username, password) {
//   return Auth.signIn(username, password);
// }

// function forgotPassword(username, metadata) {
//   return Auth.forgotPassword(username, metadata);
// }

// function forgotPasswordSubmit(username, code, password) {
//   return Auth.forgotPasswordSubmit(username, code, password);
// }

// function completeNewPassword(user, password) {
//   return Auth.completeNewPassword(user, password);
// }

// function federatedSignIn(provider) {
//   return Auth.federatedSignIn(provider);
// }

// function federatedDomainSignIn(idp_identifier, redirectUrl) {
//   return Auth.federatedSignIn(
//     {
//       customProvider: idp_identifier,
//       customState: redirectUrl
//     });
// }

// export default {
//   initialize,
//   logout,
//   redirectToLogin,
//   getIdleTimeout,
//   signIn,
//   forgotPassword,
//   forgotPasswordSubmit,
//   completeNewPassword,
//   federatedSignIn,
//   federatedDomainSignIn,
//   hasCurrentSession
// };

/************************************************************************
 *
 * WorldAware Request Utilities
 *
 * A collection of request related methods used to make request
 * against WorldAware authenticated endpoints and manage some common
 * scenarios around pagination, retry, and race conditions.
 *
 ************************************************************************/

import retry from 'retry-as-promised';
import _ from 'lodash';

import waHttp from './wa-http';
// import authStorage from './auth-storage';
// import authentication from './authentication';

/**
 * Handles adding the WA Authorization token header
 * and also changes the way array params are serialized
 * to conform with WA APIs
 *
 * @param requestConfig
 * @returns {Promise}
 */
async function requestAuthenticated(requestConfig) {
  return waHttp.httpRequest(addAuthToConfig(requestConfig))
    .catch((error) => {
      const errorCode = _.get(error, 'response.status');

      // Logout on authentication errors
      if (errorCode === 401) {
        // authentication.logout();
      }
      throw (error);
    });
}

/**
 * Creates a function that wraps requestAuthenticated that cancels
 * any outstanding requests made with this function (regardless of config).
 * This helps address slow request race conditions to ensure the last request made
 * is the one we ultimately respond to.
 *
 * If a call is canceled, it resolves with prop canceled = true
 *
 * usage:
 *
 * const requestSingleton = createRequestSingleton();
 *
 * requestSingleton({url:'someConfig'})
 *  .then(result => {
 *    if(result.canceled){
 *      // call was canceled
 *    }
 *    else {
 *      // call resolved
 *    }
 *  })
 *
 * @returns {function(*): Promise<any>}
 */
function createRequestSingleton() {
  let cancel;

  return (requestConfig) => {

    // Cancel outstanding request if exists
    if (cancel) {
      cancel();
      cancel = null;
    }

    return new Promise((resolve, reject) => {

      const configWithCancel = {
        ...requestConfig,
        cancelToken: waHttp.getRequestCancelToken((c) => cancel = c)
      };

      // Make new request with cancelTokenIncluded
      requestAuthenticated(configWithCancel)
        .then((result) => {
          cancel = null;
          resolve(result);
        })
        .catch((error) => {
          if (error.constructor.name === 'Cancel' || error.__CANCEL__ === true) {
            resolve({ canceled: true });
          }
          else {
            reject(error);
          }
        });
    });
  };
}

/**
 * Makes a repeated request until one of the following is true:
 *
 * 1. the request returns a non-error response
 * 2. the max number of errors is reached (rejects with last error)
 * 3. the timeout millis value is reached (rejects with timeout)
 * 4. the time in millis to wait between retries
 *
 * @param requestConfig - requestAuthenticated config
 * @param max - max number of requests with errors before failure
 * @param timeout - max amount of time retrying before failure
 * @param interval - millis to wait between retries
 * @returns {Promise}
 */
async function requestWithRetry(requestConfig, max = 3, timeout = 60000, interval = 1000) {
  return retry(() => {
    return requestAuthenticated(requestConfig);
  }, { max, timeout, backoffBase: interval, backoffExponent: 1 });
}

/**
 * Makes a repeated request until one of the following is true:
 *
 * 1. the successCallback returns true
 * 2. the max number of retries is reached (rejects last response)
 * 3. the timeout millis value is reached (rejects with timeout)
 *
 * The request is made repeatedly every interval number of millis
 * and the successCallback is called with the results. It is up to the
 * implementor to return true when the response is complete and should not
 * be continued.
 *
 * successCallback(response)
 *
 * @param requestConfig - requestAuthenticated config
 * @param successCallback - function that is called after each non error response, should return true if success
 * @param interval - millis to wait between retries
 * @param max - max number of requests with errors before failure
 * @param timeout - max amount of time retrying before failure
 * @returns {Promise}
 */
async function requestWithRetryUntilSuccess(
  requestConfig, successCallback, interval = 1000, max = 60, timeout = 60000) {
  const NO_SUCCESS = 'Response was not ready';
  const noSuccessError = new Error(NO_SUCCESS);

  return retry(() => {
    return new Promise((resolve, reject) => {
      requestAuthenticated(requestConfig)
        .then(result => successCallback(result) ? resolve(result) : reject(noSuccessError))
        .catch(reject);
    });
  }, {
    max: max,
    timeout: timeout,
    backoffBase: interval,
    backoffExponent: 1,
    match: [NO_SUCCESS]
  });
}

/**
 * Wrapper around the requestWithRetry method with some logging for use in multi-page request chains
 *
 * Clones the config, sets the page number, attaches the authorization token and calls request with config
 *
 * The request can handle failure and will retry up to 3 times before throwing an error.
 *
 * The resulting page information and data are returned in a wrapped model like below
 *
 * ex. {
 *   size: 50,
 *   totalElements: 210,
 *   totalPages: 5,
 *   number: 0,
 *   data: {response.data}
 *  }
 *
 * @param requestConfig
 * @param page
 * @returns {Promise<{size, totalElements, totalPages, number, summary, data}>}
 */
async function requestSinglePage(requestConfig, page = 0) {

  const config = setConfigPage(requestConfig, page);

  const response = await requestWithRetry(config);

  return wrapPage(response.data);
}

/**
 * For requests that return pages results, this method will call the endpoint with
 * the provided config for the first page, then based on the page metadata returned from
 * the first request, it will continue to request each page until all pages are fetched.
 *
 * Each request is added to a pages collection that will need to be parsed and combined
 *
 * ex. {
 *   size: 50,
 *   totalElements: 210,
 *   totalPages: 5,
 *   number: 0,
 *   data: {response.data}
 *  }
 *
 * @param requestConfig
 * @param onProgress
 * @param firstPageIndex
 * @returns {Promise<{size, totalElements, totalPages, number, summary, data}[]>}
 */
async function requestAllPages(requestConfig, onProgress, firstPageIndex = 0) {

  // Request first page of results and seed output page collection
  const firstPage = await requestSinglePage(requestConfig, firstPageIndex);
  const output = [firstPage];

  // These account for firstPageIndex other than 0
  const indexAdjustment = 1 - firstPageIndex;
  const secondPageIndex = firstPageIndex + 1;
  const adjustedPageTotal = firstPage.totalPages + firstPageIndex;

  // Call onProgress for first page
  if (onProgress) {
    onProgress(firstPageIndex + indexAdjustment, firstPage.totalPages);
  }

  // If more pages to request, get each remaining page
  if (firstPage.totalPages > 1) {
    for (let i = secondPageIndex; i < adjustedPageTotal; i++) {

      output.push(await requestSinglePage(requestConfig, i));

      if (onProgress) {
        onProgress(i + indexAdjustment, firstPage.totalPages);
      }
    }
  }

  return output;
}

/**
 * Wrap a page result in an object that contains the page identifiers as
 * well as the result set to 'data' property.
 *
 * These objects are returned from the getAllPages service, and should be parsed
 * and combined to create a combined collection of results.
 *
 * ex. {
 *   size: 50,
 *   totalElements: 210,
 *   totalPages: 5,
 *   number: 0,
 *   summary: '1/5',
 *   data: {data}
 *  }
 *
 * @param data
 * @returns {*}
 */
function wrapPage(data) {
  const size = getPageValue(data, 'size');
  const totalPages = getPageValue(data, 'totalPages');
  const totalElements = getPageValue(data, 'totalElements');
  const number = getPageValue(data, 'number');

  const displayPage = Math.min(totalPages, number + 1);
  const summary = `${displayPage}/${totalPages}`;

  return {
    size,
    totalElements,
    totalPages,
    number,
    summary,
    data
  };
}

/**
 * Function that fetches page information from result.
 * Looks first for property an inner 'page' object,
 * falls back to looking on object directly if not found in page object,
 * and lastly returns 0 if neither of those exist.
 *
 * ex. if getPageValue(item, 'prop')
 * => returns item.page.prop if exists
 * => else returns item.prop if exists
 * => else returns 0
 *
 * @param item
 * @param property
 * @returns {*}
 */
function getPageValue(item, property) {
  return _.get(item, ['page', property], _.get(item, property, 0));
}

/**
 * Given the results from a requestAllPages request, extract all page values
 * and combine them into one array.
 *
 * Assumes each page result collection is contained in embeddedPropPath ex 'data._embedded.values'
 * and combined results are returned in the same structure for consistency
 *
 * The results can be filtered for uniqueness using the
 * uniqueBy arg (which passes directly to lodash).
 *
 * @param pagedResults
 * @param embeddedPath
 * @param uniqueBy
 * @returns {{data: *}}
 */
function extractAllPagesResult(pagedResults, embeddedPath, uniqueBy) {
  if (!embeddedPath) {
    return pagedResults;
  }

  const data = _.chain(pagedResults)
    .map(embeddedPath)
    .flatten()
    .compact()
    .uniqBy(uniqueBy)
    .value();

  return _.set({}, embeddedPath, data);
}

/**
 *
 * @param requestConfig
 * @param page
 * @returns {*}
 */
function setConfigPage(requestConfig, page = 0) {
  const config = _.cloneDeep(requestConfig);

  _.set(config, 'params.page', page);

  return config;
}

/**
 *
 * @param requestConfig
 * @returns {{headers: {"Content-Type": string, Authorization: string}}}
 */
function addAuthToConfig(requestConfig) {
  const baseURL = process.env.REACT_APP_GATEWAY_URL;
  console.log(`baseURL: ${baseURL}`);

  return {
    baseURL: baseURL,
    headers: {
      // 'Content-Type': 'application/json',
      // 'Authorization': `Bearer ${authToken}`,
      // 'x-api-key': apiKey,
      'cookie': '.AspNet.Cookies=jRhfiksduJAPaPoGXTfJajXH1HHgXUBOUJk6-NTsWBeyrid-noGqnGhWoIPr3aymv79cA8Rx0afglSoNFcRx5qN9D5xDq6Z7-ozHQxdgs-jZITx6Pjnvx7OAl645vsMqRSh0-OLhomajn5mwc0ABEs0PyNIgnO0NtSD-g7H5eNCRnpq_yQeevJc5e6eTFzJUax1aLAHT9uIGQ8MZ5YLsOPjPUbx10DhaeYAg46G2H8o92U9JA29HoADv9Z2S-ELLb7G2mqndyvOTK9Ci-yF0OiUgQHxVAf92-ub55VX8eWc7Q7HgmLG4l3GiE-Mdvp4Xn3A5-LuT3u74IRYS0xOnMwiNd48ZtqzC5f1VJsvs1AmnAeJOlqCXZ0VDXL0Llybp_p-7VLbRI-dXMjYLgOR1Nls9izZsSPGIFQgBJakwfYPEzYA99FSj3clMvz-B0E2hFaN8_vEN5PDS6UnevTR6Su9Vlpn90_tHRfsdTkg2c3DfqlFz9tDKDXcjpmswc0KmtIHxGHH8M5p8quMCj5yEW9_QoH0wQIxe4jNW3pJoR3wbuVscr1EpSA93yFeMLnX6TYb0SDfu7bbQLkyJA3DzcstjEna16ehjx-lWpbbMz66LKMazsubxvOHoJjVAnyEc6wOkpjwRJ6BcSRTMyAAJh2ULedA4g9D_z58frxyRhYjMvLZTbIilrKWFWagF1SLOd3spc9Yy-LGGhmoMYnWYSaHn_i9cF7UBOnusxoFle_wTLzWKTUhUOiTmYXovtofdL4XL16qGvSpsMsfW0prHAHAVlHhZjEfm2rz_VL04oH_7szq419smXBkMMZZDAcxuGaNatuZVHpeGzdNSZLk_z-TyselaCmkHyCvbAyTH6WquZaVIES51igWWuOx8cjtwJ8KgzDHrVU1bvm_Fcq9ZDjE8LxxZsQT-MHNLlJWZGDeKK1bI_26y6LBBRM1Yj0EMIox2Lf1DJZkEoyaa0w-n1chdMOCngHGoUawivYX9K0kBD2L9AqpRG3G8leT1oXgeeT-dLc78Cagl1xjn-Daw7P6POEx5WhORQIu2g4hLf9sniUeuda9ORgb8CPAJ4LyvuN4vrcYT4cQxqy7yMNISp2Z8tx6-6DhT6VwQmEllMgpf2kc_uuLiwjXmyjs5abxe7Tf_c47ix8_8Y3ATAv2wO7bANJD-th5JaiaP0rKC26Qv5BgymV1uKEmagiGnpLGaJXvXq7L73757uR9X52aSYgT0rGcmVzrny24LlfI2GT_58An7fqEU3ak_dA10tdE2Po8WKWaHiaQqB6V8NsyqvMvCyel5CcDcI0H95tiojrON8lp9TMa0KkBGrWnNMoi0s__qNN7OF_kkMdMUm-NzBB0dS3Idw2iOvF5rr1soFES_Qd6z0gGqhRfsaNJx0cBpkxoNUkfU48fCQ4Wi_tyv964JuXL5E-8Bs6fehWYqlZEcVUL7hi92yq3kl3hZE6coaR42093UOL3DlELdbugy07e1ntpbmzhoBJC6p502grWtNSbWaxIsnpeV-NVNo_jQwZBFo03rqZOS1LteQS48VCmTPIWTwxifMSn0FNe8lNAQeLOpQYa8WMieB888YUjLeckmYhD9BQrpYU3KzSFbZ0pRoYq6nMQydls7qX6aInsquNFlg_bnvTu9N44DgjJlXXbcFtx4MoUDdbMGxdSGI5memKrz9vs_gaKaM6LxCk9rrXengwzD6EXokqyC2AEvwKlM8-EiS1FHAhDFrzfv8fLq0XJ7QGDU1pMLkOliP0Bx0iaLqxsgbDdbtvveA6RIJbuQtw; _dd_s=rum=1&id=6e6591ef-2be3-4b30-b297-b814250b570c&created=1637682272625&expire=1637683476040'
    },
    paramsSerializer: (params) => {
      // Serialize arrays without brackets
      return waHttp.encodeQueryString(params);
    },
    ...requestConfig
  };

  // const baseURL = process.env.REACT_APP_GATEWAY_URL;
  // const apiKey = process.env.REACT_APP_AUTH_API_KEY;

  // return authStorage.getToken()
  //   .then(authToken => {
  //     return {
  //       baseURL: baseURL,
  //       headers: {
  //         'Content-Type': 'application/json',
  //         'Authorization': `Bearer ${authToken}`,
  //         'x-api-key': apiKey,
  //       },
  //       paramsSerializer: (params) => {
  //         // Serialize arrays without brackets
  //         return waHttp.encodeQueryString(params);
  //       },
  //       ...requestConfig
  //     };
  //   });
}

/**
 * Creates an array of sort parameters for the request in the form:
 * sortField,asc from antd table sort values.
 *
 * @param sortField
 * @param tableSortDirection
 * @returns {*}
 */
export function createSortParams(sortField, tableSortDirection) {
  const sortDir = tableSortDirection === 'descend' ? 'desc' : 'asc';
  let sortParams = [`${sortField},${sortDir}`];

  if (sortField === 'lastName') {
    sortParams.push(`firstName,${sortDir}`); // adds firstName to sort to allow full name sorting
  }
  else if (sortField === 'createdBy.lastName') {
    sortParams.push(`createdBy.firstName,${sortDir}`); // adds firstName to sort to allow full name sorting
  }

  return sortField && tableSortDirection ? sortParams : undefined;
}

// Public API
export default {
  requestAuthenticated,
  requestWithRetry,
  requestWithRetryUntilSuccess,
  requestSinglePage,
  requestAllPages,
  createRequestSingleton,
  addAuthToConfig,
  extractAllPagesResult,
  wrapPage,
  getPageValue,
  setConfigPage,
  createSortParams
};

import React, { useState, useEffect } from 'react';

import { Layout, Menu } from 'antd';
import {
	MenuUnfoldOutlined,
	MenuFoldOutlined,
	UserOutlined,
	VideoCameraOutlined,
	UploadOutlined,
} from '@ant-design/icons';

import classes from './style.module.less';

const { Header, Sider, Content } = Layout;


const MainLayout = (props) => {
	const [collapsed, setCollapsed] = useState(true);

	const toggle = () => {
		setCollapsed(!collapsed);
	};

	return (
		<>
			<Layout className={classes.root} >
				<Sider className={classes.sidebar} trigger={null} collapsible collapsed={collapsed}>
					<div className="logo" />
					<Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
						<Menu.Item key="1" icon={<UserOutlined />}>
							nav 1
						</Menu.Item>
						<Menu.Item key="2" icon={<VideoCameraOutlined />}>
							nav 2
						</Menu.Item>
						<Menu.Item key="3" icon={<UploadOutlined />}>
							nav 3
						</Menu.Item>
					</Menu>
				</Sider>
				<Layout>
					<Header className={classes.siteLayoutBackground} >
						{React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
							className: classes.trigger,
							onClick: toggle,
						})}
					</Header>
					<Content
						className={classes.siteLayout}
					>
						Content
					</Content>
				</Layout>
			</Layout>
		</>
	);
};

export default MainLayout;

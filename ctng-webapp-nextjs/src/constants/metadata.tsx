import LINK from '../constants/urls'

const metadata = {
	APP_NAME: 'Cashtrak',
	APP_DESCRIPTION: 'Cashtrak Web',
	IMG_SHARE: LINK.WEB_URL + '/icons/512x512.png',
	KEY_WORDS: '',
	WEB_URL: LINK.WEB_URL,
	PRIMARY_COLOR: 'red',
}

export default metadata


import type { AppProps } from 'next/app'

import Head from 'next/head';

import MainLayout from 'src/components/Layout/MainLayout';


const MyApp = ({ Component, pageProps }: AppProps) => {

	// return <Component {...pageProps} />

	const Layout = Component.Layout || MainLayout

	return (
		<Layout>
			<Head>
				<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, height=device-height, user-scalable=0" />
			</Head>
			<Component {...pageProps}
			// router={router}
			/>
			{/* <Loading fullScreen loading={awaitLoading} /> */}
		</Layout>
	);

}

MyApp.getInitialProps = async (context) => {
	const { ctx, Component } = context;

	// if (!process.browser) {
	// 	cookie.plugToRequest(ctx.req, ctx.res);
	// }

	// if (!AuthStorage.loggedIn && !urlsIgnore.includes(ctx.pathname)) {
	// 	if (ctx.res) {
	// 		ctx.res.writeHead(302, { Location: '/login' });
	// 		ctx.res.end();
	// 	}
	// }

	// if (AuthStorage.loggedIn && urlsIgnore.includes(ctx.pathname)) {
	// 	if (ctx.res) {
	// 		ctx.res.writeHead(302, { Location: '/inbox' });
	// 		ctx.res.end();
	// 	}
	// }

	// calls page's `getInitialProps` and fills `appProps.pageProps`
	let pageProps = {};

	if (Component?.getInitialProps) {
		pageProps = await Component?.getInitialProps(ctx);
	}

	const propsData = {
		...pageProps,
	};

	let layoutProps = {};

	if (Component?.Layout) {
		layoutProps = await Component?.Layout?.getInitialProps?.({
			...ctx,
			pageProps: propsData,
		});
	} else {
		layoutProps = await MainLayout?.getInitialProps?.({
			...ctx,
			pageProps: propsData,
		});
	}

	return {
		pageProps: {
			...propsData,
			...layoutProps,
		},
	};
};

export default MyApp
